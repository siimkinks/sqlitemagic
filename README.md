Project moved to [GitHub](https://github.com/SiimKinks/sqlitemagic/)!
====================================================================


# SETUP #

Download latest IntelliJ plugin from [downloads](https://bitbucket.org/siimkinks/sqlitemagic/downloads/) and install it into Android Studio (Settings -> Plugins -> Install plugin from disk...).

Modify project root build.gradle file


	buildscript {
		repositories {
			jcenter()
			maven {
				url  "http://dl.bintray.com/siimkinks/maven"
			}
		}
		dependencies {
            // replace with the current version of the Android plugin
			classpath 'com.android.tools.build:gradle:2.1.2'
            // the latest version of the sqlitemagic plugin
			classpath 'com.siimkinks.sqlitemagic:sqlitemagic-plugin:0.8.0'
		}
	}

	allprojects {
		repositories {
			jcenter()
			maven {
				url  "http://dl.bintray.com/siimkinks/maven"
			}
		}
	}


Apply SqliteMagic plugin to your app build.gradle

	apply plugin: 'com.android.application'
	apply plugin: 'com.siimkinks.sqlitemagic'

	android {
		defaultConfig {
			...
			buildConfigField "int", "DB_VERSION", "1"
			// Optional (defaults to "database.db")
			buildConfigField "String", "DB_NAME", "\"sample.db\""
		}
	}
	dependencies {
		...
		// don't forget to add RxJava manually
		compile 'io.reactivex:rxjava:1.1.6'
	}

Init SqliteMagic somewhere (usually in Application's onCreate):

	public final class MyApplication extends Application {
		@Override
		public void onCreate() {
			super.onCreate();
			SqliteMagic.init(this);
		}
	}

Note: this init uses the default config. To modify it explore SqliteMagic static methods

**Start using it** #profit

## Optional ##

SqliteMagic gradle plugin has following configurations (with default values):

	sqlitemagic {
		boolean debugBytecodeProcessor = false
		boolean generateLogging = true
		String autoValueAnnotation = "com.google.auto.value.AutoValue"
	}


# Changelog #

## 0.8.0 ##

* NEW: Gradle plugin rewrite to support [android transform API](http://tools.android.com/tech-docs/new-build-system/transform-api)
* NEW: Use column alias if present in expression in favor of full definition
* FIX: Not throwing NPE when querying not nullable single column when result set is empty

## 0.7.2 ##

* NEW: Functions `replace`, `substring` and `trim`
* MISC: String escape aliases

## 0.7.1 ##

* NEW: Ability to define subquery as column with `asColumn(@NonNull String alias)` method
* NEW: Topsort tables creation order
* NEW: Support for any type in `Select.val()`
* FIX: User defined table rename now handled correctly

## 0.7.0 ##

* NEW: Breaking change -> Bulk observe methods now propagating only operation end result to downstream
* NEW: Breaking change -> Default AutoValue implementation is now Google's original library (thanks to added extensions support)
* NEW: Operators `runQueryOrDefault` and `runQueryOnceOrDefault
* NEW: Now generating compiler warnings if some transformer method is missing @NonNull or @NotNull annotation, but some data structure is requiring it
* FIX: Apply backpressure strategy between database/content provider and the supplied Scheduler. This guards against backpressure exceptions when the scheduler is unable to keep up with the rate at which queries are being triggered
* FIX: `runQuery()` operator now asks for more when item is dropped fixing stream "hanging" bug
* FIX: Support for transformer classes with generics
* MISC: Annotation processor will not generate warnings if project is configured properly

## 0.6.0 ##

* NEW: Ability to create multiple simultaneous connections to different databases
* MISC: Breaking change -> init builder API changes

## 0.5.2 ##

* NEW: Complex columns now have option to define expressions with long type
* NEW: Removed @CheckResult from database setup builder
* NEW: Removed ordered view support since it was quite error prone
* NEW: Removed view complex columns generating in Table classes -- there is no way to support them in queries
* NEW: IntelliJ plugin now uses interface types in magic methods
* FIX: Use Single.fromCallable in favor of Single.create
* FIX: Using view columns in query now work in all cases
* FIX: Generate generics for complex column alias override return type

## 0.5.1 ##

* FIX: Value classes now handling @IgnoreColumn correctly
* FIX: Columns from inherited methods now handled correctly

## 0.5.0 ##

* NEW: Breaking change -> query API is rewritten to be typesafe. Now annotation processor generates <TableObjectName>Table.java classes which contain table and column constant which must be used in queries.
* NEW: Single column query support.
* NEW: SQL functions support.
* NEW: Subquery support.
* NEW: Improved update builder.
* NEW: Views (completely rebuilt view system)
* NEW: Query logs now contain timing information on how long they took to execute. This only covers the time until a `Cursor` was made available, not object mapping or delivering to subscribers.
* NEW: Option to specify a `Scheduler` with `scheduleRxQueriesOn` method which will be used when sending query triggers. This allows the query to be run in subsequent operators without needing an additional `observeOn`. It also eliminates the need to use `subscribeOn` since the supplied `Scheduler` will be used for all emissions (similar to RxJava's `timer`, `interval`, etc.).
* NEW: Removed non-static transformers support. Breaking change: renamed annotation @ObjectTransformer -> @Transformer
* NEW: Ability to re-init to connect to different database
* NEW: Removed SqlDateConverter and CalendarTransformer
* FIX: Check for subscribing inside a transaction using a more accurate primitive.
* FIX: Fixed query operators breaking unsubscription chain -> fixes some query leaks
* FIX: Nullable primitive column caused NPE in value objects when selection was defined
* MISC: now min API 16

## 0.4.0-snapshot ##

* FEATURE: breaking change + requires IntelliJ plugin update -> added method to bulk delete objects
* FEATURE: breaking change -> implemented RxJava Single where appropriate
* BUG: Better error message when lib is initialized with no persistable models
* MISC: bumped dependencies versions. IMPORTANT: now min version of RxJava is 1.1.0
* MISC: removed Android v4 support library dependency
* MISC: Removed creation of synthetic accessor methods
* MISC: Other micro optimizations

## 0.3.1-snapshot ##

* FEATURE: potentially breaking change -> in order to eliminate potential thread hopping, all observable operations now do NOT have defined scheduler
* BUG: fixed immutable object builder redundant warning
* BUG: fixed FastCursor limit bug
* BUG: fixed instant run incompatibility - removed unused Afterburner and Morpheus dependencies
* Other micro bug fixes

## 0.3.0-snapshot ##

* FEATURE: breaking change -> new builder for orderBy. Option to specify ordering direction
* FEATURE: new count query operator ```isZero```
* BUG: fixed upgrade scripts execution
* BUG: fixed default db name
* BUG: fixed orderBy and groupBy sql generation with Column parameter
* Other micro improvements and bug fixes

## 0.2.0-snapshot ##

* FEATURE: breaking changes -> new query specific operators and functions
* FEATURE: breaking change -> db version must be defined in the android buildConfigField as "DB_VERSION". Also, db name can be defined the same way with "DB_NAME".
* FEATURE: migration scripts applying on db upgrade - scripts must be located in the assets folder and in the form of <version nr>.sql (NB! no automagic generation yet)
* BUG: fixed magic column constants generating for immutable objects
* BUG: fixed immutable objects builder methods resolution bug
* Other micro improvements and bug fixes

## 0.1.4-snapshot ##

* fixed compiler dependencies issue
* now works with retrolambda

## 0.1.2-snapshot ##

* fixed build bug when used in library module

## 0.1.1-snapshot ##

* improved validation (including graph cycle detections)
* support for mutable object magic id column
* bug fixes

## 0.1.0-snapshot ##
* initial aplha release