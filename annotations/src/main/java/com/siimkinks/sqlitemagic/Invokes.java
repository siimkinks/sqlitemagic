package com.siimkinks.sqlitemagic;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Siim Kinks
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.CLASS)
public @interface Invokes {
	String value();
	boolean useThisAsOnlyParam() default false;
}
