package com.siimkinks.sqlitemagic;

import android.support.annotation.CheckResult;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;

import com.siimkinks.sqlitemagic.entity.ConnectionProvidedOperation;

import java.util.LinkedList;

import rx.Single;

abstract class UpdateSqlNode extends SqlNode {
	@NonNull
	final CompiledUpdate.Builder updateBuilder;

	UpdateSqlNode(@Nullable UpdateSqlNode parent) {
		super(parent);
		final CompiledUpdate.Builder updateBuilder;
		if (parent != null) {
			updateBuilder = parent.updateBuilder;
		} else {
			updateBuilder = new CompiledUpdate.Builder();
		}
		updateBuilder.sqlTreeRoot = this;
		updateBuilder.sqlNodeCount++;
		this.updateBuilder = updateBuilder;
	}

	@Override
	protected final void appendSql(@NonNull StringBuilder sb, @NonNull SimpleArrayMap<String, LinkedList<String>> systemRenamedTables) {
		throw new UnsupportedOperationException();
	}

	public static abstract class ExecutableNode extends UpdateSqlNode implements ConnectionProvidedOperation<ExecutableNode> {
		ExecutableNode(@NonNull UpdateSqlNode parent) {
			super(parent);
		}

		@Override
		public final ExecutableNode usingConnection(@NonNull DbConnection connection) {
			updateBuilder.dbConnection = (DbConnectionImpl) connection;
			return this;
		}

		@NonNull
		@CheckResult
		public final CompiledUpdate compile() {
			return updateBuilder.build();
		}

		@WorkerThread
		public final int execute() {
			return updateBuilder.build().execute();
		}

		@NonNull
		@CheckResult
		public final Single<Integer> observe() {
			return updateBuilder.build().observe();
		}
	}
}
