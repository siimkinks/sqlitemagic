package com.siimkinks.sqlitemagic;

import android.support.annotation.CheckResult;
import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;

public interface CompiledCountSelect<S> {
	@CheckResult
	@WorkerThread
	long execute();

	@NonNull
	@CheckResult
	CountQueryObservable observe();
}
