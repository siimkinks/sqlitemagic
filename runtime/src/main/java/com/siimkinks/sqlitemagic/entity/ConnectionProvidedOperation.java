package com.siimkinks.sqlitemagic.entity;

import android.support.annotation.CheckResult;
import android.support.annotation.NonNull;

import com.siimkinks.sqlitemagic.DbConnection;

public interface ConnectionProvidedOperation<R> {
	@CheckResult
	R usingConnection(@NonNull DbConnection connection);
}
