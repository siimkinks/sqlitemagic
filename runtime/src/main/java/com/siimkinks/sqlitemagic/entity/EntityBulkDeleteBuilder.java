package com.siimkinks.sqlitemagic.entity;

import android.support.annotation.CheckResult;

import rx.Single;

public interface EntityBulkDeleteBuilder<T> extends ConnectionProvidedOperation<EntityBulkDeleteBuilder<T>> {
	int execute();

	@CheckResult
	Single<Integer> observe();
}
