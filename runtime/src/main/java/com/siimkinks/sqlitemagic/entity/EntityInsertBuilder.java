package com.siimkinks.sqlitemagic.entity;

import android.support.annotation.CheckResult;

import com.siimkinks.sqlitemagic.ConflictAlgorithm;

import rx.Single;

public interface EntityInsertBuilder extends ConnectionProvidedOperation<EntityInsertBuilder> {
	@CheckResult
	EntityInsertBuilder conflictAlgorithm(@ConflictAlgorithm int conflictAlgorithm);

	long execute();

	@CheckResult
	Single<Long> observe();
}
