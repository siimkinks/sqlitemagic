package com.siimkinks.sqlitemagic.entity;

import android.support.annotation.CheckResult;

import rx.Observable;

public interface EntityBulkInsertBuilder<T> extends ConnectionProvidedOperation<EntityBulkInsertBuilder<T>> {
	boolean execute();

	@CheckResult
	Observable<Boolean> observe();
}
