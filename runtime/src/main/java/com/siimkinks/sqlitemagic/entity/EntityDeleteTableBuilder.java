package com.siimkinks.sqlitemagic.entity;

import android.support.annotation.CheckResult;

import rx.Single;

public interface EntityDeleteTableBuilder extends ConnectionProvidedOperation<EntityDeleteTableBuilder> {
	int execute();

	@CheckResult
	Single<Integer> observe();
}
