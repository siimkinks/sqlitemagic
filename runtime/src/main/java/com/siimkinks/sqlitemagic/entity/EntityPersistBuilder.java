package com.siimkinks.sqlitemagic.entity;

import android.support.annotation.CheckResult;

import rx.Single;

public interface EntityPersistBuilder extends ConnectionProvidedOperation<EntityPersistBuilder> {
	@CheckResult
	EntityPersistBuilder ignoreNullValues();

	long execute();

	@CheckResult
	Single<Long> observe();
}
