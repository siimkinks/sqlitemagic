package com.siimkinks.sqlitemagic.entity;

import android.support.annotation.CheckResult;

import com.siimkinks.sqlitemagic.ConflictAlgorithm;

import rx.Single;

public interface EntityUpdateBuilder extends ConnectionProvidedOperation<EntityUpdateBuilder> {
	@CheckResult
	EntityUpdateBuilder conflictAlgorithm(@ConflictAlgorithm int conflictAlgorithm);

	boolean execute();

	@CheckResult
	Single<Boolean> observe();
}
