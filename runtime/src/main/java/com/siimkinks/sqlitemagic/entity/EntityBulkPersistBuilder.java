package com.siimkinks.sqlitemagic.entity;

import android.support.annotation.CheckResult;

import rx.Observable;

public interface EntityBulkPersistBuilder<T> extends ConnectionProvidedOperation<EntityBulkPersistBuilder<T>> {
	@CheckResult
	EntityBulkPersistBuilder<T> ignoreNullValues();

	boolean execute();

	@CheckResult
	Observable<Boolean> observe();
}
