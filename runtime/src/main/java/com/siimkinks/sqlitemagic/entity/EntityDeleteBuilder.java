package com.siimkinks.sqlitemagic.entity;

import android.support.annotation.CheckResult;

import rx.Single;

public interface EntityDeleteBuilder extends ConnectionProvidedOperation<EntityDeleteBuilder> {
	int execute();

	@CheckResult
	Single<Integer> observe();
}
