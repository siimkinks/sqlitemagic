package com.siimkinks.sqlitemagic.entity;

import android.support.annotation.CheckResult;

import rx.Observable;

public interface EntityBulkUpdateBuilder<T> extends ConnectionProvidedOperation<EntityBulkUpdateBuilder<T>> {
	boolean execute();

	@CheckResult
	Observable<Boolean> observe();
}
