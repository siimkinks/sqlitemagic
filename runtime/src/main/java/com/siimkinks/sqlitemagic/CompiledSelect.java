package com.siimkinks.sqlitemagic;

import android.support.annotation.CheckResult;
import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;

import java.util.List;

public interface CompiledSelect<T, S> {
	@NonNull
	@CheckResult
	@WorkerThread
	List<T> execute();

	@NonNull
	@CheckResult
	QueryObservable<List<T>> observe();

	@NonNull
	@CheckResult
	CompiledFirstSelect<T, S> takeFirst();

	@NonNull
	@CheckResult
	CompiledCountSelect<S> count();

	@NonNull
	@CheckResult
	CompiledCursorSelect<T, S> toCursor();
}
