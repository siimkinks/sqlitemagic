package com.siimkinks.sqlitemagic;

import android.support.annotation.CheckResult;
import android.support.annotation.NonNull;

public final class Delete extends DeleteSqlNode {
	Delete() {
		super(null);
	}

	@Override
	protected void appendSql(@NonNull StringBuilder sb) {
		sb.append("DELETE");
	}

	@CheckResult
	public static <T> From<T> from(@NonNull Table<T> table) {
		return new From<>(new Delete(), table);
	}

	public static final class From<T> extends ExecutableNode {
		@NonNull
		final Table<T> table;

		From(@NonNull Delete parent, @NonNull Table<T> table) {
			super(parent);
			this.table = table;
			deleteBuilder.from = this;
		}

		@Override
		protected void appendSql(@NonNull StringBuilder sb) {
			sb.append("FROM ");
			table.appendToSqlFromClause(sb);
		}

		@CheckResult
		public Where where(@NonNull Expr expr) {
			return new Where(this, expr);
		}
	}

	public static final class Where extends ExecutableNode {
		@NonNull
		private final Expr expr;

		Where(@NonNull DeleteSqlNode parent, @NonNull Expr expr) {
			super(parent);
			this.expr = expr;
			expr.addArgs(deleteBuilder.args);
		}

		@Override
		protected void appendSql(@NonNull StringBuilder sb) {
			sb.append("WHERE ");
			expr.appendToSql(sb);
		}
	}
}
