package com.siimkinks.sqlitemagic;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public interface Logger {
	void logDebug(@NonNull String message);

	void logWarning(@NonNull String message);

	void logError(@NonNull String message);

	void logError(@NonNull String message, @NonNull Throwable throwable);

	void logQueryTime(long queryTimeInMillis, @NonNull String[] observedTables, @NonNull String sql, @Nullable String[] args);
}
