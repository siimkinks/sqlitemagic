package com.siimkinks.sqlitemagic;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.CheckResult;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.Size;
import android.support.annotation.WorkerThread;

import com.siimkinks.sqlitemagic.entity.ConnectionProvidedOperation;

import java.util.Collection;

import rx.Subscription;

import static com.siimkinks.sqlitemagic.CompiledSelectImpl.createQueryObservable;
import static java.lang.System.nanoTime;
import static java.util.concurrent.TimeUnit.NANOSECONDS;

public final class RawSelect {
	@NonNull
	final String sql;

	RawSelect(@NonNull String sql) {
		this.sql = sql;
	}

	@CheckResult
	public From from(@NonNull @Size(min = 1) Table<?>... tables) {
		final int len = tables.length;
		final String[] observedTables = new String[len];
		for (int i = 0; i < len; i++) {
			observedTables[i] = tables[i].nameInQuery;
		}
		return new From(this, observedTables);
	}

	@CheckResult
	public From from(@NonNull @Size(min = 1) Collection<Table<?>> tables) {
		final int len = tables.size();
		final String[] observedTables = new String[len];
		int i = 0;
		for (Table<?> table : tables) {
			observedTables[i] = table.nameInQuery;
			i++;
		}
		return new From(this, observedTables);
	}

	public static final class From implements ConnectionProvidedOperation<From> {
		@NonNull
		final RawSelect select;
		@NonNull
		final String[] observedTables;
		@Nullable
		String[] args;
		@NonNull
		DbConnectionImpl dbConnection = SqliteMagic.getDefaultDbConnection();

		From(@NonNull RawSelect select, @NonNull String[] observedTables) {
			this.select = select;
			this.observedTables = observedTables;
		}

		@Override
		public From usingConnection(@NonNull DbConnection connection) {
			dbConnection = (DbConnectionImpl) connection;
			return this;
		}

		@CheckResult
		public From withArgs(@NonNull @Size(min = 1) String... args) {
			this.args = args;
			return this;
		}

		@CheckResult
		public CompiledRawSelect compile() {
			return new CompiledRawSelect(this, dbConnection);
		}

		@NonNull
		@CheckResult
		@WorkerThread
		public Cursor execute() {
			return new CompiledRawSelect(this, dbConnection).execute();
		}

		@NonNull
		@CheckResult
		public QueryObservable<Cursor> observe() {
			return new CompiledRawSelect(this, dbConnection).observe();
		}
	}

	public static final class CompiledRawSelect extends Query<Cursor> {
		@NonNull
		final String sql;
		@NonNull
		final String[] observedTables;
		@Nullable
		final String[] args;

		CompiledRawSelect(@NonNull From from,
		                  @NonNull DbConnectionImpl dbConnection) {
			super(dbConnection);
			this.sql = from.select.sql;
			this.observedTables = from.observedTables;
			this.args = from.args;
		}

		@NonNull
		@Override
		Cursor runImpl(@NonNull Subscription subscriber, boolean inStream) {
			super.runImpl(subscriber, inStream);
			final SQLiteDatabase db = dbConnection.getReadableDatabase();
			final long startNanos = nanoTime();
			final Cursor cursor = db.rawQueryWithFactory(null, sql, args, null, null);
			if (SqliteMagic.LOGGING_ENABLED) {
				final long queryTimeInMillis = NANOSECONDS.toMillis(nanoTime() - startNanos);
				LogUtil.logQueryTime(queryTimeInMillis, observedTables, sql, args);
			}
			return cursor;
		}

		@NonNull
		@CheckResult
		@WorkerThread
		public Cursor execute() {
			return runImpl(INFINITE_SUBSCRIPTION, false);
		}

		@NonNull
		@CheckResult
		public QueryObservable<Cursor> observe() {
			return new QueryObservable<>(createQueryObservable(observedTables, (Query<Cursor>) this));
		}

		@Override
		public String toString() {
			return "[RAW; sql=" + sql + "]";
		}
	}
}
