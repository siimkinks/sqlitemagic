package com.siimkinks.sqlitemagic;

import android.database.Cursor;
import android.support.annotation.CheckResult;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;

public interface CompiledCursorSelect<T, S> {
	@Nullable
	@CheckResult
	@WorkerThread
	T getFromCurrentPosition(@NonNull Cursor cursor);

	@NonNull
	@CheckResult
	@WorkerThread
	Cursor execute();

	@NonNull
	@CheckResult
	QueryObservable<Cursor> observe();
}
