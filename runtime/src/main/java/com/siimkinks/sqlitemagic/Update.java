package com.siimkinks.sqlitemagic;

import android.support.annotation.CheckResult;
import android.support.annotation.NonNull;

import com.siimkinks.sqlitemagic.Select.Select1;

import java.util.ArrayList;

public final class Update extends UpdateSqlNode {
	Update() {
		super(null);
	}

	@Override
	protected void appendSql(@NonNull StringBuilder sb) {
		sb.append("UPDATE");
	}

	@CheckResult
	public static UpdateConflictAlgorithm withConflictAlgorithm(@ConflictAlgorithm int conflictAlgorithm) {
		return new UpdateConflictAlgorithm(new Update(), conflictAlgorithm);
	}

	@CheckResult
	public static <T> TableNode<T> table(@NonNull Table<T> table) {
		return new TableNode<>(new Update(), table);
	}

	public static final class UpdateConflictAlgorithm extends UpdateSqlNode {
		@ConflictAlgorithm
		private final int conflictAlgorithm;

		UpdateConflictAlgorithm(@NonNull Update parent, @ConflictAlgorithm int conflictAlgorithm) {
			super(parent);
			this.conflictAlgorithm = conflictAlgorithm;
		}

		@Override
		protected void appendSql(@NonNull StringBuilder sb) {
			sb.append(ConflictAlgorithm.CONFLICT_VALUES[conflictAlgorithm]);
		}

		@CheckResult
		public <T> TableNode<T> table(@NonNull Table<T> table) {
			return new TableNode<>(this, table);
		}
	}

	public static final class TableNode<T> extends UpdateSqlNode {
		@NonNull
		final Table<T> table;

		TableNode(@NonNull UpdateSqlNode parent, @NonNull Table<T> table) {
			super(parent);
			this.table = table;
			updateBuilder.tableNode = this;
		}

		@Override
		protected void appendSql(@NonNull StringBuilder sb) {
			table.appendToSqlFromClause(sb);
		}

		@CheckResult
		public <V, R, ET> Set<T> set(@NonNull Column<V, R, ET, T> column, @NonNull V value) {
			return new Set<>(this, new UpdateColumn<>(column).is(value));
		}

		@CheckResult
		public <V, R, ET> Set<T> set(@NonNull Column<V, R, ET, T> column,
		                             @NonNull Column<?, ?, ? extends ET, ?> assignmentColumn) {
			return new Set<>(this, new UpdateColumn<>(column).is(assignmentColumn));
		}

		@CheckResult
		public <V, R, ET> Set<T> set(@NonNull Column<V, R, ET, T> column,
		                             @NonNull SelectSqlNode.SelectNode<? extends ET, Select1> select) {
			return new Set<>(this, new UpdateColumn<>(column).is(select));
		}
	}

	private static final class UpdateColumn<T, R, ET, P> extends Column<T, R, ET, P> {
		@NonNull
		private final Column<T, R, ET, P> parentColumn;

		UpdateColumn(@NonNull Column<T, R, ET, P> parentColumn) {
			super(parentColumn.table, parentColumn.name, parentColumn.allFromTable, parentColumn.valueParser, parentColumn.nullable, parentColumn.alias);
			this.parentColumn = parentColumn;
		}

		@NonNull
		@Override
		String toSqlArg(@NonNull T val) {
			return parentColumn.toSqlArg(val);
		}

		@Override
		void appendSql(@NonNull StringBuilder sb) {
			sb.append(name);
		}
	}

	public static final class Set<T> extends ExecutableNode {
		private final ArrayList<Expr> updates = new ArrayList<>(1);

		Set(@NonNull UpdateSqlNode parent, @NonNull Expr firstUpdate) {
			super(parent);
			updates.add(firstUpdate);
			firstUpdate.addArgs(updateBuilder.args);
		}

		@Override
		protected void appendSql(@NonNull StringBuilder sb) {
			sb.append("SET ");
			final ArrayList<Expr> updates = this.updates;
			int size = updates.size();
			for (int i = 0; i < size; i++) {
				if (i != 0) {
					sb.append(',');
				}
				updates.get(i).appendToSql(sb);
			}
		}

		@CheckResult
		public <V, R, ET> Set<T> set(@NonNull Column<V, R, ET, T> column, @NonNull V value) {
			final Expr expr = new UpdateColumn<>(column).is(value);
			updates.add(expr);
			expr.addArgs(updateBuilder.args);
			return this;
		}

		@CheckResult
		public <V, R, ET> Set<T> set(@NonNull Column<V, R, ET, T> column,
		                             @NonNull Column<?, ?, ? extends ET, ?> assignmentColumn) {
			final Expr expr = new UpdateColumn<>(column).is(assignmentColumn);
			updates.add(expr);
			expr.addArgs(updateBuilder.args);
			return this;
		}

		@CheckResult
		public <V, R, ET> Set<T> set(@NonNull Column<V, R, ET, T> column,
		                             @NonNull SelectSqlNode.SelectNode<? extends ET, Select1> select) {
			final Expr expr = new UpdateColumn<>(column).is(select);
			updates.add(expr);
			expr.addArgs(updateBuilder.args);
			return this;
		}

		@CheckResult
		public Where where(@NonNull Expr expr) {
			return new Where(this, expr);
		}
	}

	public static final class Where extends ExecutableNode {
		@NonNull
		private final Expr expr;

		Where(@NonNull UpdateSqlNode parent, @NonNull Expr expr) {
			super(parent);
			this.expr = expr;
			expr.addArgs(updateBuilder.args);
		}

		@Override
		protected void appendSql(@NonNull StringBuilder sb) {
			sb.append("WHERE ");
			expr.appendToSql(sb);
		}
	}
}
