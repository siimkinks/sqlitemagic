package com.siimkinks.sqlitemagic;

import android.support.annotation.CheckResult;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;

public interface CompiledFirstSelect<T, S> {
	@Nullable
	@CheckResult
	@WorkerThread
	T execute();

	@NonNull
	@CheckResult
	QueryObservable<T> observe();
}
