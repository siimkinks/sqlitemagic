package com.siimkinks.sqlitemagic;

import android.support.annotation.CheckResult;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;

import com.siimkinks.sqlitemagic.entity.ConnectionProvidedOperation;

import java.util.LinkedList;

import rx.Single;

abstract class DeleteSqlNode extends SqlNode {
	@NonNull
	final CompiledDelete.Builder deleteBuilder;

	DeleteSqlNode(@Nullable DeleteSqlNode parent) {
		super(parent);
		final CompiledDelete.Builder deleteBuilder;
		if (parent != null) {
			deleteBuilder = parent.deleteBuilder;
		} else {
			deleteBuilder = new CompiledDelete.Builder();
		}
		deleteBuilder.sqlTreeRoot = this;
		deleteBuilder.sqlNodeCount++;
		this.deleteBuilder = deleteBuilder;
	}

	@Override
	protected final void appendSql(@NonNull StringBuilder sb, @NonNull SimpleArrayMap<String, LinkedList<String>> systemRenamedTables) {
		throw new UnsupportedOperationException();
	}

	public static abstract class ExecutableNode extends DeleteSqlNode implements ConnectionProvidedOperation<ExecutableNode> {
		ExecutableNode(@NonNull DeleteSqlNode parent) {
			super(parent);
		}

		@Override
		public ExecutableNode usingConnection(@NonNull DbConnection connection) {
			deleteBuilder.dbConnection = (DbConnectionImpl) connection;
			return this;
		}

		@NonNull
		@CheckResult
		public final CompiledDelete compile() {
			return deleteBuilder.build();
		}

		@WorkerThread
		public final int execute() {
			return deleteBuilder.build().execute();
		}

		@NonNull
		@CheckResult
		public final Single<Integer> observe() {
			return deleteBuilder.build().observe();
		}
	}
}
