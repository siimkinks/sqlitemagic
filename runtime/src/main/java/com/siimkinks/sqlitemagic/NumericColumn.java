package com.siimkinks.sqlitemagic;

import android.support.annotation.CheckResult;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.siimkinks.sqlitemagic.Select.Select1;
import com.siimkinks.sqlitemagic.SelectSqlNode.SelectNode;
import com.siimkinks.sqlitemagic.Utils.ValueParser;

import static com.siimkinks.sqlitemagic.Table.ANONYMOUS_TABLE;
import static com.siimkinks.sqlitemagic.Utils.DOUBLE_PARSER;

/**
 * A numeric column used in queries and conditions.
 *
 * @param <T>  Exact type
 * @param <R>  Return type (when this column is queried)
 * @param <ET> Equivalent type
 * @param <P>  Parent table type
 */
public class NumericColumn<T, R, ET, P> extends Column<T, R, ET, P> {
	NumericColumn(@NonNull Table<P> table, @NonNull String name, boolean allFromTable,
	              @NonNull ValueParser<?> valueParser, boolean nullable, @Nullable String alias,
	              @NonNull String nameInQuery) {
		super(table, name, allFromTable, valueParser, nullable, alias, nameInQuery);
	}

	NumericColumn(@NonNull Table<P> table, @NonNull String name, boolean allFromTable,
	              @NonNull ValueParser<?> valueParser, boolean nullable, @Nullable String alias) {
		super(table, name, allFromTable, valueParser, nullable, alias);
	}

	@Override
	@NonNull
	@CheckResult
	public NumericColumn<T, R, ET, P> as(@NonNull String alias) {
		return new NumericColumn<>(table, name, allFromTable, valueParser, nullable, alias);
	}

	@NonNull
	@CheckResult
	public final Expr greaterThan(@NonNull T value) {
		return new Expr1(this, ">?", toSqlArg(value));
	}

	@NonNull
	@CheckResult
	public final <C extends NumericColumn<?, ?, ? extends ET, ?>> Expr greaterThan(@NonNull C column) {
		return new ExprC(this, ">", column);
	}

	@NonNull
	@CheckResult
	public final Expr greaterThan(@NonNull SelectNode<? extends ET, Select1> select) {
		return new ExprS(this, ">", select);
	}

	@NonNull
	@CheckResult
	public final Expr greaterOrEqual(@NonNull T value) {
		return new Expr1(this, ">=?", toSqlArg(value));
	}

	@NonNull
	@CheckResult
	public final <C extends NumericColumn<?, ?, ? extends ET, ?>> Expr greaterOrEqual(@NonNull C column) {
		return new ExprC(this, ">=", column);
	}

	@NonNull
	@CheckResult
	public final Expr greaterOrEqual(@NonNull SelectNode<? extends ET, Select1> select) {
		return new ExprS(this, ">=", select);
	}

	@NonNull
	@CheckResult
	public final Expr lessThan(@NonNull T value) {
		return new Expr1(this, "<?", toSqlArg(value));
	}

	@NonNull
	@CheckResult
	public final <C extends NumericColumn<?, ?, ? extends ET, ?>> Expr lessThan(@NonNull C column) {
		return new ExprC(this, "<", column);
	}

	@NonNull
	@CheckResult
	public final Expr lessThan(@NonNull SelectNode<? extends ET, Select1> select) {
		return new ExprS(this, "<", select);
	}

	@NonNull
	@CheckResult
	public final Expr lessOrEqual(@NonNull T value) {
		return new Expr1(this, "<=?", toSqlArg(value));
	}

	@NonNull
	@CheckResult
	public final <C extends NumericColumn<?, ?, ? extends ET, ?>> Expr lessOrEqual(@NonNull C column) {
		return new ExprC(this, "<=", column);
	}

	@NonNull
	@CheckResult
	public final Expr lessOrEqual(@NonNull SelectNode<? extends ET, Select1> select) {
		return new ExprS(this, "<=", select);
	}

	@NonNull
	@CheckResult
	public final Between<T, ET> between(@NonNull T value) {
		return new Between<>(this, value, null, false);
	}

	@NonNull
	@CheckResult
	public final <C extends NumericColumn<?, ?, ? extends ET, ?>> Between<T, ET> between(@NonNull C column) {
		return new Between<>(this, null, column, false);
	}

	@NonNull
	@CheckResult
	public final Between<T, ET> notBetween(@NonNull T value) {
		return new Between<>(this, value, null, true);
	}

	@NonNull
	@CheckResult
	public final <C extends NumericColumn<?, ?, ? extends ET, ?>> Between<T, ET> notBetween(@NonNull C column) {
		return new Between<>(this, null, column, true);
	}

	public static final class Between<T, ET> {
		@NonNull
		final Column<T, ?, ?, ?> column;
		@Nullable
		final T firstVal;
		@Nullable
		final Column<?, ?, ?, ?> firstColumn;
		final boolean not;

		Between(@NonNull Column<T, ?, ?, ?> column, @Nullable T firstVal,
		        @Nullable Column<?, ?, ?, ?> firstColumn, boolean not) {
			this.column = column;
			this.firstVal = firstVal;
			this.firstColumn = firstColumn;
			this.not = not;
		}

		@NonNull
		@CheckResult
		public final Expr and(@NonNull T value) {
			final Column<T, ?, ?, ?> column = this.column;
			return new BetweenExpr(column, firstVal != null ? column.toSqlArg(firstVal) : null,
					column.toSqlArg(value), firstColumn, null, not);
		}

		@NonNull
		@CheckResult
		public final <C extends NumericColumn<?, ?, ? extends ET, ?>> Expr and(@NonNull C column) {
			final Column<T, ?, ?, ?> baseColumn = this.column;
			return new BetweenExpr(baseColumn, firstVal != null ? baseColumn.toSqlArg(firstVal) : null,
					null, firstColumn, column, not);
		}
	}

	@NonNull
	@CheckResult
	public final <X extends NumericColumn<?, ?, ? extends Number, ?>> NumericColumn<Double, Double, Number, ?> add(@NonNull X column) {
		return new FunctionColumn<>(ANONYMOUS_TABLE, new Column[]{this, column}, "(", "+", ")", DOUBLE_PARSER, true, null);
	}

	@NonNull
	@CheckResult
	public final NumericColumn<Double, Double, Number, P> add(@NonNull T value) {
		return new FunctionColumn<>(table.internalAlias(""), this, "(", '+' + value.toString() + ')', DOUBLE_PARSER, true, null);
	}

	@NonNull
	@CheckResult
	public final <X extends NumericColumn<?, ?, ? extends Number, ?>> NumericColumn<Double, Double, Number, ?> sub(@NonNull X column) {
		return new FunctionColumn<>(ANONYMOUS_TABLE, new Column[]{this, column}, "(", "-", ")", DOUBLE_PARSER, true, null);
	}

	@NonNull
	@CheckResult
	public final NumericColumn<Double, Double, Number, P> sub(@NonNull T value) {
		return new FunctionColumn<>(table.internalAlias(""), this, "(", "-" + value.toString() + ")", DOUBLE_PARSER, true, null);
	}

	@NonNull
	@CheckResult
	public final <X extends NumericColumn<?, ?, ? extends Number, ?>> NumericColumn<Double, Double, Number, ?> mul(@NonNull X column) {
		return new FunctionColumn<>(ANONYMOUS_TABLE, new Column[]{this, column}, "(", "*", ")", DOUBLE_PARSER, true, null);
	}

	@NonNull
	@CheckResult
	public final NumericColumn<Double, Double, Number, P> mul(@NonNull T value) {
		return new FunctionColumn<>(table.internalAlias(""), this, "(", "*" + value.toString() + ")", DOUBLE_PARSER, true, null);
	}

	@NonNull
	@CheckResult
	public final <X extends NumericColumn<?, ?, ? extends Number, ?>> NumericColumn<Double, Double, Number, ?> div(@NonNull X column) {
		return new FunctionColumn<>(ANONYMOUS_TABLE, new Column[]{this, column}, "(", "/", ")", DOUBLE_PARSER, true, null);
	}

	@NonNull
	@CheckResult
	public final NumericColumn<Double, Double, Number, P> div(@NonNull T value) {
		return new FunctionColumn<>(table.internalAlias(""), this, "(", "/" + value.toString() + ")", DOUBLE_PARSER, true, null);
	}

	@NonNull
	@CheckResult
	public final <X extends NumericColumn<?, ?, ? extends Number, ?>> NumericColumn<Double, Double, Number, ?> mod(@NonNull X column) {
		return new FunctionColumn<>(ANONYMOUS_TABLE, new Column[]{this, column}, "(", "%", ")", DOUBLE_PARSER, true, null);
	}

	@NonNull
	@CheckResult
	public final NumericColumn<Double, Double, Number, P> mod(@NonNull T value) {
		return new FunctionColumn<>(table.internalAlias(""), this, "(", "%" + value.toString() + ")", DOUBLE_PARSER, true, null);
	}
}
