package com.siimkinks.sqlitemagic.util;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public final class ObjectPool<T> {
	private final Object lock = new Object();

	private final Object[] pool;

	private int currentSize;

	public ObjectPool(int maxPoolSize) {
		pool = new Object[maxPoolSize];
	}

	/**
	 * @return An instance from the pool if such, null otherwise.
	 */
	@Nullable
	@SuppressWarnings("unchecked")
	public T acquire() {
		synchronized (lock) {
			if (currentSize > 0) {
				final int lastPooledIndex = currentSize - 1;
				T instance = (T) pool[lastPooledIndex];
				pool[lastPooledIndex] = null;
				currentSize--;
				return instance;
			}
			return null;
		}
	}

	/**
	 * Release an instance to the pool.
	 *
	 * @param instance The instance to release.
	 * @return Whether the instance was put in the pool.
	 */
	public boolean release(@NonNull T instance) {
		synchronized (lock) {
			if (currentSize < pool.length) {
				pool[currentSize] = instance;
				currentSize++;
				return true;
			}
			return false;
		}
	}
}
