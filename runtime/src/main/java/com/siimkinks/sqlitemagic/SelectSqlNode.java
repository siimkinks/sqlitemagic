package com.siimkinks.sqlitemagic;

import android.support.annotation.CheckResult;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;

import com.siimkinks.sqlitemagic.entity.ConnectionProvidedOperation;

import java.util.List;

public abstract class SelectSqlNode<S> extends SqlNode {
	@NonNull
	final SelectBuilder<S> selectBuilder;

	SelectSqlNode(@Nullable SelectSqlNode<S> parent) {
		super(parent);
		final SelectBuilder<S> selectBuilder;
		if (parent != null) {
			selectBuilder = parent.selectBuilder;
		} else {
			selectBuilder = new SelectBuilder<>();
		}
		selectBuilder.sqlTreeRoot = this;
		selectBuilder.sqlNodeCount++;
		this.selectBuilder = selectBuilder;
	}

	public static abstract class SelectNode<T, S> extends SelectSqlNode<S> implements ConnectionProvidedOperation<SelectNode<T, S>> {
		SelectNode(SelectSqlNode<S> parent) {
			super(parent);
		}

		@NonNull
		@CheckResult
		public final NumericColumn<T, T, T, ?> asColumn(@NonNull String alias) {
			return SelectionColumn.from(selectBuilder, alias);
		}

		@NonNull
		@CheckResult
		public final SelectNode<T, S> queryDeep() {
			selectBuilder.deep = true;
			return this;
		}

		@Override
		public final SelectNode<T, S> usingConnection(@NonNull DbConnection connection) {
			selectBuilder.dbConnection = (DbConnectionImpl) connection;
			return this;
		}

		@NonNull
		@CheckResult
		public final CompiledSelect<T, S> compile() {
			return selectBuilder.build();
		}

		@NonNull
		@CheckResult
		public final CompiledFirstSelect<T, S> takeFirst() {
			return selectBuilder.<T>build().takeFirst();
		}

		@NonNull
		@CheckResult
		public final CompiledCursorSelect<T, S> toCursor() {
			return selectBuilder.<T>build().toCursor();
		}

		@NonNull
		@CheckResult
		public final CompiledCountSelect count() {
			return selectBuilder.<T>build().count();
		}

		@NonNull
		@CheckResult
		@WorkerThread
		public final List<T> execute() {
			return selectBuilder.<T>build().execute();
		}

		@NonNull
		@CheckResult
		public final QueryObservable<List<T>> observe() {
			return selectBuilder.<T>build().observe();
		}
	}
}
