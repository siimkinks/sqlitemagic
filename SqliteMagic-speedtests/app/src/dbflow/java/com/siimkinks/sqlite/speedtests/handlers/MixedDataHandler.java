package com.siimkinks.sqlite.speedtests.handlers;

import android.support.annotation.NonNull;

import com.raizlabs.android.dbflow.runtime.TransactionManager;
import com.raizlabs.android.dbflow.sql.language.Delete;
import com.raizlabs.android.dbflow.sql.language.Select;
import com.raizlabs.android.dbflow.sql.language.Where;
import com.siimkinks.sqlite.speedtests.DBFlowDatabase;
import com.siimkinks.sqlite.speedtests.data.DataHandler;
import com.siimkinks.sqlite.speedtests.data.MixedData;

import java.util.List;

public final class MixedDataHandler extends DataHandler<MixedData> {
	private static final Where<MixedData> QUERY_ALL = new Select().from(MixedData.class).where();
	private static final Where<MixedData> QUERY_FIRST = new Select().from(MixedData.class).where();
	public static final Where<MixedData> COUNT = new Select().count().from(MixedData.class).where();

	@Override
	public void deleteTable() {
		Delete.table(MixedData.class);
	}

	@Override
	public void bulkInsert(@NonNull List<MixedData> data) {
		TransactionManager.transact(DBFlowDatabase.NAME, () -> {
			for (MixedData o : data) {
				o.insert();
			}
		});
	}

	@Override
	public void bulkUpdate(@NonNull List<MixedData> data) {
		TransactionManager.transact(DBFlowDatabase.NAME, () -> {
			for (MixedData o : data) {
				o.update();
			}
		});
	}

	@Override
	public void bulkPersist(@NonNull List<MixedData> data) {
		TransactionManager.transact(DBFlowDatabase.NAME, () -> {
			for (MixedData o : data) {
				o.update();
			}
		});
	}

	@NonNull
	@Override
	public List<MixedData> queryAll() {
		return QUERY_ALL.queryList();
	}

	@NonNull
	@Override
	public MixedData queryFirst() {
		return QUERY_FIRST.querySingle();
	}

	@Override
	public long countAll() {
		return COUNT.count();
	}

	@Override
	public long countDependencies() {
		return 0;
	}

	@Override
	public long expectedDependenciesSize(int size) {
		return 0;
	}
}
