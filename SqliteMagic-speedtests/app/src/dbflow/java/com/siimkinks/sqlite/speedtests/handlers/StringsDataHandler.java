package com.siimkinks.sqlite.speedtests.handlers;

import android.support.annotation.NonNull;

import com.raizlabs.android.dbflow.runtime.TransactionManager;
import com.raizlabs.android.dbflow.sql.language.Delete;
import com.raizlabs.android.dbflow.sql.language.Select;
import com.raizlabs.android.dbflow.sql.language.Where;
import com.siimkinks.sqlite.speedtests.DBFlowDatabase;
import com.siimkinks.sqlite.speedtests.data.DataHandler;
import com.siimkinks.sqlite.speedtests.data.StringsData;

import java.util.List;

public final class StringsDataHandler extends DataHandler<StringsData> {
	private static final Where<StringsData> QUERY_ALL = new Select().from(StringsData.class).where();
	private static final Where<StringsData> QUERY_FIRST = new Select().from(StringsData.class).where();
	private static final Where<StringsData> COUNT = new Select().count().from(StringsData.class).where();

	@Override
	public void deleteTable() {
		Delete.table(StringsData.class);
	}

	@Override
	public void bulkInsert(@NonNull List<StringsData> data) {
		TransactionManager.transact(DBFlowDatabase.NAME, () -> {
			for (StringsData o : data) {
				o.insert();
			}
		});
	}

	@Override
	public void bulkUpdate(@NonNull List<StringsData> data) {
		TransactionManager.transact(DBFlowDatabase.NAME, () -> {
			for (StringsData o : data) {
				o.update();
			}
		});
	}

	@Override
	public void bulkPersist(@NonNull List<StringsData> data) {
		TransactionManager.transact(DBFlowDatabase.NAME, () -> {
			for (StringsData o : data) {
				o.update();
			}
		});
	}

	@NonNull
	@Override
	public List<StringsData> queryAll() {
		return QUERY_ALL.queryList();
	}

	@NonNull
	@Override
	public StringsData queryFirst() {
		return QUERY_FIRST.querySingle();
	}

	@Override
	public long countAll() {
		return COUNT.count();
	}

	@Override
	public long countDependencies() {
		return 0;
	}

	@Override
	public long expectedDependenciesSize(int size) {
		return 0;
	}
}
