package com.siimkinks.sqlite.speedtests.data;

import android.support.annotation.NonNull;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ForeignKey;
import com.raizlabs.android.dbflow.annotation.ForeignKeyReference;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;
import com.siimkinks.sqlite.speedtests.DBFlowDatabase;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
@Table(databaseName = DBFlowDatabase.NAME)
public final class ThirdLevel extends BaseModel {
	@PrimaryKey(autoincrement = true)
	@Column
	long id;

	@ForeignKey(references = {
			@ForeignKeyReference(
					columnName = "one_id",
					columnType = Long.class,
					foreignColumnName = "id")})
	@Column
	@NonNull
	SecondLevel one;

	@ForeignKey(references = {
			@ForeignKeyReference(
					columnName = "two_id",
					columnType = Long.class,
					foreignColumnName = "id")})
	@Column
	@NonNull
	SecondLevel two;
}
