package com.siimkinks.sqlite.speedtests.data;

import android.support.annotation.NonNull;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;
import com.siimkinks.sqlite.speedtests.DBFlowDatabase;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
@Table(allFields = true, databaseName = DBFlowDatabase.NAME)
public final class MixedData extends BaseModel {
	@PrimaryKey(autoincrement = true)
	@Column
	long id;

	@NonNull
	String one;
	@NonNull
	String two;
	String three; // null
	String four; // null
	long oneL;
	long twoL;
	@NonNull
	Long threeL;
	Long fourL; // null
	double oneD;
	double twoD;
	@NonNull
	Double threeD;
	Double fourD; // null
}
