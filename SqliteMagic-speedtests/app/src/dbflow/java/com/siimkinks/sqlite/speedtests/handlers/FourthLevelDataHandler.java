package com.siimkinks.sqlite.speedtests.handlers;

import android.support.annotation.NonNull;

import com.raizlabs.android.dbflow.runtime.TransactionManager;
import com.raizlabs.android.dbflow.sql.language.Delete;
import com.raizlabs.android.dbflow.sql.language.Select;
import com.raizlabs.android.dbflow.sql.language.Where;
import com.siimkinks.sqlite.speedtests.DBFlowDatabase;
import com.siimkinks.sqlite.speedtests.data.DataHandler;
import com.siimkinks.sqlite.speedtests.data.FirstLevel;
import com.siimkinks.sqlite.speedtests.data.FourthLevel;
import com.siimkinks.sqlite.speedtests.data.MixedData;
import com.siimkinks.sqlite.speedtests.data.SecondLevel;
import com.siimkinks.sqlite.speedtests.data.ThirdLevel;

import java.util.List;

public final class FourthLevelDataHandler extends DataHandler<FourthLevel> {
	private static final Where<FourthLevel> QUERY_ALL = new Select().from(FourthLevel.class).where();
	private static final Where<FourthLevel> QUERY_FIRST = new Select().from(FourthLevel.class).where();
	private static final Where<FourthLevel> COUNT = new Select().count().from(FourthLevel.class).where();

	@Override
	public void deleteTable() {
		Delete.tables(MixedData.class, FirstLevel.class, SecondLevel.class, ThirdLevel.class, FourthLevel.class);
	}

	@Override
	public void bulkInsert(@NonNull List<FourthLevel> data) {
		TransactionManager.transact(DBFlowDatabase.NAME, () -> {
			for (FourthLevel o : data) {
				o.insert();
			}
		});
	}

	@Override
	public void bulkUpdate(@NonNull List<FourthLevel> data) {
		TransactionManager.transact(DBFlowDatabase.NAME, () -> {
			for (FourthLevel o : data) {
				o.update();
			}
		});
	}

	@Override
	public void bulkPersist(@NonNull List<FourthLevel> data) {
		TransactionManager.transact(DBFlowDatabase.NAME, () -> {
			for (FourthLevel o : data) {
				o.update();
			}
		});
	}

	@NonNull
	@Override
	public List<FourthLevel> queryAll() {
		return QUERY_ALL.queryList();
	}

	@NonNull
	@Override
	public FourthLevel queryFirst() {
		return QUERY_FIRST.querySingle();
	}

	@Override
	public long countAll() {
		return COUNT.count();
	}

	@Override
	public long countDependencies() {
		return MixedDataHandler.COUNT.count() +
				FirstLevelDataHandler.COUNT.count() +
				new Select().count().from(SecondLevel.class).where().count() +
				new Select().count().from(ThirdLevel.class).where().count();
	}

	@Override
	public long expectedDependenciesSize(int size) {
		return size * 30;
	}
}
