package com.siimkinks.sqlite.speedtests;

import com.raizlabs.android.dbflow.annotation.Database;

@Database(name = DBFlowDatabase.NAME, version = DBFlowDatabase.VERSION)
public class DBFlowDatabase {

	public static final String NAME = "dbflow";

	public static final int VERSION = 1;
}
