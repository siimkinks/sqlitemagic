package com.siimkinks.sqlite.speedtests.data;

import android.support.annotation.NonNull;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;
import com.siimkinks.sqlite.speedtests.DBFlowDatabase;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
@Table(allFields = true, databaseName = DBFlowDatabase.NAME)
public final class LongMixedData extends BaseModel {
	@PrimaryKey(autoincrement = true)
	@Column
	long id;

	@NonNull
	String one;
	@NonNull
	String two;
	String three; // null
	String four; // null
	@NonNull
	String five;
	@NonNull
	String six;
	String seven; // null
	String eight; // null
	@NonNull
	String nine;
	@NonNull
	String ten;
	String eleven; // null
	String twelve; // null
	long oneL;
	long twoL;
	@NonNull
	Long threeL;
	Long fourL; // null
	long fiveL;
	long sixL;
	@NonNull
	Long sevenL;
	Long eightL; // null
	double oneD;
	double twoD;
	@NonNull
	Double threeD;
	Double fourD; // null
	double fiveD;
	double sixD;
	@NonNull
	Double sevenD;
	Double eightD; // null
	int oneI;
	int twoI;
	@NonNull
	Integer threeI;
	Integer fourI; // null
	int fiveI;
	int sixI;
	@NonNull
	Integer sevenI;
	Integer eightI; // null
	float oneF;
	float twoF;
	@NonNull
	Float threeF;
	Float fourF; // null
	float fiveF;
	float sixF;
	@NonNull
	Float sevenF;
	Float eightF; // null
}
