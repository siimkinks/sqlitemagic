package com.siimkinks.sqlite.speedtests;

import com.raizlabs.android.dbflow.config.FlowManager;

public class LibInitializer implements Initializer {
	@Override
	public void init(App app) {
		FlowManager.init(app);
	}

	@Override
	public void destroy(App app) {
	}
}
