package com.siimkinks.sqlite.speedtests.data;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;
import com.siimkinks.sqlite.speedtests.DBFlowDatabase;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
@Table(allFields = true, databaseName = DBFlowDatabase.NAME)
public final class PingData extends BaseModel {
	@PrimaryKey(autoincrement = true)
	@Column
	long id;

	String test;
}
