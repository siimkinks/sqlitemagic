package com.siimkinks.sqlite.speedtests.handlers;

import android.support.annotation.NonNull;

import com.raizlabs.android.dbflow.runtime.TransactionManager;
import com.raizlabs.android.dbflow.sql.language.Delete;
import com.raizlabs.android.dbflow.sql.language.Select;
import com.raizlabs.android.dbflow.sql.language.Where;
import com.siimkinks.sqlite.speedtests.DBFlowDatabase;
import com.siimkinks.sqlite.speedtests.data.DataHandler;
import com.siimkinks.sqlite.speedtests.data.FirstLevel;
import com.siimkinks.sqlite.speedtests.data.MixedData;

import java.util.List;

public final class FirstLevelDataHandler extends DataHandler<FirstLevel> {
	private static final Where<FirstLevel> QUERY_ALL = new Select().from(FirstLevel.class).where();
	private static final Where<FirstLevel> QUERY_FIRST = new Select().from(FirstLevel.class).where();
	public static final Where<FirstLevel> COUNT = new Select().count().from(FirstLevel.class).where();

	@Override
	public void deleteTable() {
		Delete.tables(MixedData.class, FirstLevel.class);
	}

	@Override
	public void bulkInsert(@NonNull List<FirstLevel> data) {
		TransactionManager.transact(DBFlowDatabase.NAME, () -> {
			for (FirstLevel o : data) {
				o.insert();
			}
		});
	}

	@Override
	public void bulkUpdate(@NonNull List<FirstLevel> data) {
		TransactionManager.transact(DBFlowDatabase.NAME, () -> {
			for (FirstLevel o : data) {
				o.update();
			}
		});
	}

	@Override
	public void bulkPersist(@NonNull List<FirstLevel> data) {
		TransactionManager.transact(DBFlowDatabase.NAME, () -> {
			for (FirstLevel o : data) {
				o.update();
			}
		});
	}

	@NonNull
	@Override
	public List<FirstLevel> queryAll() {
		return QUERY_ALL.queryList();
	}

	@NonNull
	@Override
	public FirstLevel queryFirst() {
		return QUERY_FIRST.querySingle();
	}

	@Override
	public long countAll() {
		return COUNT.count();
	}

	@Override
	public long countDependencies() {
		return MixedDataHandler.COUNT.count();
	}

	@Override
	public long expectedDependenciesSize(int size) {
		return size * 2;
	}
}
