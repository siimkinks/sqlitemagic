package com.siimkinks.sqlite.speedtests.handlers;

import android.support.annotation.NonNull;

import com.raizlabs.android.dbflow.runtime.TransactionManager;
import com.raizlabs.android.dbflow.sql.language.Delete;
import com.raizlabs.android.dbflow.sql.language.Select;
import com.raizlabs.android.dbflow.sql.language.Where;
import com.siimkinks.sqlite.speedtests.DBFlowDatabase;
import com.siimkinks.sqlite.speedtests.data.DataHandler;
import com.siimkinks.sqlite.speedtests.data.LongMixedData;

import java.util.List;

public final class LongMixedDataHandler extends DataHandler<LongMixedData> {
	private static final Where<LongMixedData> QUERY_ALL = new Select().from(LongMixedData.class).where();
	private static final Where<LongMixedData> QUERY_FIRST = new Select().from(LongMixedData.class).where();
	private static final Where<LongMixedData> COUNT = new Select().count().from(LongMixedData.class).where();

	@Override
	public void deleteTable() {
		Delete.table(LongMixedData.class);
	}

	@Override
	public void bulkInsert(@NonNull List<LongMixedData> data) {
		TransactionManager.transact(DBFlowDatabase.NAME, () -> {
			for (LongMixedData o : data) {
				o.insert();
			}
		});
	}

	@Override
	public void bulkUpdate(@NonNull List<LongMixedData> data) {
		TransactionManager.transact(DBFlowDatabase.NAME, () -> {
			for (LongMixedData o : data) {
				o.update();
			}
		});
	}

	@Override
	public void bulkPersist(@NonNull List<LongMixedData> data) {
		TransactionManager.transact(DBFlowDatabase.NAME, () -> {
			for (LongMixedData o : data) {
				o.update();
			}
		});
	}

	@NonNull
	@Override
	public List<LongMixedData> queryAll() {
		return QUERY_ALL.queryList();
	}

	@NonNull
	@Override
	public LongMixedData queryFirst() {
		return QUERY_FIRST.querySingle();
	}

	@Override
	public long countAll() {
		return COUNT.count();
	}

	@Override
	public long countDependencies() {
		return 0;
	}

	@Override
	public long expectedDependenciesSize(int size) {
		return 0;
	}
}
