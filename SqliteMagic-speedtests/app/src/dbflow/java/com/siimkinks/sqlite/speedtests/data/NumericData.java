package com.siimkinks.sqlite.speedtests.data;

import android.support.annotation.NonNull;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;
import com.siimkinks.sqlite.speedtests.DBFlowDatabase;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
@Table(allFields = true, databaseName = DBFlowDatabase.NAME)
public final class NumericData extends BaseModel {
	@PrimaryKey(autoincrement = true)
	@Column
	long id;

	int primitiveInt;
	@NonNull
	Integer boxedInt;
	long primitiveLong;
	@NonNull
	Long boxedLong;
	boolean primitiveBoolean;
	@NonNull
	Boolean boxedBoolean;
	float primitiveFloat;
	@NonNull
	Float boxedFloat;
	double primitiveDouble;
	@NonNull
	Double boxedDouble;
	short primitiveShort;
	@NonNull
	Short boxedShort;
}
