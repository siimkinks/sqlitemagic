package com.siimkinks.sqlite.speedtests.handlers;


import android.support.annotation.NonNull;

import com.raizlabs.android.dbflow.runtime.TransactionManager;
import com.raizlabs.android.dbflow.sql.language.Delete;
import com.raizlabs.android.dbflow.sql.language.Select;
import com.raizlabs.android.dbflow.sql.language.Where;
import com.siimkinks.sqlite.speedtests.DBFlowDatabase;
import com.siimkinks.sqlite.speedtests.data.DataHandler;
import com.siimkinks.sqlite.speedtests.data.NumericData;

import java.util.List;

public final class NumericDataHandler extends DataHandler<NumericData> {
	private static final Where<NumericData> QUERY_ALL = new Select().from(NumericData.class).where();
	private static final Where<NumericData> QUERY_FIRST = new Select().from(NumericData.class).where();
	private static final Where<NumericData> COUNT = new Select().count().from(NumericData.class).where();

	@Override
	public void deleteTable() {
		Delete.table(NumericData.class);
	}

	@Override
	public void bulkInsert(@NonNull List<NumericData> data) {
		TransactionManager.transact(DBFlowDatabase.NAME, () -> {
			for (NumericData o : data) {
				o.insert();
			}
		});
	}

	@Override
	public void bulkUpdate(@NonNull List<NumericData> data) {
		TransactionManager.transact(DBFlowDatabase.NAME, () -> {
			for (NumericData o : data) {
				o.update();
			}
		});
	}

	@Override
	public void bulkPersist(@NonNull List<NumericData> data) {
		TransactionManager.transact(DBFlowDatabase.NAME, () -> {
			for (NumericData o : data) {
				o.update();
			}
		});
	}

	@NonNull
	@Override
	public List<NumericData> queryAll() {
		return QUERY_ALL.queryList();
	}

	@NonNull
	@Override
	public NumericData queryFirst() {
		return QUERY_FIRST.querySingle();
	}

	@Override
	public long countAll() {
		return COUNT.count();
	}

	@Override
	public long countDependencies() {
		return 0;
	}

	@Override
	public long expectedDependenciesSize(int size) {
		return 0;
	}
}
