package com.siimkinks.sqlite.speedtests.data;

import android.support.annotation.NonNull;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;
import com.siimkinks.sqlite.speedtests.DBFlowDatabase;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
@Table(allFields = true, databaseName = DBFlowDatabase.NAME)
public final class StringsData extends BaseModel {
	@PrimaryKey(autoincrement = true)
	@Column
	long id;

	@NonNull
	String one;
	@NonNull
	String two;
	@NonNull
	String three;
	@NonNull
	String four;
	@NonNull
	String five;
	@NonNull
	String six;
	@NonNull
	String seven;
	@NonNull
	String eight;
	@NonNull
	String nine;
	@NonNull
	String ten;
	@NonNull
	String eleven;
	@NonNull
	String twelve;
}
