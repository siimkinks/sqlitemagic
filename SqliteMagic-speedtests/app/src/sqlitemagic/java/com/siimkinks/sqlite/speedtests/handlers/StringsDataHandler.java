package com.siimkinks.sqlite.speedtests.handlers;

import android.support.annotation.NonNull;

import com.siimkinks.sqlite.speedtests.data.DataHandler;
import com.siimkinks.sqlite.speedtests.data.StringsData;
import com.siimkinks.sqlitemagic.CompiledSelect;
import com.siimkinks.sqlitemagic.Select;

import java.util.List;

public final class StringsDataHandler extends DataHandler<StringsData> {
	private static final CompiledSelect<StringsData> QUERY_ALL = Select.from(StringsData.class).compile();
	private static final CompiledSelect.CompiledFirstSelect<StringsData> QUERY_FIRST = QUERY_ALL.takeFirst();
	private static final CompiledSelect.CompiledCountSelect COUNT = QUERY_ALL.count();

	@Override
	public void deleteTable() {
		StringsData.deleteTable().execute();
	}

	@Override
	public void bulkInsert(@NonNull List<StringsData> data) {
		StringsData.insert(data).execute();
	}

	@Override
	public void bulkUpdate(@NonNull List<StringsData> data) {
		StringsData.update(data).execute();
	}

	@Override
	public void bulkPersist(@NonNull List<StringsData> data) {
		StringsData.persist(data).execute();
	}

	@NonNull
	@Override
	public List<StringsData> queryAll() {
		return QUERY_ALL.execute();
	}

	@NonNull
	@Override
	public StringsData queryFirst() {
		return QUERY_FIRST.execute();
	}

	@Override
	public long countAll() {
		return COUNT.execute();
	}

	@Override
	public long countDependencies() {
		return 0;
	}

	@Override
	public long expectedDependenciesSize(int size) {
		return 0;
	}
}
