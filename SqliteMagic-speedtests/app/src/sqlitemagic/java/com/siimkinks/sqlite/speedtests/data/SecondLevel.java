package com.siimkinks.sqlite.speedtests.data;

import android.support.annotation.NonNull;

import com.siimkinks.sqlitemagic.annotation.Id;
import com.siimkinks.sqlitemagic.annotation.Table;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
@Table(persistAll = true)
public final class SecondLevel {
	@Id
	long id;
	@NonNull
	FirstLevel one;
	@NonNull
	FirstLevel two;
}
