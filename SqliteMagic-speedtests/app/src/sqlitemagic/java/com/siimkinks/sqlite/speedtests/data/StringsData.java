package com.siimkinks.sqlite.speedtests.data;

import android.support.annotation.NonNull;

import com.siimkinks.sqlitemagic.annotation.Id;
import com.siimkinks.sqlitemagic.annotation.Table;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
@Table(persistAll = true)
public final class StringsData {
	@Id
	long id;
	@NonNull
	String one;
	@NonNull
	String two;
	@NonNull
	String three;
	@NonNull
	String four;
	@NonNull
	String five;
	@NonNull
	String six;
	@NonNull
	String seven;
	@NonNull
	String eight;
	@NonNull
	String nine;
	@NonNull
	String ten;
	@NonNull
	String eleven;
	@NonNull
	String twelve;
}
