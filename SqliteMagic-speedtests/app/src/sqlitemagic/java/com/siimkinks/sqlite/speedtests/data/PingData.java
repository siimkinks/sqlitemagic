package com.siimkinks.sqlite.speedtests.data;

import com.siimkinks.sqlitemagic.annotation.Table;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
@Table(persistAll = true)
public final class PingData {
	String test;
}
