package com.siimkinks.sqlite.speedtests.handlers;

import android.support.annotation.NonNull;

import com.siimkinks.sqlite.speedtests.data.DataHandler;
import com.siimkinks.sqlite.speedtests.data.MixedData;
import com.siimkinks.sqlitemagic.CompiledSelect;
import com.siimkinks.sqlitemagic.Select;

import java.util.List;

public final class MixedDataHandler extends DataHandler<MixedData> {
	private static final CompiledSelect<MixedData> QUERY_ALL = Select.from(MixedData.class).compile();
	private static final CompiledSelect.CompiledFirstSelect<MixedData> QUERY_FIRST = QUERY_ALL.takeFirst();
	public static final CompiledSelect.CompiledCountSelect COUNT = QUERY_ALL.count();

	@Override
	public void deleteTable() {
		MixedData.deleteTable().execute();
	}

	@Override
	public void bulkInsert(@NonNull List<MixedData> data) {
		MixedData.insert(data).execute();
	}

	@Override
	public void bulkUpdate(@NonNull List<MixedData> data) {
		MixedData.update(data).execute();
	}

	@Override
	public void bulkPersist(@NonNull List<MixedData> data) {
		MixedData.persist(data).execute();
	}

	@NonNull
	@Override
	public List<MixedData> queryAll() {
		return QUERY_ALL.execute();
	}

	@NonNull
	@Override
	public MixedData queryFirst() {
		return QUERY_FIRST.execute();
	}

	@Override
	public long countAll() {
		return COUNT.execute();
	}

	@Override
	public long countDependencies() {
		return 0;
	}

	@Override
	public long expectedDependenciesSize(int size) {
		return 0;
	}
}
