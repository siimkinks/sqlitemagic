package com.siimkinks.sqlite.speedtests.handlers;

import android.support.annotation.NonNull;

import com.siimkinks.sqlite.speedtests.data.DataHandler;
import com.siimkinks.sqlite.speedtests.data.FirstLevel;
import com.siimkinks.sqlite.speedtests.data.MixedData;
import com.siimkinks.sqlitemagic.CompiledSelect;
import com.siimkinks.sqlitemagic.Select;

import java.util.List;

public final class FirstLevelDataHandler extends DataHandler<FirstLevel> {
	private static final CompiledSelect<FirstLevel> QUERY_ALL = Select.from(FirstLevel.class).queryDeep().compile();
	private static final CompiledSelect.CompiledFirstSelect<FirstLevel> QUERY_FIRST = QUERY_ALL.takeFirst();
	public static final CompiledSelect.CompiledCountSelect COUNT = Select.from(FirstLevel.class).count();

	@Override
	public void deleteTable() {
		MixedData.deleteTable().execute();
		FirstLevel.deleteTable().execute();
	}

	@Override
	public void bulkInsert(@NonNull List<FirstLevel> data) {
		FirstLevel.insert(data).execute();
	}

	@Override
	public void bulkUpdate(@NonNull List<FirstLevel> data) {
		FirstLevel.update(data).execute();
	}

	@Override
	public void bulkPersist(@NonNull List<FirstLevel> data) {
		FirstLevel.persist(data).execute();
	}

	@NonNull
	@Override
	public List<FirstLevel> queryAll() {
		return QUERY_ALL.execute();
	}

	@NonNull
	@Override
	public FirstLevel queryFirst() {
		return QUERY_FIRST.execute();
	}

	@Override
	public long countAll() {
		return COUNT.execute();
	}

	@Override
	public long countDependencies() {
		return MixedDataHandler.COUNT.execute();
	}

	@Override
	public long expectedDependenciesSize(int size) {
		return size * 2;
	}
}
