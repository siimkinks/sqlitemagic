package com.siimkinks.sqlite.speedtests.handlers;

import com.siimkinks.sqlite.speedtests.data.PingData;

public final class PingDataHandler {
	public static void insert(PingData pingData) {
		pingData.insert().execute();
	}
}
