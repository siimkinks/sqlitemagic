package com.siimkinks.sqlite.speedtests.handlers;

import android.support.annotation.NonNull;

import com.siimkinks.sqlite.speedtests.data.DataHandler;
import com.siimkinks.sqlite.speedtests.data.FirstLevel;
import com.siimkinks.sqlite.speedtests.data.FourthLevel;
import com.siimkinks.sqlite.speedtests.data.MixedData;
import com.siimkinks.sqlite.speedtests.data.SecondLevel;
import com.siimkinks.sqlite.speedtests.data.ThirdLevel;
import com.siimkinks.sqlitemagic.CompiledSelect;
import com.siimkinks.sqlitemagic.Select;

import java.util.List;

public final class FourthLevelDataHandler extends DataHandler<FourthLevel> {
	private static final CompiledSelect<FourthLevel> QUERY_ALL = Select.from(FourthLevel.class).queryDeep().compile();
	private static final CompiledSelect.CompiledFirstSelect<FourthLevel> QUERY_FIRST = QUERY_ALL.takeFirst();
	private static final CompiledSelect.CompiledCountSelect COUNT = Select.from(FourthLevel.class).count();

	@Override
	public void deleteTable() {
		MixedData.deleteTable().execute();
		FirstLevel.deleteTable().execute();
		SecondLevel.deleteTable().execute();
		ThirdLevel.deleteTable().execute();
		FourthLevel.deleteTable().execute();
	}

	@Override
	public void bulkInsert(@NonNull List<FourthLevel> data) {
		FourthLevel.insert(data).execute();
	}

	@Override
	public void bulkUpdate(@NonNull List<FourthLevel> data) {
		FourthLevel.update(data).execute();
	}

	@Override
	public void bulkPersist(@NonNull List<FourthLevel> data) {
		FourthLevel.persist(data).execute();
	}

	@NonNull
	@Override
	public List<FourthLevel> queryAll() {
		return QUERY_ALL.execute();
	}

	@NonNull
	@Override
	public FourthLevel queryFirst() {
		return QUERY_FIRST.execute();
	}

	@Override
	public long countAll() {
		return COUNT.execute();
	}

	@Override
	public long countDependencies() {
		return MixedDataHandler.COUNT.execute() +
				FirstLevelDataHandler.COUNT.execute() +
				Select.from(SecondLevel.class).count().execute() +
				Select.from(ThirdLevel.class).count().execute();
	}

	@Override
	public long expectedDependenciesSize(int size) {
		return size * 30;
	}

}
