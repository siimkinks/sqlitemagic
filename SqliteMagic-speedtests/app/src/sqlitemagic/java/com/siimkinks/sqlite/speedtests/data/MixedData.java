package com.siimkinks.sqlite.speedtests.data;

import android.support.annotation.NonNull;

import com.siimkinks.sqlitemagic.annotation.Id;
import com.siimkinks.sqlitemagic.annotation.Table;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
@Table(persistAll = true)
public final class MixedData {
	@Id
	long id;
	@NonNull
	String one;
	@NonNull
	String two;
	String three; // null
	String four; // null
	long oneL;
	long twoL;
	@NonNull
	Long threeL;
	Long fourL; // null
	double oneD;
	double twoD;
	@NonNull
	Double threeD;
	Double fourD; // null
}
