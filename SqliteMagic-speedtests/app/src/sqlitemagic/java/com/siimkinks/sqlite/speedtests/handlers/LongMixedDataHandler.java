package com.siimkinks.sqlite.speedtests.handlers;

import android.support.annotation.NonNull;

import com.siimkinks.sqlite.speedtests.data.DataHandler;
import com.siimkinks.sqlite.speedtests.data.LongMixedData;
import com.siimkinks.sqlitemagic.CompiledSelect;
import com.siimkinks.sqlitemagic.Select;

import java.util.List;

public final class LongMixedDataHandler extends DataHandler<LongMixedData> {
	private static final CompiledSelect<LongMixedData> QUERY_ALL = Select.from(LongMixedData.class).compile();
	private static final CompiledSelect.CompiledFirstSelect<LongMixedData> QUERY_FIRST = QUERY_ALL.takeFirst();
	private static final CompiledSelect.CompiledCountSelect COUNT = QUERY_ALL.count();

	@Override
	public void deleteTable() {
		LongMixedData.deleteTable().execute();
	}

	@Override
	public void bulkInsert(@NonNull List<LongMixedData> data) {
		LongMixedData.insert(data).execute();
	}

	@Override
	public void bulkUpdate(@NonNull List<LongMixedData> data) {
		LongMixedData.update(data).execute();
	}

	@Override
	public void bulkPersist(@NonNull List<LongMixedData> data) {
		LongMixedData.persist(data).execute();
	}

	@NonNull
	@Override
	public List<LongMixedData> queryAll() {
		return QUERY_ALL.execute();
	}

	@NonNull
	@Override
	public LongMixedData queryFirst() {
		return QUERY_FIRST.execute();
	}

	@Override
	public long countAll() {
		return COUNT.execute();
	}

	@Override
	public long countDependencies() {
		return 0;
	}

	@Override
	public long expectedDependenciesSize(int size) {
		return 0;
	}
}
