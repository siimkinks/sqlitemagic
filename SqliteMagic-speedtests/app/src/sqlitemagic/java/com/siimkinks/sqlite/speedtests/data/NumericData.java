package com.siimkinks.sqlite.speedtests.data;

import android.support.annotation.NonNull;

import com.siimkinks.sqlitemagic.annotation.Id;
import com.siimkinks.sqlitemagic.annotation.Table;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
@Table(persistAll = true)
public final class NumericData {
	@Id
	long id;
	int primitiveInt;
	@NonNull
	Integer boxedInt;
	long primitiveLong;
	@NonNull
	Long boxedLong;
	boolean primitiveBoolean;
	@NonNull
	Boolean boxedBoolean;
	float primitiveFloat;
	@NonNull
	Float boxedFloat;
	double primitiveDouble;
	@NonNull
	Double boxedDouble;
	short primitiveShort;
	@NonNull
	Short boxedShort;
}
