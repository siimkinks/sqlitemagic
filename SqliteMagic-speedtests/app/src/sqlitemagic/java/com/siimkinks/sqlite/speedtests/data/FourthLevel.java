package com.siimkinks.sqlite.speedtests.data;

import android.support.annotation.NonNull;

import com.siimkinks.sqlitemagic.annotation.Id;
import com.siimkinks.sqlitemagic.annotation.Table;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
@Table(persistAll = true)
public final class FourthLevel {
	@Id
	long id;
	@NonNull
	ThirdLevel one;
	@NonNull
	ThirdLevel two;
}
