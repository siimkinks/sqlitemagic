package com.siimkinks.sqlite.speedtests;

import com.siimkinks.sqlitemagic.SqliteMagic;

public class LibInitializer implements Initializer {

	public void init(App app) {
		SqliteMagic
				.setupDatabase()
				.withName("db.db")
				.init(app);
	}

	@Override
	public void destroy(App app) {
	}
}
