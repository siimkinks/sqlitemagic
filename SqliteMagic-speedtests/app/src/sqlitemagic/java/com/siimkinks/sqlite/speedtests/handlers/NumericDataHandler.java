package com.siimkinks.sqlite.speedtests.handlers;


import android.support.annotation.NonNull;

import com.siimkinks.sqlite.speedtests.data.DataHandler;
import com.siimkinks.sqlite.speedtests.data.NumericData;
import com.siimkinks.sqlitemagic.CompiledSelect;
import com.siimkinks.sqlitemagic.Select;

import java.util.List;

public final class NumericDataHandler extends DataHandler<NumericData> {
	private static final CompiledSelect<NumericData> QUERY_ALL = Select.from(NumericData.class).compile();
	private static final CompiledSelect.CompiledFirstSelect<NumericData> QUERY_FIRST = QUERY_ALL.takeFirst();
	private static final CompiledSelect.CompiledCountSelect COUNT = QUERY_ALL.count();

	@Override
	public void deleteTable() {
		NumericData.deleteTable().execute();
	}

	@Override
	public void bulkInsert(@NonNull List<NumericData> data) {
		NumericData.insert(data).execute();
	}

	@Override
	public void bulkUpdate(@NonNull List<NumericData> data) {
		NumericData.update(data).execute();
	}

	@Override
	public void bulkPersist(@NonNull List<NumericData> data) {
		NumericData.persist(data).execute();
	}

	@NonNull
	@Override
	public List<NumericData> queryAll() {
		return QUERY_ALL.execute();
	}

	@NonNull
	@Override
	public NumericData queryFirst() {
		return QUERY_FIRST.execute();
	}

	@Override
	public long countAll() {
		return COUNT.execute();
	}

	@Override
	public long countDependencies() {
		return 0;
	}

	@Override
	public long expectedDependenciesSize(int size) {
		return 0;
	}
}
