package com.siimkinks.sqlite.speedtests.handlers;

import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import com.siimkinks.sqlite.speedtests.LibInitializer;
import com.siimkinks.sqlite.speedtests.data.DaoSession;
import com.siimkinks.sqlite.speedtests.data.DataHandler;
import com.siimkinks.sqlite.speedtests.data.FirstLevel;
import com.siimkinks.sqlite.speedtests.data.FirstLevelDao;
import com.siimkinks.sqlite.speedtests.data.FourthLevel;
import com.siimkinks.sqlite.speedtests.data.FourthLevelDao;
import com.siimkinks.sqlite.speedtests.data.MixedDataDao;
import com.siimkinks.sqlite.speedtests.data.SecondLevel;
import com.siimkinks.sqlite.speedtests.data.SecondLevelDao;
import com.siimkinks.sqlite.speedtests.data.ThirdLevel;
import com.siimkinks.sqlite.speedtests.data.ThirdLevelDao;

import java.util.List;

import de.greenrobot.dao.identityscope.IdentityScopeType;

import static com.siimkinks.sqlite.speedtests.modifier.DataModifier.modifyFirstLevelData;
import static com.siimkinks.sqlite.speedtests.modifier.DataModifier.modifyFourthLevelData;
import static com.siimkinks.sqlite.speedtests.modifier.DataModifier.modifySecondLevelData;
import static com.siimkinks.sqlite.speedtests.modifier.DataModifier.modifyThirdLevelData;

public final class FourthLevelDataHandler extends DataHandler<FourthLevel> {
	public static FourthLevelDao getDao() {
		final DaoSession daoSession = getDaoSession();
		return daoSession.getFourthLevelDao();
	}

	private static DaoSession getDaoSession() {
		return LibInitializer.daoMaster.newSession(IdentityScopeType.None);
	}

	@Override
	public void deleteTable() {
		final DaoSession daoSession = getDaoSession();
		MixedDataHandler.getDao().deleteAll();
		daoSession.getFirstLevelDao().deleteAll();
		daoSession.getSecondLevelDao().deleteAll();
		daoSession.getThirdLevelDao().deleteAll();
		getDao().deleteAll();
	}

	@Override
	public void bulkInsert(@NonNull List<FourthLevel> data) {
		final DaoSession daoSession = getDaoSession();
		final FourthLevelDao fourthDao = daoSession.getFourthLevelDao();
		final ThirdLevelDao thirdDao = daoSession.getThirdLevelDao();
		final SecondLevelDao secondDao = daoSession.getSecondLevelDao();
		final FirstLevelDao firstDao = daoSession.getFirstLevelDao();
		final MixedDataDao mixedDao = daoSession.getMixedDataDao();
		final SQLiteDatabase db = daoSession.getDatabase();
		db.beginTransaction();
		try {
			for (FourthLevel o : data) {
				insertThirdLevel(o.one, thirdDao, secondDao, firstDao, mixedDao);
				insertThirdLevel(o.two, thirdDao, secondDao, firstDao, mixedDao);
				modifyFourthLevelData(o);
				fourthDao.insert(o);
			}
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}

	private void insertThirdLevel(ThirdLevel o, ThirdLevelDao thirdDao, SecondLevelDao secondDao, FirstLevelDao firstDao, MixedDataDao mixedDao) {
		insertSecondLevel(o.one, secondDao, firstDao, mixedDao);
		insertSecondLevel(o.two, secondDao, firstDao, mixedDao);
		modifyThirdLevelData(o);
		thirdDao.insert(o);
	}

	private void insertSecondLevel(SecondLevel o, SecondLevelDao secondDao, FirstLevelDao firstDao, MixedDataDao mixedDao) {
		insertFirstLevel(o.one, firstDao, mixedDao);
		insertFirstLevel(o.two, firstDao, mixedDao);
		modifySecondLevelData(o);
		secondDao.insert(o);
	}

	private void insertFirstLevel(FirstLevel o, FirstLevelDao firstDao, MixedDataDao mixedDao) {
		mixedDao.insert(o.one);
		mixedDao.insert(o.two);
		modifyFirstLevelData(o);
		firstDao.insert(o);
	}

	@Override
	public void bulkUpdate(@NonNull List<FourthLevel> data) {
		final DaoSession daoSession = getDaoSession();
		final FourthLevelDao fourthDao = daoSession.getFourthLevelDao();
		final ThirdLevelDao thirdDao = daoSession.getThirdLevelDao();
		final SecondLevelDao secondDao = daoSession.getSecondLevelDao();
		final FirstLevelDao firstDao = daoSession.getFirstLevelDao();
		final MixedDataDao mixedDao = daoSession.getMixedDataDao();
		final SQLiteDatabase db = daoSession.getDatabase();
		db.beginTransaction();
		try {
			for (FourthLevel o : data) {
				updateThirdLevel(o.one, thirdDao, secondDao, firstDao, mixedDao);
				updateThirdLevel(o.two, thirdDao, secondDao, firstDao, mixedDao);
				fourthDao.update(o);
			}
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}

	private void updateThirdLevel(ThirdLevel o, ThirdLevelDao thirdDao, SecondLevelDao secondDao, FirstLevelDao firstDao, MixedDataDao mixedDao) {
		updateSecondLevel(o.one, secondDao, firstDao, mixedDao);
		updateSecondLevel(o.two, secondDao, firstDao, mixedDao);
		thirdDao.update(o);
	}

	private void updateSecondLevel(SecondLevel o, SecondLevelDao secondDao, FirstLevelDao firstDao, MixedDataDao mixedDao) {
		updateFirstLevel(o.one, firstDao, mixedDao);
		updateFirstLevel(o.two, firstDao, mixedDao);
		secondDao.update(o);
	}

	private void updateFirstLevel(FirstLevel o, FirstLevelDao firstDao, MixedDataDao mixedDao) {
		mixedDao.update(o.one);
		mixedDao.update(o.two);
		firstDao.update(o);
	}

	@Override
	public void bulkPersist(@NonNull List<FourthLevel> data) {
		final DaoSession daoSession = getDaoSession();
		final FourthLevelDao fourthDao = daoSession.getFourthLevelDao();
		final ThirdLevelDao thirdDao = daoSession.getThirdLevelDao();
		final SecondLevelDao secondDao = daoSession.getSecondLevelDao();
		final FirstLevelDao firstDao = daoSession.getFirstLevelDao();
		final MixedDataDao mixedDao = daoSession.getMixedDataDao();
		final SQLiteDatabase db = daoSession.getDatabase();
		db.beginTransaction();
		try {
			for (FourthLevel o : data) {
				insertOrReplaceThirdLevel(o.one, thirdDao, secondDao, firstDao, mixedDao);
				insertOrReplaceThirdLevel(o.two, thirdDao, secondDao, firstDao, mixedDao);
				modifyFourthLevelData(o);
				fourthDao.insertOrReplace(o);
			}
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}

	private void insertOrReplaceThirdLevel(ThirdLevel o, ThirdLevelDao thirdDao, SecondLevelDao secondDao, FirstLevelDao firstDao, MixedDataDao mixedDao) {
		insertOrReplaceSecondLevel(o.one, secondDao, firstDao, mixedDao);
		insertOrReplaceSecondLevel(o.two, secondDao, firstDao, mixedDao);
		modifyThirdLevelData(o);
		thirdDao.insertOrReplace(o);
	}

	private void insertOrReplaceSecondLevel(SecondLevel o, SecondLevelDao secondDao, FirstLevelDao firstDao, MixedDataDao mixedDao) {
		insertOrReplaceFirstLevel(o.one, firstDao, mixedDao);
		insertOrReplaceFirstLevel(o.two, firstDao, mixedDao);
		modifySecondLevelData(o);
		secondDao.insertOrReplace(o);
	}

	private void insertOrReplaceFirstLevel(FirstLevel o, FirstLevelDao firstDao, MixedDataDao mixedDao) {
		mixedDao.insertOrReplace(o.one);
		mixedDao.insertOrReplace(o.two);
		modifyFirstLevelData(o);
		firstDao.insertOrReplace(o);
	}

	@NonNull
	@Override
	public List<FourthLevel> queryAll() {
		final List<FourthLevel> results = getDao().queryDeep("");
		for (FourthLevel result : results) {
			queryFourthLevelDep(result);
		}
		return results;
	}

	@NonNull
	@Override
	public FourthLevel queryFirst() {
		final FourthLevel fourthLevel = getDao().queryDeep("LIMIT 1").get(0);
		queryFourthLevelDep(fourthLevel);
		return fourthLevel;
	}

	private static void queryFourthLevelDep(FourthLevel fourthLevel) {
		queryThirdLevelDep(fourthLevel.getOne());
		queryThirdLevelDep(fourthLevel.getTwo());
	}

	private static void queryThirdLevelDep(ThirdLevel o) {
		querySecondLevelDep(o.getOne());
		querySecondLevelDep(o.getTwo());
	}

	private static void querySecondLevelDep(SecondLevel o) {
		queryFirstLevelDep(o.getOne());
		queryFirstLevelDep(o.getTwo());
	}

	private static void queryFirstLevelDep(FirstLevel one) {
		one.getOne();
		one.getTwo();
	}

	@Override
	public long countAll() {
		return getDao().count();
	}

	@Override
	public long countDependencies() {
		return MixedDataHandler.getDao().count() +
				FirstLevelDataHandler.getDao().count() +
				getDaoSession().getSecondLevelDao().count() +
				getDaoSession().getThirdLevelDao().count();
	}

	@Override
	public long expectedDependenciesSize(int size) {
		return size * 30;
	}
}
