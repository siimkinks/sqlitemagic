package com.siimkinks.sqlite.speedtests.handlers;

import android.support.annotation.NonNull;

import com.siimkinks.sqlite.speedtests.LibInitializer;
import com.siimkinks.sqlite.speedtests.data.DaoSession;
import com.siimkinks.sqlite.speedtests.data.DataHandler;
import com.siimkinks.sqlite.speedtests.data.LongMixedData;
import com.siimkinks.sqlite.speedtests.data.LongMixedDataDao;

import java.util.List;

import de.greenrobot.dao.identityscope.IdentityScopeType;

public final class LongMixedDataHandler extends DataHandler<LongMixedData> {
	private LongMixedDataDao getDao() {
		final DaoSession daoSession = LibInitializer.daoMaster.newSession(IdentityScopeType.None);
		return daoSession.getLongMixedDataDao();
	}

	@Override
	public void deleteTable() {
		final LongMixedDataDao dao = getDao();
		dao.deleteAll();
	}

	@Override
	public void bulkInsert(@NonNull List<LongMixedData> data) {
		getDao().insertInTx(data, true);
	}

	@Override
	public void bulkUpdate(@NonNull List<LongMixedData> data) {
		getDao().updateInTx(data);
	}

	@Override
	public void bulkPersist(@NonNull List<LongMixedData> data) {
		getDao().insertOrReplaceInTx(data, true);
	}

	@NonNull
	@Override
	public List<LongMixedData> queryAll() {
		return getDao().loadAll();
	}

	@NonNull
	@Override
	public LongMixedData queryFirst() {
		return getDao().queryBuilder().limit(1).list().get(0);
	}

	@Override
	public long countAll() {
		return getDao().count();
	}

	@Override
	public long countDependencies() {
		return 0;
	}

	@Override
	public long expectedDependenciesSize(int size) {
		return 0;
	}
}
