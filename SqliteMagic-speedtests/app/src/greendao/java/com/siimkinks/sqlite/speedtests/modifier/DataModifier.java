package com.siimkinks.sqlite.speedtests.modifier;

import android.support.annotation.NonNull;

import com.siimkinks.sqlite.speedtests.data.FirstLevel;
import com.siimkinks.sqlite.speedtests.data.FourthLevel;
import com.siimkinks.sqlite.speedtests.data.SecondLevel;
import com.siimkinks.sqlite.speedtests.data.ThirdLevel;

public final class DataModifier {
	public static void modifyFirstLevelData(@NonNull FirstLevel firstLevel) {
		firstLevel.setOne(firstLevel.one);
		firstLevel.setTwo(firstLevel.two);
	}

	public static void modifySecondLevelData(@NonNull SecondLevel secondLevel) {
		secondLevel.setOne(secondLevel.one);
		secondLevel.setTwo(secondLevel.two);
	}

	public static void modifyThirdLevelData(@NonNull ThirdLevel thirdLevel) {
		thirdLevel.setOne(thirdLevel.one);
		thirdLevel.setTwo(thirdLevel.two);
	}

	public static void modifyFourthLevelData(@NonNull FourthLevel fourthLevel) {
		fourthLevel.setOne(fourthLevel.one);
		fourthLevel.setTwo(fourthLevel.two);
	}
}
