package com.siimkinks.sqlite.speedtests.handlers;

import android.support.annotation.NonNull;

import com.siimkinks.sqlite.speedtests.LibInitializer;
import com.siimkinks.sqlite.speedtests.data.DaoSession;
import com.siimkinks.sqlite.speedtests.data.DataHandler;
import com.siimkinks.sqlite.speedtests.data.MixedData;
import com.siimkinks.sqlite.speedtests.data.MixedDataDao;

import java.util.List;

import de.greenrobot.dao.identityscope.IdentityScopeType;

public final class MixedDataHandler extends DataHandler<MixedData> {
	public static MixedDataDao getDao() {
		final DaoSession daoSession = LibInitializer.daoMaster.newSession(IdentityScopeType.None);
		return daoSession.getMixedDataDao();
	}

	@Override
	public void deleteTable() {
		final MixedDataDao dao = getDao();
		dao.deleteAll();
	}

	@Override
	public void bulkInsert(@NonNull List<MixedData> data) {
		getDao().insertInTx(data, true);
	}

	@Override
	public void bulkUpdate(@NonNull List<MixedData> data) {
		getDao().updateInTx(data);
	}

	@Override
	public void bulkPersist(@NonNull List<MixedData> data) {
		getDao().insertOrReplaceInTx(data, true);
	}

	@NonNull
	@Override
	public List<MixedData> queryAll() {
		return getDao().loadAll();
	}

	@NonNull
	@Override
	public MixedData queryFirst() {
		return getDao().queryBuilder().limit(1).list().get(0);
	}

	@Override
	public long countAll() {
		return getDao().count();
	}

	@Override
	public long countDependencies() {
		return 0;
	}

	@Override
	public long expectedDependenciesSize(int size) {
		return 0;
	}
}
