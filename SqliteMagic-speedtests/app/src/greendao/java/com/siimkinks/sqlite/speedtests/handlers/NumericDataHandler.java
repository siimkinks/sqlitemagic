package com.siimkinks.sqlite.speedtests.handlers;


import android.support.annotation.NonNull;

import com.siimkinks.sqlite.speedtests.LibInitializer;
import com.siimkinks.sqlite.speedtests.data.DaoSession;
import com.siimkinks.sqlite.speedtests.data.DataHandler;
import com.siimkinks.sqlite.speedtests.data.NumericData;
import com.siimkinks.sqlite.speedtests.data.NumericDataDao;

import java.util.List;

import de.greenrobot.dao.identityscope.IdentityScopeType;

public final class NumericDataHandler extends DataHandler<NumericData> {
	private NumericDataDao getDao() {
		final DaoSession daoSession = LibInitializer.daoMaster.newSession(IdentityScopeType.None);
		return daoSession.getNumericDataDao();
	}

	@Override
	public void deleteTable() {
		final NumericDataDao dao = getDao();
		dao.deleteAll();
	}

	@Override
	public void bulkInsert(@NonNull List<NumericData> data) {
		getDao().insertInTx(data, true);
	}

	@Override
	public void bulkUpdate(@NonNull List<NumericData> data) {
		getDao().updateInTx(data);
	}

	@Override
	public void bulkPersist(@NonNull List<NumericData> data) {
		getDao().insertOrReplaceInTx(data, true);
	}

	@NonNull
	@Override
	public List<NumericData> queryAll() {
		return getDao().loadAll();
	}

	@NonNull
	@Override
	public NumericData queryFirst() {
		return getDao().queryBuilder().limit(1).list().get(0);
	}

	@Override
	public long countAll() {
		return getDao().count();
	}

	@Override
	public long countDependencies() {
		return 0;
	}

	@Override
	public long expectedDependenciesSize(int size) {
		return 0;
	}
}
