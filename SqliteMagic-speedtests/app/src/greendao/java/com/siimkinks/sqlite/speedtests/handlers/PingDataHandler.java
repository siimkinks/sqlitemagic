package com.siimkinks.sqlite.speedtests.handlers;

import com.siimkinks.sqlite.speedtests.LibInitializer;
import com.siimkinks.sqlite.speedtests.data.DaoSession;
import com.siimkinks.sqlite.speedtests.data.PingData;
import com.siimkinks.sqlite.speedtests.data.PingDataDao;

import de.greenrobot.dao.identityscope.IdentityScopeType;

public final class PingDataHandler {
	public static void insert(PingData pingData) {
		final DaoSession daoSession = LibInitializer.daoMaster.newSession(IdentityScopeType.None);
		final PingDataDao pingDataDao = daoSession.getPingDataDao();
		pingDataDao.insert(pingData);
	}
}
