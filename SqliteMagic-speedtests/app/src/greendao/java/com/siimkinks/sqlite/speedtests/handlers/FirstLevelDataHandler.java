package com.siimkinks.sqlite.speedtests.handlers;

import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import com.siimkinks.sqlite.speedtests.LibInitializer;
import com.siimkinks.sqlite.speedtests.data.DaoSession;
import com.siimkinks.sqlite.speedtests.data.DataHandler;
import com.siimkinks.sqlite.speedtests.data.FirstLevel;
import com.siimkinks.sqlite.speedtests.data.FirstLevelDao;
import com.siimkinks.sqlite.speedtests.data.MixedDataDao;

import java.util.List;

import de.greenrobot.dao.identityscope.IdentityScopeType;

import static com.siimkinks.sqlite.speedtests.modifier.DataModifier.modifyFirstLevelData;

public final class FirstLevelDataHandler extends DataHandler<FirstLevel> {
	public static FirstLevelDao getDao() {
		final DaoSession daoSession = LibInitializer.daoMaster.newSession(IdentityScopeType.None);
		return daoSession.getFirstLevelDao();
	}

	@Override
	public void deleteTable() {
		getDao().deleteAll();
		MixedDataHandler.getDao().deleteAll();
	}

	@Override
	public void bulkInsert(@NonNull List<FirstLevel> data) {
		final FirstLevelDao dao = getDao();
		final MixedDataDao depDao = MixedDataHandler.getDao();
		final SQLiteDatabase db = dao.getDatabase();
		db.beginTransaction();
		try {
			for (FirstLevel o : data) {
				depDao.insert(o.one);
				depDao.insert(o.two);
				modifyFirstLevelData(o);
				dao.insert(o);
			}
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}

	@Override
	public void bulkUpdate(@NonNull List<FirstLevel> data) {
		final FirstLevelDao dao = getDao();
		final MixedDataDao depDao = MixedDataHandler.getDao();
		final SQLiteDatabase db = dao.getDatabase();
		db.beginTransaction();
		try {
			for (FirstLevel o : data) {
				depDao.update(o.one);
				depDao.update(o.two);
				dao.update(o);
			}
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}

	@Override
	public void bulkPersist(@NonNull List<FirstLevel> data) {
		final FirstLevelDao dao = getDao();
		final MixedDataDao depDao = MixedDataHandler.getDao();
		final SQLiteDatabase db = dao.getDatabase();
		db.beginTransaction();
		try {
			for (FirstLevel o : data) {
				depDao.insertOrReplace(o.one);
				depDao.insertOrReplace(o.two);
				modifyFirstLevelData(o);
				dao.insertOrReplace(o);
			}
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}

	@NonNull
	@Override
	public List<FirstLevel> queryAll() {
		return getDao().queryDeep("");
	}

	@NonNull
	@Override
	public FirstLevel queryFirst() {
		return getDao().queryDeep("LIMIT 1").get(0);
	}

	@Override
	public long countAll() {
		return getDao().count();
	}

	@Override
	public long countDependencies() {
		return MixedDataHandler.getDao().count();
	}

	@Override
	public long expectedDependenciesSize(int size) {
		return size * 2;
	}
}
