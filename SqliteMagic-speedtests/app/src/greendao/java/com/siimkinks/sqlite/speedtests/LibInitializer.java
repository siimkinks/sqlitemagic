package com.siimkinks.sqlite.speedtests;

import com.siimkinks.sqlite.speedtests.data.DaoMaster;

public final class LibInitializer implements Initializer {
	public static DaoMaster daoMaster;

	@Override
	public void init(App app) {
		DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(app, "greendao", null);
		daoMaster = new DaoMaster(helper.getWritableDatabase());
	}

	@Override
	public void destroy(App app) {
	}
}
