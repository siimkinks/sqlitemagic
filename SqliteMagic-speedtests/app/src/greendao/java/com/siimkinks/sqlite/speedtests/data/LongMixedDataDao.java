package com.siimkinks.sqlite.speedtests.data;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.DaoConfig;

import com.siimkinks.sqlite.speedtests.data.LongMixedData;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table "LONG_MIXED_DATA".
*/
public class LongMixedDataDao extends AbstractDao<LongMixedData, Long> {

    public static final String TABLENAME = "LONG_MIXED_DATA";

    /**
     * Properties of entity LongMixedData.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property One = new Property(1, String.class, "one", false, "ONE");
        public final static Property Two = new Property(2, String.class, "two", false, "TWO");
        public final static Property Three = new Property(3, String.class, "three", false, "THREE");
        public final static Property Four = new Property(4, String.class, "four", false, "FOUR");
        public final static Property Five = new Property(5, String.class, "five", false, "FIVE");
        public final static Property Six = new Property(6, String.class, "six", false, "SIX");
        public final static Property Seven = new Property(7, String.class, "seven", false, "SEVEN");
        public final static Property Eight = new Property(8, String.class, "eight", false, "EIGHT");
        public final static Property Nine = new Property(9, String.class, "nine", false, "NINE");
        public final static Property Ten = new Property(10, String.class, "ten", false, "TEN");
        public final static Property Eleven = new Property(11, String.class, "eleven", false, "ELEVEN");
        public final static Property Twelve = new Property(12, String.class, "twelve", false, "TWELVE");
        public final static Property OneL = new Property(13, long.class, "oneL", false, "ONE_L");
        public final static Property TwoL = new Property(14, long.class, "twoL", false, "TWO_L");
        public final static Property ThreeL = new Property(15, Long.class, "threeL", false, "THREE_L");
        public final static Property FourL = new Property(16, Long.class, "fourL", false, "FOUR_L");
        public final static Property FiveL = new Property(17, long.class, "fiveL", false, "FIVE_L");
        public final static Property SixL = new Property(18, long.class, "sixL", false, "SIX_L");
        public final static Property SevenL = new Property(19, Long.class, "sevenL", false, "SEVEN_L");
        public final static Property EightL = new Property(20, Long.class, "eightL", false, "EIGHT_L");
        public final static Property OneD = new Property(21, double.class, "oneD", false, "ONE_D");
        public final static Property TwoD = new Property(22, double.class, "twoD", false, "TWO_D");
        public final static Property ThreeD = new Property(23, Double.class, "threeD", false, "THREE_D");
        public final static Property FourD = new Property(24, Double.class, "fourD", false, "FOUR_D");
        public final static Property FiveD = new Property(25, double.class, "fiveD", false, "FIVE_D");
        public final static Property SixD = new Property(26, double.class, "sixD", false, "SIX_D");
        public final static Property SevenD = new Property(27, Double.class, "sevenD", false, "SEVEN_D");
        public final static Property EightD = new Property(28, Double.class, "eightD", false, "EIGHT_D");
        public final static Property OneI = new Property(29, int.class, "oneI", false, "ONE_I");
        public final static Property TwoI = new Property(30, int.class, "twoI", false, "TWO_I");
        public final static Property ThreeI = new Property(31, Integer.class, "threeI", false, "THREE_I");
        public final static Property FourI = new Property(32, Integer.class, "fourI", false, "FOUR_I");
        public final static Property FiveI = new Property(33, int.class, "fiveI", false, "FIVE_I");
        public final static Property SixI = new Property(34, int.class, "sixI", false, "SIX_I");
        public final static Property SevenI = new Property(35, Integer.class, "sevenI", false, "SEVEN_I");
        public final static Property EightI = new Property(36, Integer.class, "eightI", false, "EIGHT_I");
        public final static Property OneF = new Property(37, float.class, "oneF", false, "ONE_F");
        public final static Property TwoF = new Property(38, float.class, "twoF", false, "TWO_F");
        public final static Property ThreeF = new Property(39, Float.class, "threeF", false, "THREE_F");
        public final static Property FourF = new Property(40, Float.class, "fourF", false, "FOUR_F");
        public final static Property FiveF = new Property(41, float.class, "fiveF", false, "FIVE_F");
        public final static Property SixF = new Property(42, float.class, "sixF", false, "SIX_F");
        public final static Property SevenF = new Property(43, Float.class, "sevenF", false, "SEVEN_F");
        public final static Property EightF = new Property(44, Float.class, "eightF", false, "EIGHT_F");
    };


    public LongMixedDataDao(DaoConfig config) {
        super(config);
    }
    
    public LongMixedDataDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"LONG_MIXED_DATA\" (" + //
                "\"_id\" INTEGER PRIMARY KEY AUTOINCREMENT ," + // 0: id
                "\"ONE\" TEXT NOT NULL ," + // 1: one
                "\"TWO\" TEXT NOT NULL ," + // 2: two
                "\"THREE\" TEXT," + // 3: three
                "\"FOUR\" TEXT," + // 4: four
                "\"FIVE\" TEXT NOT NULL ," + // 5: five
                "\"SIX\" TEXT NOT NULL ," + // 6: six
                "\"SEVEN\" TEXT," + // 7: seven
                "\"EIGHT\" TEXT," + // 8: eight
                "\"NINE\" TEXT NOT NULL ," + // 9: nine
                "\"TEN\" TEXT NOT NULL ," + // 10: ten
                "\"ELEVEN\" TEXT," + // 11: eleven
                "\"TWELVE\" TEXT," + // 12: twelve
                "\"ONE_L\" INTEGER NOT NULL ," + // 13: oneL
                "\"TWO_L\" INTEGER NOT NULL ," + // 14: twoL
                "\"THREE_L\" INTEGER," + // 15: threeL
                "\"FOUR_L\" INTEGER," + // 16: fourL
                "\"FIVE_L\" INTEGER NOT NULL ," + // 17: fiveL
                "\"SIX_L\" INTEGER NOT NULL ," + // 18: sixL
                "\"SEVEN_L\" INTEGER," + // 19: sevenL
                "\"EIGHT_L\" INTEGER," + // 20: eightL
                "\"ONE_D\" REAL NOT NULL ," + // 21: oneD
                "\"TWO_D\" REAL NOT NULL ," + // 22: twoD
                "\"THREE_D\" REAL," + // 23: threeD
                "\"FOUR_D\" REAL," + // 24: fourD
                "\"FIVE_D\" REAL NOT NULL ," + // 25: fiveD
                "\"SIX_D\" REAL NOT NULL ," + // 26: sixD
                "\"SEVEN_D\" REAL," + // 27: sevenD
                "\"EIGHT_D\" REAL," + // 28: eightD
                "\"ONE_I\" INTEGER NOT NULL ," + // 29: oneI
                "\"TWO_I\" INTEGER NOT NULL ," + // 30: twoI
                "\"THREE_I\" INTEGER," + // 31: threeI
                "\"FOUR_I\" INTEGER," + // 32: fourI
                "\"FIVE_I\" INTEGER NOT NULL ," + // 33: fiveI
                "\"SIX_I\" INTEGER NOT NULL ," + // 34: sixI
                "\"SEVEN_I\" INTEGER," + // 35: sevenI
                "\"EIGHT_I\" INTEGER," + // 36: eightI
                "\"ONE_F\" REAL NOT NULL ," + // 37: oneF
                "\"TWO_F\" REAL NOT NULL ," + // 38: twoF
                "\"THREE_F\" REAL," + // 39: threeF
                "\"FOUR_F\" REAL," + // 40: fourF
                "\"FIVE_F\" REAL NOT NULL ," + // 41: fiveF
                "\"SIX_F\" REAL NOT NULL ," + // 42: sixF
                "\"SEVEN_F\" REAL," + // 43: sevenF
                "\"EIGHT_F\" REAL);"); // 44: eightF
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"LONG_MIXED_DATA\"";
        db.execSQL(sql);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, LongMixedData entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
        stmt.bindString(2, entity.getOne());
        stmt.bindString(3, entity.getTwo());
 
        String three = entity.getThree();
        if (three != null) {
            stmt.bindString(4, three);
        }
 
        String four = entity.getFour();
        if (four != null) {
            stmt.bindString(5, four);
        }
        stmt.bindString(6, entity.getFive());
        stmt.bindString(7, entity.getSix());
 
        String seven = entity.getSeven();
        if (seven != null) {
            stmt.bindString(8, seven);
        }
 
        String eight = entity.getEight();
        if (eight != null) {
            stmt.bindString(9, eight);
        }
        stmt.bindString(10, entity.getNine());
        stmt.bindString(11, entity.getTen());
 
        String eleven = entity.getEleven();
        if (eleven != null) {
            stmt.bindString(12, eleven);
        }
 
        String twelve = entity.getTwelve();
        if (twelve != null) {
            stmt.bindString(13, twelve);
        }
        stmt.bindLong(14, entity.getOneL());
        stmt.bindLong(15, entity.getTwoL());
 
        Long threeL = entity.getThreeL();
        if (threeL != null) {
            stmt.bindLong(16, threeL);
        }
 
        Long fourL = entity.getFourL();
        if (fourL != null) {
            stmt.bindLong(17, fourL);
        }
        stmt.bindLong(18, entity.getFiveL());
        stmt.bindLong(19, entity.getSixL());
 
        Long sevenL = entity.getSevenL();
        if (sevenL != null) {
            stmt.bindLong(20, sevenL);
        }
 
        Long eightL = entity.getEightL();
        if (eightL != null) {
            stmt.bindLong(21, eightL);
        }
        stmt.bindDouble(22, entity.getOneD());
        stmt.bindDouble(23, entity.getTwoD());
 
        Double threeD = entity.getThreeD();
        if (threeD != null) {
            stmt.bindDouble(24, threeD);
        }
 
        Double fourD = entity.getFourD();
        if (fourD != null) {
            stmt.bindDouble(25, fourD);
        }
        stmt.bindDouble(26, entity.getFiveD());
        stmt.bindDouble(27, entity.getSixD());
 
        Double sevenD = entity.getSevenD();
        if (sevenD != null) {
            stmt.bindDouble(28, sevenD);
        }
 
        Double eightD = entity.getEightD();
        if (eightD != null) {
            stmt.bindDouble(29, eightD);
        }
        stmt.bindLong(30, entity.getOneI());
        stmt.bindLong(31, entity.getTwoI());
 
        Integer threeI = entity.getThreeI();
        if (threeI != null) {
            stmt.bindLong(32, threeI);
        }
 
        Integer fourI = entity.getFourI();
        if (fourI != null) {
            stmt.bindLong(33, fourI);
        }
        stmt.bindLong(34, entity.getFiveI());
        stmt.bindLong(35, entity.getSixI());
 
        Integer sevenI = entity.getSevenI();
        if (sevenI != null) {
            stmt.bindLong(36, sevenI);
        }
 
        Integer eightI = entity.getEightI();
        if (eightI != null) {
            stmt.bindLong(37, eightI);
        }
        stmt.bindDouble(38, entity.getOneF());
        stmt.bindDouble(39, entity.getTwoF());
 
        Float threeF = entity.getThreeF();
        if (threeF != null) {
            stmt.bindDouble(40, threeF);
        }
 
        Float fourF = entity.getFourF();
        if (fourF != null) {
            stmt.bindDouble(41, fourF);
        }
        stmt.bindDouble(42, entity.getFiveF());
        stmt.bindDouble(43, entity.getSixF());
 
        Float sevenF = entity.getSevenF();
        if (sevenF != null) {
            stmt.bindDouble(44, sevenF);
        }
 
        Float eightF = entity.getEightF();
        if (eightF != null) {
            stmt.bindDouble(45, eightF);
        }
    }

    /** @inheritdoc */
    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    /** @inheritdoc */
    @Override
    public LongMixedData readEntity(Cursor cursor, int offset) {
        LongMixedData entity = new LongMixedData( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.getString(offset + 1), // one
            cursor.getString(offset + 2), // two
            cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3), // three
            cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4), // four
            cursor.getString(offset + 5), // five
            cursor.getString(offset + 6), // six
            cursor.isNull(offset + 7) ? null : cursor.getString(offset + 7), // seven
            cursor.isNull(offset + 8) ? null : cursor.getString(offset + 8), // eight
            cursor.getString(offset + 9), // nine
            cursor.getString(offset + 10), // ten
            cursor.isNull(offset + 11) ? null : cursor.getString(offset + 11), // eleven
            cursor.isNull(offset + 12) ? null : cursor.getString(offset + 12), // twelve
            cursor.getLong(offset + 13), // oneL
            cursor.getLong(offset + 14), // twoL
            cursor.isNull(offset + 15) ? null : cursor.getLong(offset + 15), // threeL
            cursor.isNull(offset + 16) ? null : cursor.getLong(offset + 16), // fourL
            cursor.getLong(offset + 17), // fiveL
            cursor.getLong(offset + 18), // sixL
            cursor.isNull(offset + 19) ? null : cursor.getLong(offset + 19), // sevenL
            cursor.isNull(offset + 20) ? null : cursor.getLong(offset + 20), // eightL
            cursor.getDouble(offset + 21), // oneD
            cursor.getDouble(offset + 22), // twoD
            cursor.isNull(offset + 23) ? null : cursor.getDouble(offset + 23), // threeD
            cursor.isNull(offset + 24) ? null : cursor.getDouble(offset + 24), // fourD
            cursor.getDouble(offset + 25), // fiveD
            cursor.getDouble(offset + 26), // sixD
            cursor.isNull(offset + 27) ? null : cursor.getDouble(offset + 27), // sevenD
            cursor.isNull(offset + 28) ? null : cursor.getDouble(offset + 28), // eightD
            cursor.getInt(offset + 29), // oneI
            cursor.getInt(offset + 30), // twoI
            cursor.isNull(offset + 31) ? null : cursor.getInt(offset + 31), // threeI
            cursor.isNull(offset + 32) ? null : cursor.getInt(offset + 32), // fourI
            cursor.getInt(offset + 33), // fiveI
            cursor.getInt(offset + 34), // sixI
            cursor.isNull(offset + 35) ? null : cursor.getInt(offset + 35), // sevenI
            cursor.isNull(offset + 36) ? null : cursor.getInt(offset + 36), // eightI
            cursor.getFloat(offset + 37), // oneF
            cursor.getFloat(offset + 38), // twoF
            cursor.isNull(offset + 39) ? null : cursor.getFloat(offset + 39), // threeF
            cursor.isNull(offset + 40) ? null : cursor.getFloat(offset + 40), // fourF
            cursor.getFloat(offset + 41), // fiveF
            cursor.getFloat(offset + 42), // sixF
            cursor.isNull(offset + 43) ? null : cursor.getFloat(offset + 43), // sevenF
            cursor.isNull(offset + 44) ? null : cursor.getFloat(offset + 44) // eightF
        );
        return entity;
    }
     
    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, LongMixedData entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setOne(cursor.getString(offset + 1));
        entity.setTwo(cursor.getString(offset + 2));
        entity.setThree(cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3));
        entity.setFour(cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4));
        entity.setFive(cursor.getString(offset + 5));
        entity.setSix(cursor.getString(offset + 6));
        entity.setSeven(cursor.isNull(offset + 7) ? null : cursor.getString(offset + 7));
        entity.setEight(cursor.isNull(offset + 8) ? null : cursor.getString(offset + 8));
        entity.setNine(cursor.getString(offset + 9));
        entity.setTen(cursor.getString(offset + 10));
        entity.setEleven(cursor.isNull(offset + 11) ? null : cursor.getString(offset + 11));
        entity.setTwelve(cursor.isNull(offset + 12) ? null : cursor.getString(offset + 12));
        entity.setOneL(cursor.getLong(offset + 13));
        entity.setTwoL(cursor.getLong(offset + 14));
        entity.setThreeL(cursor.isNull(offset + 15) ? null : cursor.getLong(offset + 15));
        entity.setFourL(cursor.isNull(offset + 16) ? null : cursor.getLong(offset + 16));
        entity.setFiveL(cursor.getLong(offset + 17));
        entity.setSixL(cursor.getLong(offset + 18));
        entity.setSevenL(cursor.isNull(offset + 19) ? null : cursor.getLong(offset + 19));
        entity.setEightL(cursor.isNull(offset + 20) ? null : cursor.getLong(offset + 20));
        entity.setOneD(cursor.getDouble(offset + 21));
        entity.setTwoD(cursor.getDouble(offset + 22));
        entity.setThreeD(cursor.isNull(offset + 23) ? null : cursor.getDouble(offset + 23));
        entity.setFourD(cursor.isNull(offset + 24) ? null : cursor.getDouble(offset + 24));
        entity.setFiveD(cursor.getDouble(offset + 25));
        entity.setSixD(cursor.getDouble(offset + 26));
        entity.setSevenD(cursor.isNull(offset + 27) ? null : cursor.getDouble(offset + 27));
        entity.setEightD(cursor.isNull(offset + 28) ? null : cursor.getDouble(offset + 28));
        entity.setOneI(cursor.getInt(offset + 29));
        entity.setTwoI(cursor.getInt(offset + 30));
        entity.setThreeI(cursor.isNull(offset + 31) ? null : cursor.getInt(offset + 31));
        entity.setFourI(cursor.isNull(offset + 32) ? null : cursor.getInt(offset + 32));
        entity.setFiveI(cursor.getInt(offset + 33));
        entity.setSixI(cursor.getInt(offset + 34));
        entity.setSevenI(cursor.isNull(offset + 35) ? null : cursor.getInt(offset + 35));
        entity.setEightI(cursor.isNull(offset + 36) ? null : cursor.getInt(offset + 36));
        entity.setOneF(cursor.getFloat(offset + 37));
        entity.setTwoF(cursor.getFloat(offset + 38));
        entity.setThreeF(cursor.isNull(offset + 39) ? null : cursor.getFloat(offset + 39));
        entity.setFourF(cursor.isNull(offset + 40) ? null : cursor.getFloat(offset + 40));
        entity.setFiveF(cursor.getFloat(offset + 41));
        entity.setSixF(cursor.getFloat(offset + 42));
        entity.setSevenF(cursor.isNull(offset + 43) ? null : cursor.getFloat(offset + 43));
        entity.setEightF(cursor.isNull(offset + 44) ? null : cursor.getFloat(offset + 44));
     }
    
    /** @inheritdoc */
    @Override
    protected Long updateKeyAfterInsert(LongMixedData entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    /** @inheritdoc */
    @Override
    public Long getKey(LongMixedData entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    /** @inheritdoc */
    @Override    
    protected boolean isEntityUpdateable() {
        return true;
    }
    
}
