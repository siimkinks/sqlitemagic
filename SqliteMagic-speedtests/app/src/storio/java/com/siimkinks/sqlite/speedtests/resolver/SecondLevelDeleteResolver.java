package com.siimkinks.sqlite.speedtests.resolver;

import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.operations.delete.DeleteResolver;
import com.pushtorefresh.storio.sqlite.operations.delete.DeleteResult;
import com.siimkinks.sqlite.speedtests.data.SecondLevel;

public final class SecondLevelDeleteResolver extends DeleteResolver<SecondLevel> {
	@NonNull
	@Override
	public DeleteResult performDelete(@NonNull StorIOSQLite storIOSQLite, @NonNull SecondLevel firstLevel) {
		return null;
	}
}
