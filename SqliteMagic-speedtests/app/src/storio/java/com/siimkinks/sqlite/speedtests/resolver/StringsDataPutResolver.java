package com.siimkinks.sqlite.speedtests.resolver;

import android.content.ContentValues;
import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.operations.put.DefaultPutResolver;
import com.pushtorefresh.storio.sqlite.operations.put.PutResult;
import com.pushtorefresh.storio.sqlite.queries.InsertQuery;
import com.pushtorefresh.storio.sqlite.queries.UpdateQuery;
import com.siimkinks.sqlite.speedtests.data.StringsData;

public class StringsDataPutResolver extends DefaultPutResolver<StringsData> {
	/**
	 * {@inheritDoc}
	 */
	@Override
	@NonNull
	protected InsertQuery mapToInsertQuery(@NonNull StringsData object) {
		return InsertQuery.builder()
				.table("stringsdata")
				.build();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@NonNull
	protected UpdateQuery mapToUpdateQuery(@NonNull StringsData object) {
		return UpdateQuery.builder()
				.table("stringsdata")
				.where("_id = ?")
				.whereArgs(object.id)
				.build();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@NonNull
	public ContentValues mapToContentValues(@NonNull StringsData object) {
		ContentValues contentValues = new ContentValues(13);

		contentValues.put("nine", object.nine);
		contentValues.put("six", object.six);
		contentValues.put("one", object.one);
		contentValues.put("seven", object.seven);
		contentValues.put("two", object.two);
		contentValues.put("three", object.three);
		contentValues.put("eight", object.eight);
		contentValues.put("four", object.four);
		contentValues.put("twelve", object.twelve);
		contentValues.put("eleven", object.eleven);
		contentValues.put("_id", object.id);
		contentValues.put("ten", object.ten);
		contentValues.put("five", object.five);

		return contentValues;
	}

	@NonNull
	@Override
	public PutResult performPut(@NonNull StorIOSQLite storIOSQLite, @NonNull StringsData object) {
		final PutResult result = super.performPut(storIOSQLite, object);
		final Long id = result.insertedId();
		if (id != null) {
			object.id = id;
		}
		return result;
	}
}
