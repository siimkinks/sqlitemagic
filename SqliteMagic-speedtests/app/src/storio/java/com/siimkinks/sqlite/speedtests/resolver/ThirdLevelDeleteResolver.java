package com.siimkinks.sqlite.speedtests.resolver;

import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.operations.delete.DeleteResolver;
import com.pushtorefresh.storio.sqlite.operations.delete.DeleteResult;
import com.siimkinks.sqlite.speedtests.data.ThirdLevel;

public final class ThirdLevelDeleteResolver extends DeleteResolver<ThirdLevel> {
	@NonNull
	@Override
	public DeleteResult performDelete(@NonNull StorIOSQLite storIOSQLite, @NonNull ThirdLevel firstLevel) {
		return null;
	}
}
