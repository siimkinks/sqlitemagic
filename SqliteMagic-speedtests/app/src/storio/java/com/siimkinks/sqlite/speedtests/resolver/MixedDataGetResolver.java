package com.siimkinks.sqlite.speedtests.resolver;

import android.database.Cursor;
import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.operations.get.DefaultGetResolver;
import com.siimkinks.sqlite.speedtests.data.MixedData;

public final class MixedDataGetResolver extends DefaultGetResolver<MixedData> {
	@NonNull
	@Override
	public MixedData mapFromCursor(@NonNull Cursor cursor) {
		MixedData object = new MixedData();

		object.twoL = cursor.getLong(cursor.getColumnIndex("two_l"));
		object.threeD = cursor.getDouble(cursor.getColumnIndex("three_d"));
		object.oneD = cursor.getDouble(cursor.getColumnIndex("one_d"));
		object.one = cursor.getString(cursor.getColumnIndex("one"));
		int index = cursor.getColumnIndex("four_d");
		if (!cursor.isNull(index)) {
			object.fourD = cursor.getDouble(index);
		}
		object.threeL = cursor.getLong(cursor.getColumnIndex("three_l"));
		object.twoD = cursor.getDouble(cursor.getColumnIndex("two_d"));
		object.two = cursor.getString(cursor.getColumnIndex("two"));

		index = cursor.getColumnIndex("three");
		if (!cursor.isNull(index)) {
			object.three = cursor.getString(index);
		}
		index = cursor.getColumnIndex("four_l");
		if (!cursor.isNull(index)) {
			object.fourL = cursor.getLong(index);
		}
		index = cursor.getColumnIndex("four");
		if (cursor.isNull(index)) {
			object.four = cursor.getString(index);
		}
		object.id = cursor.getLong(cursor.getColumnIndex("_id"));
		object.oneL = cursor.getLong(cursor.getColumnIndex("one_l"));

		return object;
	}
}
