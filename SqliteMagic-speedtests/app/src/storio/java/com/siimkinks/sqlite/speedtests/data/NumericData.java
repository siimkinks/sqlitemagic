package com.siimkinks.sqlite.speedtests.data;

import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
@StorIOSQLiteType(table = NumericData.TABLE)
public final class NumericData {
	public static final String CREATE = "CREATE TABLE IF NOT EXISTS numericdata (primitive_int INTEGER, boxed_int INTEGER, primitive_long INTEGER, boxed_long INTEGER, primitive_boolean INTEGER, boxed_boolean INTEGER, primitive_float REAL, boxed_float REAL, primitive_double REAL, boxed_double REAL, primitive_short INTEGER, boxed_short INTEGER, _id INTEGER PRIMARY KEY AUTOINCREMENT)";
	public static final String TABLE = "numericdata";
	public static final String C_ID = "_id";
	public static final String C_PRIMITIVE_INT = "primitive_int";
	public static final String C_BOXED_INT = "boxed_int";
	public static final String C_PRIMITIVE_LONG = "primitive_long";
	public static final String C_BOXED_LONG = "boxed_long";
	public static final String C_PRIMITIVE_BOOLEAN = "primitive_boolean";
	public static final String C_BOXED_BOOLEAN = "boxed_boolean";
	public static final String C_PRIMITIVE_FLOAT = "primitive_float";
	public static final String C_BOXED_FLOAT = "boxed_float";
	public static final String C_PRIMITIVE_DOUBLE = "primitive_double";
	public static final String C_BOXED_DOUBLE = "boxed_double";
	public static final String C_PRIMITIVE_SHORT = "primitive_short";
	public static final String C_BOXED_SHORT = "boxed_short";

	@StorIOSQLiteColumn(name = C_ID, key = true)
	public Long id;

	@StorIOSQLiteColumn(name = C_PRIMITIVE_INT)
	public int primitiveInt;
	@NonNull
	@StorIOSQLiteColumn(name = C_BOXED_INT)
	public Integer boxedInt;
	@StorIOSQLiteColumn(name = C_PRIMITIVE_LONG)
	public long primitiveLong;
	@NonNull
	@StorIOSQLiteColumn(name = C_BOXED_LONG)
	public Long boxedLong;
	@StorIOSQLiteColumn(name = C_PRIMITIVE_BOOLEAN)
	public boolean primitiveBoolean;
	@NonNull
	@StorIOSQLiteColumn(name = C_BOXED_BOOLEAN)
	public Boolean boxedBoolean;
	@StorIOSQLiteColumn(name = C_PRIMITIVE_FLOAT)
	public float primitiveFloat;
	@NonNull
	@StorIOSQLiteColumn(name = C_BOXED_FLOAT)
	public Float boxedFloat;
	@StorIOSQLiteColumn(name = C_PRIMITIVE_DOUBLE)
	public double primitiveDouble;
	@NonNull
	@StorIOSQLiteColumn(name = C_BOXED_DOUBLE)
	public Double boxedDouble;
	@StorIOSQLiteColumn(name = C_PRIMITIVE_SHORT)
	public short primitiveShort;
	@NonNull
	@StorIOSQLiteColumn(name = C_BOXED_SHORT)
	public Short boxedShort;
}
