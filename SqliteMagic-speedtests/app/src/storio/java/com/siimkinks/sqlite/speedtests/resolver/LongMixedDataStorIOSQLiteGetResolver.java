package com.siimkinks.sqlite.speedtests.resolver;

import android.database.Cursor;
import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.operations.get.DefaultGetResolver;
import com.siimkinks.sqlite.speedtests.data.LongMixedData;

public final class LongMixedDataStorIOSQLiteGetResolver extends DefaultGetResolver<LongMixedData> {
	/**
	 * {@inheritDoc}
	 */
	@Override
	@NonNull
	public LongMixedData mapFromCursor(@NonNull Cursor cursor) {
		LongMixedData object = new LongMixedData();
		object.sevenF = cursor.getFloat(cursor.getColumnIndex("seven_f"));
		object.nine = cursor.getString(cursor.getColumnIndex("nine"));
		object.sixD = cursor.getDouble(cursor.getColumnIndex("six_d"));
		object.oneF = cursor.getFloat(cursor.getColumnIndex("one_f"));
		object.threeD = cursor.getDouble(cursor.getColumnIndex("three_d"));
		object.sevenI = cursor.getInt(cursor.getColumnIndex("seven_i"));
		object.oneD = cursor.getDouble(cursor.getColumnIndex("one_d"));
		object.threeF = cursor.getFloat(cursor.getColumnIndex("three_f"));
		object.sevenD = cursor.getDouble(cursor.getColumnIndex("seven_d"));
		object.threeI = cursor.getInt(cursor.getColumnIndex("three_i"));
		int index = cursor.getColumnIndex("seven");
		if (!cursor.isNull(index)) {
			object.seven = cursor.getString(index);
		}
		object.sixL = cursor.getLong(cursor.getColumnIndex("six_l"));
		object.sixI = cursor.getInt(cursor.getColumnIndex("six_i"));
		object.threeL = cursor.getLong(cursor.getColumnIndex("three_l"));
		object.two = cursor.getString(cursor.getColumnIndex("two"));

		index = cursor.getColumnIndex("three");
		if (!cursor.isNull(index)) {
			object.three = cursor.getString(index);
		}
		object.sevenL = cursor.getLong(cursor.getColumnIndex("seven_l"));
		index = cursor.getColumnIndex("eight");
		if (!cursor.isNull(index)) {
			object.eight = cursor.getString(index);
		}
		object.sixF = cursor.getFloat(cursor.getColumnIndex("six_f"));
		object.fiveI = cursor.getInt(cursor.getColumnIndex("five_i"));
		object.fiveL = cursor.getLong(cursor.getColumnIndex("five_l"));
		object.fiveF = cursor.getFloat(cursor.getColumnIndex("five_f"));
		index = cursor.getColumnIndex("four");
		if (!cursor.isNull(index)) {
			object.four = cursor.getString(index);
		}
		object.fiveD = cursor.getDouble(cursor.getColumnIndex("five_d"));
		object.ten = cursor.getString(cursor.getColumnIndex("ten"));
		object.oneL = cursor.getLong(cursor.getColumnIndex("one_l"));
		object.five = cursor.getString(cursor.getColumnIndex("five"));
		object.oneI = cursor.getInt(cursor.getColumnIndex("one_i"));
		index = cursor.getColumnIndex("eight_l");
		if (!cursor.isNull(index)) {
			object.eightL = cursor.getLong(index);
		}
		object.six = cursor.getString(cursor.getColumnIndex("six"));
		object.twoL = cursor.getLong(cursor.getColumnIndex("two_l"));
		object.one = cursor.getString(cursor.getColumnIndex("one"));
		index = cursor.getColumnIndex("eight_i");
		if (!cursor.isNull(index)) {
			object.eightI = cursor.getInt(index);
		}
		index = cursor.getColumnIndex("eight_f");
		if (!cursor.isNull(index)) {
			object.eightF = cursor.getFloat(index);
		}
		object.twoF = cursor.getFloat(cursor.getColumnIndex("two_f"));
		index = cursor.getColumnIndex("four_d");
		if (!cursor.isNull(index)) {
			object.fourD = cursor.getDouble(index);
		}
		index = cursor.getColumnIndex("eight_d");
		if (!cursor.isNull(index)) {
			object.eightD = cursor.getDouble(index);
		}
		object.twoD = cursor.getDouble(cursor.getColumnIndex("two_d"));
		object.twoI = cursor.getInt(cursor.getColumnIndex("two_i"));

		index = cursor.getColumnIndex("four_f");
		if (!cursor.isNull(index)) {
			object.fourF = cursor.getFloat(index);
		}
		index = cursor.getColumnIndex("four_i");
		if (!cursor.isNull(index)) {
			object.fourI = cursor.getInt(index);
		}
		index = cursor.getColumnIndex("four_l");
		if (!cursor.isNull(index)) {
			object.fourL = cursor.getLong(index);
		}
		index = cursor.getColumnIndex("twelve");
		if (!cursor.isNull(index)) {
			object.twelve = cursor.getString(index);
		}
		index = cursor.getColumnIndex("eleven");
		if (!cursor.isNull(index)) {
			object.eleven = cursor.getString(index);
		}
		object.id = cursor.getLong(cursor.getColumnIndex("_id"));

		return object;
	}

}
