package com.siimkinks.sqlite.speedtests.handlers;

import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.queries.DeleteQuery;
import com.pushtorefresh.storio.sqlite.queries.Query;
import com.siimkinks.sqlite.speedtests.data.DataHandler;
import com.siimkinks.sqlite.speedtests.data.MixedData;

import java.util.List;

import static com.siimkinks.sqlite.speedtests.LibInitializer.storIOSQLite;

public final class MixedDataHandler extends DataHandler<MixedData> {
	public static final Query QUERY_ALL = Query.builder().table(MixedData.TABLE).build();
	private static final Query QUERY_FIRST = Query.builder().table(MixedData.TABLE).limit(1).build();

	@Override
	public void deleteTable() {
		storIOSQLite
				.delete()
				.byQuery(DeleteQuery.builder()
						.table(MixedData.TABLE)
						.build())
				.prepare()
				.executeAsBlocking();
	}

	@Override
	public void bulkInsert(@NonNull List<MixedData> data) {
		storIOSQLite
				.put()
				.objects(data)
				.useTransaction(true)
				.prepare()
				.executeAsBlocking()
				.results();
	}

	@Override
	public void bulkUpdate(@NonNull List<MixedData> data) {
		storIOSQLite
				.put()
				.objects(data)
				.useTransaction(true)
				.prepare()
				.executeAsBlocking();
	}

	@Override
	public void bulkPersist(@NonNull List<MixedData> data) {
		storIOSQLite
				.put()
				.objects(data)
				.useTransaction(true)
				.prepare()
				.executeAsBlocking();
	}

	@NonNull
	@Override
	public List<MixedData> queryAll() {
		return storIOSQLite
				.get()
				.listOfObjects(MixedData.class)
				.withQuery(QUERY_ALL)
				.prepare()
				.executeAsBlocking();
	}

	@NonNull
	@Override
	public MixedData queryFirst() {
		return storIOSQLite
				.get()
				.listOfObjects(MixedData.class)
				.withQuery(QUERY_FIRST)
				.prepare()
				.executeAsBlocking()
				.get(0);
	}

	@Override
	public long countAll() {
		return storIOSQLite
				.get()
				.numberOfResults()
				.withQuery(QUERY_ALL)
				.prepare()
				.executeAsBlocking();
	}

	@Override
	public long countDependencies() {
		return 0;
	}

	@Override
	public long expectedDependenciesSize(int size) {
		return 0;
	}
}
