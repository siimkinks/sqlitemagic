package com.siimkinks.sqlite.speedtests.resolver;

import android.content.ContentValues;
import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.operations.put.DefaultPutResolver;
import com.pushtorefresh.storio.sqlite.operations.put.PutResult;
import com.pushtorefresh.storio.sqlite.queries.InsertQuery;
import com.pushtorefresh.storio.sqlite.queries.UpdateQuery;
import com.siimkinks.sqlite.speedtests.data.NumericData;

public class NumericDataPutResolver extends DefaultPutResolver<NumericData> {
	/**
	 * {@inheritDoc}
	 */
	@Override
	@NonNull
	protected InsertQuery mapToInsertQuery(@NonNull NumericData object) {
		return InsertQuery.builder()
				.table("numericdata")
				.build();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@NonNull
	protected UpdateQuery mapToUpdateQuery(@NonNull NumericData object) {
		return UpdateQuery.builder()
				.table("numericdata")
				.where("_id = ?")
				.whereArgs(object.id)
				.build();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@NonNull
	public ContentValues mapToContentValues(@NonNull NumericData object) {
		ContentValues contentValues = new ContentValues(13);

		contentValues.put("primitive_double", object.primitiveDouble);
		contentValues.put("boxed_short", object.boxedShort);
		contentValues.put("boxed_long", object.boxedLong);
		contentValues.put("boxed_float", object.boxedFloat);
		contentValues.put("primitive_short", object.primitiveShort);
		contentValues.put("boxed_boolean", object.boxedBoolean);
		contentValues.put("boxed_int", object.boxedInt);
		contentValues.put("primitive_boolean", object.primitiveBoolean);
		contentValues.put("primitive_float", object.primitiveFloat);
		contentValues.put("boxed_double", object.boxedDouble);
		contentValues.put("primitive_long", object.primitiveLong);
		contentValues.put("primitive_int", object.primitiveInt);
		contentValues.put("_id", object.id);

		return contentValues;
	}

	@NonNull
	@Override
	public PutResult performPut(@NonNull StorIOSQLite storIOSQLite, @NonNull NumericData object) {
		final PutResult result = super.performPut(storIOSQLite, object);
		final Long id = result.insertedId();
		if (id != null) {
			object.id = id;
		}
		return result;
	}
}