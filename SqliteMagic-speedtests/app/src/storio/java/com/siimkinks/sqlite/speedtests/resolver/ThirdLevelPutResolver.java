package com.siimkinks.sqlite.speedtests.resolver;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.operations.put.PutResolver;
import com.pushtorefresh.storio.sqlite.operations.put.PutResult;
import com.pushtorefresh.storio.sqlite.operations.put.PutResults;
import com.siimkinks.sqlite.speedtests.LibInitializer;
import com.siimkinks.sqlite.speedtests.data.FirstLevel;
import com.siimkinks.sqlite.speedtests.data.MixedData;
import com.siimkinks.sqlite.speedtests.data.SecondLevel;
import com.siimkinks.sqlite.speedtests.data.ThirdLevel;

import java.util.HashSet;
import java.util.Set;

import static java.util.Arrays.asList;

public final class ThirdLevelPutResolver extends PutResolver<ThirdLevel> {
	@NonNull
	@Override
	public PutResult performPut(@NonNull StorIOSQLite storIOSQLite, @NonNull ThirdLevel object) {
		final PutResults<SecondLevel> dependencyResults = storIOSQLite
				.put()
				.objects(asList(object.one, object.two))
				.prepare()
				.executeAsBlocking();
		final ContentValues contentValues = new ContentValues();
		contentValues.put(ThirdLevel.C_ONE, object.one.id);
		contentValues.put(ThirdLevel.C_TWO, object.two.id);
		final SQLiteDatabase db = LibInitializer.dbHelper.getWritableDatabase();
		final int updateCount = db.update(ThirdLevel.TABLE, contentValues, ThirdLevel.C_ID + "=?", new String[]{String.valueOf(object.id)});
		if (updateCount <= 0) {
			final long id = db.insert(ThirdLevel.TABLE, null, contentValues);
			if (id != -1) {
				object.id = id;
			}
		}
		final Set<String> affectedTables = new HashSet<>(2);

		affectedTables.add(MixedData.TABLE);
		affectedTables.add(FirstLevel.TABLE);
		affectedTables.add(SecondLevel.TABLE);
		affectedTables.add(ThirdLevel.TABLE);
		return PutResult.newUpdateResult(dependencyResults.numberOfUpdates() + dependencyResults.numberOfInserts() + 1, affectedTables);
	}
}
