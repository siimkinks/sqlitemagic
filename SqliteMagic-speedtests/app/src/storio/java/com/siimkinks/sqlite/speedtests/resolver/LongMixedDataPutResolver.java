package com.siimkinks.sqlite.speedtests.resolver;

import android.content.ContentValues;
import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.operations.put.DefaultPutResolver;
import com.pushtorefresh.storio.sqlite.operations.put.PutResult;
import com.pushtorefresh.storio.sqlite.queries.InsertQuery;
import com.pushtorefresh.storio.sqlite.queries.UpdateQuery;
import com.siimkinks.sqlite.speedtests.data.LongMixedData;

public final class LongMixedDataPutResolver extends DefaultPutResolver<LongMixedData> {
	/**
	 * {@inheritDoc}
	 */
	@Override
	@NonNull
	protected InsertQuery mapToInsertQuery(@NonNull LongMixedData object) {
		return InsertQuery.builder()
				.table("longmixeddata")
				.build();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@NonNull
	protected UpdateQuery mapToUpdateQuery(@NonNull LongMixedData object) {
		return UpdateQuery.builder()
				.table("longmixeddata")
				.where("_id = ?")
				.whereArgs(object.id)
				.build();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@NonNull
	public ContentValues mapToContentValues(@NonNull LongMixedData object) {
		ContentValues contentValues = new ContentValues(45);

		contentValues.put("seven_f", object.sevenF);
		contentValues.put("nine", object.nine);
		contentValues.put("six_d", object.sixD);
		contentValues.put("one_f", object.oneF);
		contentValues.put("three_d", object.threeD);
		contentValues.put("seven_i", object.sevenI);
		contentValues.put("one_d", object.oneD);
		contentValues.put("three_f", object.threeF);
		contentValues.put("seven_d", object.sevenD);
		contentValues.put("three_i", object.threeI);
		contentValues.put("seven", object.seven);
		contentValues.put("six_l", object.sixL);
		contentValues.put("six_i", object.sixI);
		contentValues.put("three_l", object.threeL);
		contentValues.put("two", object.two);
		contentValues.put("three", object.three);
		contentValues.put("seven_l", object.sevenL);
		contentValues.put("eight", object.eight);
		contentValues.put("six_f", object.sixF);
		contentValues.put("five_i", object.fiveI);
		contentValues.put("five_l", object.fiveL);
		contentValues.put("five_f", object.fiveF);
		contentValues.put("four", object.four);
		contentValues.put("five_d", object.fiveD);
		contentValues.put("ten", object.ten);
		contentValues.put("one_l", object.oneL);
		contentValues.put("five", object.five);
		contentValues.put("one_i", object.oneI);
		contentValues.put("eight_l", object.eightL);
		contentValues.put("six", object.six);
		contentValues.put("two_l", object.twoL);
		contentValues.put("one", object.one);
		contentValues.put("eight_i", object.eightI);
		contentValues.put("eight_f", object.eightF);
		contentValues.put("two_f", object.twoF);
		contentValues.put("four_d", object.fourD);
		contentValues.put("eight_d", object.eightD);
		contentValues.put("two_d", object.twoD);
		contentValues.put("two_i", object.twoI);
		contentValues.put("four_f", object.fourF);
		contentValues.put("four_i", object.fourI);
		contentValues.put("four_l", object.fourL);
		contentValues.put("twelve", object.twelve);
		contentValues.put("eleven", object.eleven);
		contentValues.put("_id", object.id);

		return contentValues;
	}

	@NonNull
	@Override
	public PutResult performPut(@NonNull StorIOSQLite storIOSQLite, @NonNull LongMixedData object) {
		final PutResult result = super.performPut(storIOSQLite, object);
		final Long id = result.insertedId();
		if (id != null) {
			object.id = id;
		}
		return result;
	}
}
