package com.siimkinks.sqlite.speedtests.handlers;

import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.queries.DeleteQuery;
import com.pushtorefresh.storio.sqlite.queries.Query;
import com.siimkinks.sqlite.speedtests.data.DataHandler;
import com.siimkinks.sqlite.speedtests.data.FirstLevel;
import com.siimkinks.sqlite.speedtests.data.MixedData;

import java.util.List;

import static com.siimkinks.sqlite.speedtests.LibInitializer.storIOSQLite;

public final class FirstLevelDataHandler extends DataHandler<FirstLevel> {
	public static final Query QUERY_ALL = Query.builder().table(FirstLevel.TABLE).build();
	private static final Query QUERY_FIRST = Query.builder().table(FirstLevel.TABLE).limit(1).build();

	@Override
	public void deleteTable() {
		storIOSQLite
				.delete()
				.byQuery(DeleteQuery.builder()
						.table(FirstLevel.TABLE)
						.build())
				.prepare()
				.executeAsBlocking();
		storIOSQLite
				.delete()
				.byQuery(DeleteQuery.builder()
						.table(MixedData.TABLE)
						.build())
				.prepare()
				.executeAsBlocking();
	}

	@Override
	public void bulkInsert(@NonNull List<FirstLevel> data) {
		storIOSQLite
				.put()
				.objects(data)
				.useTransaction(true)
				.prepare()
				.executeAsBlocking()
				.results();
	}

	@Override
	public void bulkUpdate(@NonNull List<FirstLevel> data) {
		storIOSQLite
				.put()
				.objects(data)
				.useTransaction(true)
				.prepare()
				.executeAsBlocking();
	}

	@Override
	public void bulkPersist(@NonNull List<FirstLevel> data) {
		storIOSQLite
				.put()
				.objects(data)
				.useTransaction(true)
				.prepare()
				.executeAsBlocking();
	}

	@NonNull
	@Override
	public List<FirstLevel> queryAll() {
		return storIOSQLite
				.get()
				.listOfObjects(FirstLevel.class)
				.withQuery(QUERY_ALL)
				.prepare()
				.executeAsBlocking();
	}

	@NonNull
	@Override
	public FirstLevel queryFirst() {
		return storIOSQLite
				.get()
				.listOfObjects(FirstLevel.class)
				.withQuery(QUERY_FIRST)
				.prepare()
				.executeAsBlocking()
				.get(0);
	}

	@Override
	public long countAll() {
		return storIOSQLite
				.get()
				.numberOfResults()
				.withQuery(QUERY_ALL)
				.prepare()
				.executeAsBlocking();
	}

	@Override
	public long countDependencies() {
		return storIOSQLite
				.get()
				.numberOfResults()
				.withQuery(MixedDataHandler.QUERY_ALL)
				.prepare()
				.executeAsBlocking();
	}

	@Override
	public long expectedDependenciesSize(int size) {
		return size * 2;
	}
}
