package com.siimkinks.sqlite.speedtests;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import com.siimkinks.sqlite.speedtests.data.FirstLevel;
import com.siimkinks.sqlite.speedtests.data.FourthLevel;
import com.siimkinks.sqlite.speedtests.data.LongMixedData;
import com.siimkinks.sqlite.speedtests.data.MixedData;
import com.siimkinks.sqlite.speedtests.data.NumericData;
import com.siimkinks.sqlite.speedtests.data.PingData;
import com.siimkinks.sqlite.speedtests.data.SecondLevel;
import com.siimkinks.sqlite.speedtests.data.StringsData;
import com.siimkinks.sqlite.speedtests.data.ThirdLevel;

public class DbHelper extends SQLiteOpenHelper {

	public DbHelper(@NonNull Context context) {
		super(context, "storio.db", null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(LongMixedData.CREATE);
		db.execSQL(MixedData.CREATE);
		db.execSQL(NumericData.CREATE);
		db.execSQL(PingData.CREATE);
		db.execSQL(StringsData.CREATE);
		db.execSQL(FirstLevel.CREATE);
		db.execSQL(SecondLevel.CREATE);
		db.execSQL(ThirdLevel.CREATE);
		db.execSQL(FourthLevel.CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}
}
