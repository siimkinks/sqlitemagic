package com.siimkinks.sqlite.speedtests.handlers;

import com.siimkinks.sqlite.speedtests.data.PingData;

import static com.siimkinks.sqlite.speedtests.LibInitializer.storIOSQLite;

public final class PingDataHandler {
	public static void insert(PingData pingData) {
		pingData.id = storIOSQLite
				.put()
				.object(pingData)
				.prepare()
				.executeAsBlocking()
				.insertedId();
	}
}
