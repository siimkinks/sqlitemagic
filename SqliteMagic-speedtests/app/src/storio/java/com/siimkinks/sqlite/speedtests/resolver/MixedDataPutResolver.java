package com.siimkinks.sqlite.speedtests.resolver;

import android.content.ContentValues;
import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.operations.put.DefaultPutResolver;
import com.pushtorefresh.storio.sqlite.operations.put.PutResult;
import com.pushtorefresh.storio.sqlite.queries.InsertQuery;
import com.pushtorefresh.storio.sqlite.queries.UpdateQuery;
import com.siimkinks.sqlite.speedtests.data.MixedData;

public class MixedDataPutResolver extends DefaultPutResolver<MixedData> {
	@Override
	@NonNull
	protected InsertQuery mapToInsertQuery(@NonNull MixedData object) {
		return InsertQuery.builder()
				.table("mixeddata")
				.build();
	}

	@Override
	@NonNull
	protected UpdateQuery mapToUpdateQuery(@NonNull MixedData object) {
		return UpdateQuery.builder()
				.table("mixeddata")
				.where("_id = ?")
				.whereArgs(object.id)
				.build();
	}

	@Override
	@NonNull
	public ContentValues mapToContentValues(@NonNull MixedData object) {
		ContentValues contentValues = new ContentValues(13);

		contentValues.put("two_l", object.twoL);
		contentValues.put("three_d", object.threeD);
		contentValues.put("one_d", object.oneD);
		contentValues.put("one", object.one);
		contentValues.put("four_d", object.fourD);
		contentValues.put("three_l", object.threeL);
		contentValues.put("two_d", object.twoD);
		contentValues.put("two", object.two);
		contentValues.put("three", object.three);
		contentValues.put("four_l", object.fourL);
		contentValues.put("four", object.four);
		contentValues.put("_id", object.id);
		contentValues.put("one_l", object.oneL);

		return contentValues;
	}

	@NonNull
	@Override
	public PutResult performPut(@NonNull StorIOSQLite storIOSQLite, @NonNull MixedData object) {
		final PutResult result = super.performPut(storIOSQLite, object);
		final Long id = result.insertedId();
		if (id != null) {
			object.id = id;
		}
		return result;
	}
}
