package com.siimkinks.sqlite.speedtests.resolver;

import android.database.Cursor;
import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.operations.get.DefaultGetResolver;
import com.pushtorefresh.storio.sqlite.queries.Query;
import com.siimkinks.sqlite.speedtests.LibInitializer;
import com.siimkinks.sqlite.speedtests.data.FirstLevel;
import com.siimkinks.sqlite.speedtests.data.MixedData;

public final class FirstLevelGetResolver extends DefaultGetResolver<FirstLevel> {
	@NonNull
	@Override
	public FirstLevel mapFromCursor(@NonNull Cursor cursor) {
		final FirstLevel o = new FirstLevel();
		o.id = cursor.getLong(cursor.getColumnIndex(FirstLevel.C_ID));
		final long oneId = cursor.getLong(cursor.getColumnIndex(FirstLevel.C_ONE));
		o.one = LibInitializer.storIOSQLite.get()
				.listOfObjects(MixedData.class)
				.withQuery(Query.builder()
						.table(MixedData.TABLE)
						.where(MixedData.C_ID + "=?")
						.whereArgs(oneId)
						.build())
				.prepare()
				.executeAsBlocking()
				.get(0);

		final long twoId = cursor.getLong(cursor.getColumnIndex(FirstLevel.C_TWO));
		o.two = LibInitializer.storIOSQLite.get()
				.listOfObjects(MixedData.class)
				.withQuery(Query.builder()
						.table(MixedData.TABLE)
						.where(MixedData.C_ID + "=?")
						.whereArgs(twoId)
						.build())
				.prepare()
				.executeAsBlocking()
				.get(0);
		return o;
	}
}
