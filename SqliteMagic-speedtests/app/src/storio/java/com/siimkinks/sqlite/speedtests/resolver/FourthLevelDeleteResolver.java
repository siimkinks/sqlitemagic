package com.siimkinks.sqlite.speedtests.resolver;

import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.operations.delete.DeleteResolver;
import com.pushtorefresh.storio.sqlite.operations.delete.DeleteResult;
import com.siimkinks.sqlite.speedtests.data.FourthLevel;

public final class FourthLevelDeleteResolver extends DeleteResolver<FourthLevel> {
	@NonNull
	@Override
	public DeleteResult performDelete(@NonNull StorIOSQLite storIOSQLite, @NonNull FourthLevel firstLevel) {

		return null;
	}
}
