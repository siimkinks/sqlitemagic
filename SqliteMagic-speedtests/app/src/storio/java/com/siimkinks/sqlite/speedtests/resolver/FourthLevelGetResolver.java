package com.siimkinks.sqlite.speedtests.resolver;

import android.database.Cursor;
import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.operations.get.DefaultGetResolver;
import com.pushtorefresh.storio.sqlite.queries.Query;
import com.siimkinks.sqlite.speedtests.LibInitializer;
import com.siimkinks.sqlite.speedtests.data.ThirdLevel;
import com.siimkinks.sqlite.speedtests.data.FourthLevel;

public final class FourthLevelGetResolver extends DefaultGetResolver<FourthLevel> {
	@NonNull
	@Override
	public FourthLevel mapFromCursor(@NonNull Cursor cursor) {
		final FourthLevel o = new FourthLevel();
		o.id = cursor.getLong(cursor.getColumnIndex(FourthLevel.C_ID));
		final long oneId = cursor.getLong(cursor.getColumnIndex(FourthLevel.C_ONE));
		o.one = LibInitializer.storIOSQLite.get()
				.listOfObjects(ThirdLevel.class)
				.withQuery(Query.builder()
						.table(ThirdLevel.TABLE)
						.where(ThirdLevel.C_ID + "=?")
						.whereArgs(oneId)
						.build())
				.prepare()
				.executeAsBlocking()
				.get(0);

		final long twoId = cursor.getLong(cursor.getColumnIndex(FourthLevel.C_TWO));
		o.two = LibInitializer.storIOSQLite.get()
				.listOfObjects(ThirdLevel.class)
				.withQuery(Query.builder()
						.table(ThirdLevel.TABLE)
						.where(ThirdLevel.C_ID + "=?")
						.whereArgs(twoId)
						.build())
				.prepare()
				.executeAsBlocking()
				.get(0);
		return o;
	}
}
