package com.siimkinks.sqlite.speedtests.resolver;

import android.database.Cursor;
import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.operations.get.DefaultGetResolver;
import com.pushtorefresh.storio.sqlite.queries.Query;
import com.siimkinks.sqlite.speedtests.LibInitializer;
import com.siimkinks.sqlite.speedtests.data.SecondLevel;
import com.siimkinks.sqlite.speedtests.data.ThirdLevel;

public final class ThirdLevelGetResolver extends DefaultGetResolver<ThirdLevel> {
	@NonNull
	@Override
	public ThirdLevel mapFromCursor(@NonNull Cursor cursor) {
		final ThirdLevel o = new ThirdLevel();
		o.id = cursor.getLong(cursor.getColumnIndex(ThirdLevel.C_ID));
		final long oneId = cursor.getLong(cursor.getColumnIndex(ThirdLevel.C_ONE));
		o.one = LibInitializer.storIOSQLite.get()
				.listOfObjects(SecondLevel.class)
				.withQuery(Query.builder()
						.table(SecondLevel.TABLE)
						.where(SecondLevel.C_ID + "=?")
						.whereArgs(oneId)
						.build())
				.prepare()
				.executeAsBlocking()
				.get(0);

		final long twoId = cursor.getLong(cursor.getColumnIndex(ThirdLevel.C_TWO));
		o.two = LibInitializer.storIOSQLite.get()
				.listOfObjects(SecondLevel.class)
				.withQuery(Query.builder()
						.table(SecondLevel.TABLE)
						.where(SecondLevel.C_ID + "=?")
						.whereArgs(twoId)
						.build())
				.prepare()
				.executeAsBlocking()
				.get(0);
		return o;
	}
}
