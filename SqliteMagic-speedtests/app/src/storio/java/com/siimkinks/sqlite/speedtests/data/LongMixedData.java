package com.siimkinks.sqlite.speedtests.data;

import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
@StorIOSQLiteType(table = LongMixedData.TABLE)
public final class LongMixedData {
	public static final String CREATE = "CREATE TABLE IF NOT EXISTS longmixeddata (one TEXT, two TEXT, three TEXT, four TEXT, five TEXT, six TEXT, seven TEXT, eight TEXT, nine TEXT, ten TEXT, eleven TEXT, twelve TEXT, one_l INTEGER, two_l INTEGER, three_l INTEGER, four_l INTEGER, five_l INTEGER, six_l INTEGER, seven_l INTEGER, eight_l INTEGER, one_d REAL, two_d REAL, three_d REAL, four_d REAL, five_d REAL, six_d REAL, seven_d REAL, eight_d REAL, one_i INTEGER, two_i INTEGER, three_i INTEGER, four_i INTEGER, five_i INTEGER, six_i INTEGER, seven_i INTEGER, eight_i INTEGER, one_f REAL, two_f REAL, three_f REAL, four_f REAL, five_f REAL, six_f REAL, seven_f REAL, eight_f REAL, _id INTEGER PRIMARY KEY AUTOINCREMENT)";
	public static final String TABLE = "longmixeddata";
	public static final String C_ID = "_id";
	public static final String C_ONE = "one";
	public static final String C_TWO = "two";
	public static final String C_THREE = "three";
	public static final String C_FOUR = "four";
	public static final String C_FIVE = "five";
	public static final String C_SIX = "six";
	public static final String C_SEVEN = "seven";
	public static final String C_EIGHT = "eight";
	public static final String C_NINE = "nine";
	public static final String C_TEN = "ten";
	public static final String C_ELEVEN = "eleven";
	public static final String C_TWELVE = "twelve";
	public static final String C_ONE_L = "one_l";
	public static final String C_TWO_L = "two_l";
	public static final String C_THREE_L = "three_l";
	public static final String C_FOUR_L = "four_l";
	public static final String C_FIVE_L = "five_l";
	public static final String C_SIX_L = "six_l";
	public static final String C_SEVEN_L = "seven_l";
	public static final String C_EIGHT_L = "eight_l";
	public static final String C_ONE_D = "one_d";
	public static final String C_TWO_D = "two_d";
	public static final String C_THREE_D = "three_d";
	public static final String C_FOUR_D = "four_d";
	public static final String C_FIVE_D = "five_d";
	public static final String C_SIX_D = "six_d";
	public static final String C_SEVEN_D = "seven_d";
	public static final String C_EIGHT_D = "eight_d";
	public static final String C_ONE_I = "one_i";
	public static final String C_TWO_I = "two_i";
	public static final String C_THREE_I = "three_i";
	public static final String C_FOUR_I = "four_i";
	public static final String C_FIVE_I = "five_i";
	public static final String C_SIX_I = "six_i";
	public static final String C_SEVEN_I = "seven_i";
	public static final String C_EIGHT_I = "eight_i";
	public static final String C_ONE_F = "one_f";
	public static final String C_TWO_F = "two_f";
	public static final String C_THREE_F = "three_f";
	public static final String C_FOUR_F = "four_f";
	public static final String C_FIVE_F = "five_f";
	public static final String C_SIX_F = "six_f";
	public static final String C_SEVEN_F = "seven_f";
	public static final String C_EIGHT_F = "eight_f";

	@StorIOSQLiteColumn(name = LongMixedData.C_ID, key = true)
	public Long id;

	@NonNull
	@StorIOSQLiteColumn(name = C_ONE)
	public String one;
	@NonNull
	@StorIOSQLiteColumn(name = C_TWO)
	public String two;
	@StorIOSQLiteColumn(name = C_THREE)
	public String three; // null
	@StorIOSQLiteColumn(name = C_FOUR)
	public String four; // null
	@NonNull
	@StorIOSQLiteColumn(name = C_FIVE)
	public String five;
	@NonNull
	@StorIOSQLiteColumn(name = C_SIX)
	public String six;
	@StorIOSQLiteColumn(name = C_SEVEN)
	public String seven; // null
	@StorIOSQLiteColumn(name = C_EIGHT)
	public String eight; // null
	@NonNull
	@StorIOSQLiteColumn(name = C_NINE)
	public String nine;
	@NonNull
	@StorIOSQLiteColumn(name = C_TEN)
	public String ten;
	@StorIOSQLiteColumn(name = C_ELEVEN)
	public String eleven; // null
	@StorIOSQLiteColumn(name = C_TWELVE)
	public String twelve; // null
	@StorIOSQLiteColumn(name = C_ONE_L)
	public long oneL;
	@StorIOSQLiteColumn(name = C_TWO_L)
	public long twoL;
	@NonNull
	@StorIOSQLiteColumn(name = C_THREE_L)
	public Long threeL;
	@StorIOSQLiteColumn(name = C_FOUR_L)
	public Long fourL; // null
	@StorIOSQLiteColumn(name = C_FIVE_L)
	public long fiveL;
	@StorIOSQLiteColumn(name = C_SIX_L)
	public long sixL;
	@NonNull
	@StorIOSQLiteColumn(name = C_SEVEN_L)
	public Long sevenL;
	@StorIOSQLiteColumn(name = C_EIGHT_L)
	public Long eightL; // null
	@StorIOSQLiteColumn(name = C_ONE_D)
	public double oneD;
	@StorIOSQLiteColumn(name = C_TWO_D)
	public double twoD;
	@NonNull
	@StorIOSQLiteColumn(name = C_THREE_D)
	public Double threeD;
	@StorIOSQLiteColumn(name = C_FOUR_D)
	public Double fourD; // null
	@StorIOSQLiteColumn(name = C_FIVE_D)
	public double fiveD;
	@StorIOSQLiteColumn(name = C_SIX_D)
	public double sixD;
	@NonNull
	@StorIOSQLiteColumn(name = C_SEVEN_D)
	public Double sevenD;
	@StorIOSQLiteColumn(name = C_EIGHT_D)
	public Double eightD; // null
	@StorIOSQLiteColumn(name = C_ONE_I)
	public int oneI;
	@StorIOSQLiteColumn(name = C_TWO_I)
	public int twoI;
	@NonNull
	@StorIOSQLiteColumn(name = C_THREE_I)
	public Integer threeI;
	@StorIOSQLiteColumn(name = C_FOUR_I)
	public Integer fourI; // null
	@StorIOSQLiteColumn(name = C_FIVE_I)
	public int fiveI;
	@StorIOSQLiteColumn(name = C_SIX_I)
	public int sixI;
	@NonNull
	@StorIOSQLiteColumn(name = C_SEVEN_I)
	public Integer sevenI;
	@StorIOSQLiteColumn(name = C_EIGHT_I)
	public Integer eightI; // null
	@StorIOSQLiteColumn(name = C_ONE_F)
	public float oneF;
	@StorIOSQLiteColumn(name = C_TWO_F)
	public float twoF;
	@NonNull
	@StorIOSQLiteColumn(name = C_THREE_F)
	public Float threeF;
	@StorIOSQLiteColumn(name = C_FOUR_F)
	public Float fourF; // null
	@StorIOSQLiteColumn(name = C_FIVE_F)
	public float fiveF;
	@StorIOSQLiteColumn(name = C_SIX_F)
	public float sixF;
	@NonNull
	@StorIOSQLiteColumn(name = C_SEVEN_F)
	public Float sevenF;
	@StorIOSQLiteColumn(name = C_EIGHT_F)
	public Float eightF; // null
}
