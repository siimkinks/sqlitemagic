package com.siimkinks.sqlite.speedtests.handlers;


import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.queries.DeleteQuery;
import com.pushtorefresh.storio.sqlite.queries.Query;
import com.siimkinks.sqlite.speedtests.data.DataHandler;
import com.siimkinks.sqlite.speedtests.data.NumericData;

import java.util.List;

import static com.siimkinks.sqlite.speedtests.LibInitializer.storIOSQLite;

public final class NumericDataHandler extends DataHandler<NumericData> {
	private static final Query QUERY_ALL = Query.builder().table(NumericData.TABLE).build();
	private static final Query QUERY_FIRST = Query.builder().table(NumericData.TABLE).limit(1).build();

	@Override
	public void deleteTable() {
		storIOSQLite
				.delete()
				.byQuery(DeleteQuery.builder()
						.table(NumericData.TABLE)
						.build())
				.prepare()
				.executeAsBlocking();
	}

	@Override
	public void bulkInsert(@NonNull List<NumericData> data) {
		storIOSQLite
				.put()
				.objects(data)
				.useTransaction(true)
				.prepare()
				.executeAsBlocking()
				.results();
	}

	@Override
	public void bulkUpdate(@NonNull List<NumericData> data) {
		storIOSQLite
				.put()
				.objects(data)
				.useTransaction(true)
				.prepare()
				.executeAsBlocking();
	}

	@Override
	public void bulkPersist(@NonNull List<NumericData> data) {
		storIOSQLite
				.put()
				.objects(data)
				.useTransaction(true)
				.prepare()
				.executeAsBlocking();
	}

	@NonNull
	@Override
	public List<NumericData> queryAll() {
		return storIOSQLite
				.get()
				.listOfObjects(NumericData.class)
				.withQuery(QUERY_ALL)
				.prepare()
				.executeAsBlocking();
	}

	@NonNull
	@Override
	public NumericData queryFirst() {
		return storIOSQLite
				.get()
				.listOfObjects(NumericData.class)
				.withQuery(QUERY_FIRST)
				.prepare()
				.executeAsBlocking()
				.get(0);
	}

	@Override
	public long countAll() {
		return storIOSQLite
				.get()
				.numberOfResults()
				.withQuery(QUERY_ALL)
				.prepare()
				.executeAsBlocking();
	}

	@Override
	public long countDependencies() {
		return 0;
	}

	@Override
	public long expectedDependenciesSize(int size) {
		return 0;
	}
}
