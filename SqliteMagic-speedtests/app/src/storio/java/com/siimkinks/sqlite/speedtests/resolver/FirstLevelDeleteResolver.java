package com.siimkinks.sqlite.speedtests.resolver;

import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.operations.delete.DeleteResolver;
import com.pushtorefresh.storio.sqlite.operations.delete.DeleteResult;
import com.pushtorefresh.storio.sqlite.queries.DeleteQuery;
import com.siimkinks.sqlite.speedtests.data.FirstLevel;
import com.siimkinks.sqlite.speedtests.data.MixedData;

import java.util.HashSet;
import java.util.Set;

public final class FirstLevelDeleteResolver extends DeleteResolver<FirstLevel> {
	@NonNull
	@Override
	public DeleteResult performDelete(@NonNull StorIOSQLite storIOSQLite, @NonNull FirstLevel firstLevel) {
		storIOSQLite.delete()
				.object(firstLevel.one)
				.prepare()
				.executeAsBlocking();
		storIOSQLite.delete()
				.object(firstLevel.two)
				.prepare()
				.executeAsBlocking();
		storIOSQLite.delete()
				.byQuery(DeleteQuery.builder()
						.table(FirstLevel.TABLE)
						.where(FirstLevel.C_ID + "=?")
						.whereArgs(firstLevel.id)
						.build())
				.prepare()
				.executeAsBlocking();
		final Set<String> affectedTables = new HashSet<>(2);

		affectedTables.add(MixedData.TABLE);
		affectedTables.add(FirstLevel.TABLE);

		return DeleteResult.newInstance(2, affectedTables);
	}
}
