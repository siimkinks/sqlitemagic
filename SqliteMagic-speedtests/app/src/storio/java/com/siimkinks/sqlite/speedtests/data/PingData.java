package com.siimkinks.sqlite.speedtests.data;

import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
@StorIOSQLiteType(table = PingData.TABLE)
public final class PingData {
	public static final String CREATE = "CREATE TABLE IF NOT EXISTS pingdata (test TEXT, _id INTEGER PRIMARY KEY AUTOINCREMENT)";
	public static final String TABLE = "pingdata";
	public static final String C_ID = "_id";
	public static final String C_TEST = "test";

	@StorIOSQLiteColumn(name = C_ID, key = true)
	public Long id;
	@StorIOSQLiteColumn(name = C_TEST)
	String test;
}
