package com.siimkinks.sqlite.speedtests;

import com.pushtorefresh.storio.sqlite.SQLiteTypeMapping;
import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.impl.DefaultStorIOSQLite;
import com.siimkinks.sqlite.speedtests.data.FirstLevel;
import com.siimkinks.sqlite.speedtests.data.FourthLevel;
import com.siimkinks.sqlite.speedtests.data.LongMixedData;
import com.siimkinks.sqlite.speedtests.data.LongMixedDataStorIOSQLiteDeleteResolver;
import com.siimkinks.sqlite.speedtests.data.MixedData;
import com.siimkinks.sqlite.speedtests.data.MixedDataStorIOSQLiteDeleteResolver;
import com.siimkinks.sqlite.speedtests.data.NumericData;
import com.siimkinks.sqlite.speedtests.data.NumericDataStorIOSQLiteDeleteResolver;
import com.siimkinks.sqlite.speedtests.data.NumericDataStorIOSQLiteGetResolver;
import com.siimkinks.sqlite.speedtests.data.PingData;
import com.siimkinks.sqlite.speedtests.data.PingDataStorIOSQLiteDeleteResolver;
import com.siimkinks.sqlite.speedtests.data.PingDataStorIOSQLiteGetResolver;
import com.siimkinks.sqlite.speedtests.data.PingDataStorIOSQLitePutResolver;
import com.siimkinks.sqlite.speedtests.data.SecondLevel;
import com.siimkinks.sqlite.speedtests.data.StringsData;
import com.siimkinks.sqlite.speedtests.data.StringsDataStorIOSQLiteDeleteResolver;
import com.siimkinks.sqlite.speedtests.data.StringsDataStorIOSQLiteGetResolver;
import com.siimkinks.sqlite.speedtests.data.ThirdLevel;
import com.siimkinks.sqlite.speedtests.resolver.FirstLevelDeleteResolver;
import com.siimkinks.sqlite.speedtests.resolver.FirstLevelGetResolver;
import com.siimkinks.sqlite.speedtests.resolver.FirstLevelPutResolver;
import com.siimkinks.sqlite.speedtests.resolver.FourthLevelDeleteResolver;
import com.siimkinks.sqlite.speedtests.resolver.FourthLevelGetResolver;
import com.siimkinks.sqlite.speedtests.resolver.FourthLevelPutResolver;
import com.siimkinks.sqlite.speedtests.resolver.LongMixedDataPutResolver;
import com.siimkinks.sqlite.speedtests.resolver.LongMixedDataStorIOSQLiteGetResolver;
import com.siimkinks.sqlite.speedtests.resolver.MixedDataGetResolver;
import com.siimkinks.sqlite.speedtests.resolver.MixedDataPutResolver;
import com.siimkinks.sqlite.speedtests.resolver.NumericDataPutResolver;
import com.siimkinks.sqlite.speedtests.resolver.SecondLevelDeleteResolver;
import com.siimkinks.sqlite.speedtests.resolver.SecondLevelGetResolver;
import com.siimkinks.sqlite.speedtests.resolver.SecondLevelPutResolver;
import com.siimkinks.sqlite.speedtests.resolver.StringsDataPutResolver;
import com.siimkinks.sqlite.speedtests.resolver.ThirdLevelDeleteResolver;
import com.siimkinks.sqlite.speedtests.resolver.ThirdLevelGetResolver;
import com.siimkinks.sqlite.speedtests.resolver.ThirdLevelPutResolver;

public class LibInitializer implements Initializer {

	public static StorIOSQLite storIOSQLite;
	public static DbHelper dbHelper;

	@Override
	public void init(App app) {
		dbHelper = new DbHelper(app);
		storIOSQLite = DefaultStorIOSQLite.builder()
				.sqliteOpenHelper(dbHelper)
				.addTypeMapping(LongMixedData.class, SQLiteTypeMapping.<LongMixedData>builder()
						.putResolver(new LongMixedDataPutResolver())
						.getResolver(new LongMixedDataStorIOSQLiteGetResolver())
						.deleteResolver(new LongMixedDataStorIOSQLiteDeleteResolver())
						.build())
				.addTypeMapping(MixedData.class, SQLiteTypeMapping.<MixedData>builder()
						.putResolver(new MixedDataPutResolver())
						.getResolver(new MixedDataGetResolver())
						.deleteResolver(new MixedDataStorIOSQLiteDeleteResolver())
						.build())
				.addTypeMapping(NumericData.class, SQLiteTypeMapping.<NumericData>builder()
						.putResolver(new NumericDataPutResolver())
						.getResolver(new NumericDataStorIOSQLiteGetResolver())
						.deleteResolver(new NumericDataStorIOSQLiteDeleteResolver())
						.build())
				.addTypeMapping(PingData.class, SQLiteTypeMapping.<PingData>builder()
						.putResolver(new PingDataStorIOSQLitePutResolver())
						.getResolver(new PingDataStorIOSQLiteGetResolver())
						.deleteResolver(new PingDataStorIOSQLiteDeleteResolver())
						.build())
				.addTypeMapping(StringsData.class, SQLiteTypeMapping.<StringsData>builder()
						.putResolver(new StringsDataPutResolver())
						.getResolver(new StringsDataStorIOSQLiteGetResolver())
						.deleteResolver(new StringsDataStorIOSQLiteDeleteResolver())
						.build())
				.addTypeMapping(FirstLevel.class, SQLiteTypeMapping.<FirstLevel>builder()
						.putResolver(new FirstLevelPutResolver())
						.getResolver(new FirstLevelGetResolver())
						.deleteResolver(new FirstLevelDeleteResolver())
						.build())
				.addTypeMapping(SecondLevel.class, SQLiteTypeMapping.<SecondLevel>builder()
						.putResolver(new SecondLevelPutResolver())
						.getResolver(new SecondLevelGetResolver())
						.deleteResolver(new SecondLevelDeleteResolver())
						.build())
				.addTypeMapping(ThirdLevel.class, SQLiteTypeMapping.<ThirdLevel>builder()
						.putResolver(new ThirdLevelPutResolver())
						.getResolver(new ThirdLevelGetResolver())
						.deleteResolver(new ThirdLevelDeleteResolver())
						.build())
				.addTypeMapping(FourthLevel.class, SQLiteTypeMapping.<FourthLevel>builder()
						.putResolver(new FourthLevelPutResolver())
						.getResolver(new FourthLevelGetResolver())
						.deleteResolver(new FourthLevelDeleteResolver())
						.build())
				.build();
	}

	@Override
	public void destroy(App app) {
	}
}
