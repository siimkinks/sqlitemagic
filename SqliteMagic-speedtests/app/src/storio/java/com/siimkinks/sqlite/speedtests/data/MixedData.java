package com.siimkinks.sqlite.speedtests.data;

import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
@StorIOSQLiteType(table = MixedData.TABLE)
public final class MixedData {
	public static final String CREATE = "CREATE TABLE IF NOT EXISTS mixeddata (one TEXT, two TEXT, three TEXT, four TEXT, one_l INTEGER, two_l INTEGER, three_l INTEGER, four_l INTEGER, one_d REAL, two_d REAL, three_d REAL, four_d REAL, _id INTEGER PRIMARY KEY AUTOINCREMENT)";
	public static final String TABLE = "mixeddata";
	public static final String C_ID = "_id";
	public static final String C_ONE = "one";
	public static final String C_TWO = "two";
	public static final String C_THREE = "three";
	public static final String C_FOUR = "four";
	public static final String C_ONE_L = "one_l";
	public static final String C_TWO_L = "two_l";
	public static final String C_THREE_L = "three_l";
	public static final String C_FOUR_L = "four_l";
	public static final String C_ONE_D = "one_d";
	public static final String C_TWO_D = "two_d";
	public static final String C_THREE_D = "three_d";
	public static final String C_FOUR_D = "four_d";

	@StorIOSQLiteColumn(name = C_ID, key = true)
	public Long id;

	@NonNull
	@StorIOSQLiteColumn(name = C_ONE)
	public String one;
	@NonNull
	@StorIOSQLiteColumn(name = C_TWO)
	public String two;
	@StorIOSQLiteColumn(name = C_THREE)
	public String three; // null
	@StorIOSQLiteColumn(name = C_FOUR)
	public String four; // null
	@StorIOSQLiteColumn(name = C_ONE_L)
	public long oneL;
	@StorIOSQLiteColumn(name = C_TWO_L)
	public long twoL;
	@NonNull
	@StorIOSQLiteColumn(name = C_THREE_L)
	public Long threeL;
	@StorIOSQLiteColumn(name = C_FOUR_L)
	public Long fourL; // null
	@StorIOSQLiteColumn(name = C_ONE_D)
	public double oneD;
	@StorIOSQLiteColumn(name = C_TWO_D)
	public double twoD;
	@NonNull
	@StorIOSQLiteColumn(name = C_THREE_D)
	public Double threeD;
	@StorIOSQLiteColumn(name = C_FOUR_D)
	public Double fourD; // null
}
