package com.siimkinks.sqlite.speedtests.data;

import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
@StorIOSQLiteType(table = StringsData.TABLE)
public final class StringsData {
	public static final String CREATE = "CREATE TABLE IF NOT EXISTS stringsdata (one TEXT, two TEXT, three TEXT, four TEXT, five TEXT, six TEXT, seven TEXT, eight TEXT, nine TEXT, ten TEXT, eleven TEXT, twelve TEXT, _id INTEGER PRIMARY KEY AUTOINCREMENT)";
	public static final String TABLE = "stringsdata";
	public static final String C_ID = "_id";
	public static final String C_ONE = "one";
	public static final String C_TWO = "two";
	public static final String C_THREE = "three";
	public static final String C_FOUR = "four";
	public static final String C_FIVE = "five";
	public static final String C_SIX = "six";
	public static final String C_SEVEN = "seven";
	public static final String C_EIGHT = "eight";
	public static final String C_NINE = "nine";
	public static final String C_TEN = "ten";
	public static final String C_ELEVEN = "eleven";
	public static final String C_TWELVE = "twelve";

	@StorIOSQLiteColumn(name = C_ID, key = true)
	public Long id;

	@NonNull
	@StorIOSQLiteColumn(name = C_ONE)
	public String one;
	@NonNull
	@StorIOSQLiteColumn(name = C_TWO)
	public String two;
	@NonNull
	@StorIOSQLiteColumn(name = C_THREE)
	public String three;
	@NonNull
	@StorIOSQLiteColumn(name = C_FOUR)
	public String four;
	@NonNull
	@StorIOSQLiteColumn(name = C_FIVE)
	public String five;
	@NonNull
	@StorIOSQLiteColumn(name = C_SIX)
	public String six;
	@NonNull
	@StorIOSQLiteColumn(name = C_SEVEN)
	public String seven;
	@NonNull
	@StorIOSQLiteColumn(name = C_EIGHT)
	public String eight;
	@NonNull
	@StorIOSQLiteColumn(name = C_NINE)
	public String nine;
	@NonNull
	@StorIOSQLiteColumn(name = C_TEN)
	public String ten;
	@NonNull
	@StorIOSQLiteColumn(name = C_ELEVEN)
	public String eleven;
	@NonNull
	@StorIOSQLiteColumn(name = C_TWELVE)
	public String twelve;
}
