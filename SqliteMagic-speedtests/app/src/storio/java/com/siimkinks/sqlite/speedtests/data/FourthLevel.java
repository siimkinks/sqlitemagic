package com.siimkinks.sqlite.speedtests.data;

import android.support.annotation.NonNull;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public final class FourthLevel {
	public static final String CREATE = "CREATE TABLE IF NOT EXISTS fourthlevel (one INTEGER, two INTEGER, _id INTEGER PRIMARY KEY AUTOINCREMENT)";
	public static final String TABLE = "fourthlevel";
	public static final String C_ID = "_id";
	public static final String C_ONE = "one";
	public static final String C_TWO = "two";

	public Long id;
	@NonNull
	public ThirdLevel one;
	@NonNull
	public ThirdLevel two;
}
