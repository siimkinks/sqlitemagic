package com.siimkinks.sqlite.speedtests.resolver;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.operations.put.PutResolver;
import com.pushtorefresh.storio.sqlite.operations.put.PutResult;
import com.pushtorefresh.storio.sqlite.operations.put.PutResults;
import com.siimkinks.sqlite.speedtests.LibInitializer;
import com.siimkinks.sqlite.speedtests.data.FirstLevel;
import com.siimkinks.sqlite.speedtests.data.FourthLevel;
import com.siimkinks.sqlite.speedtests.data.MixedData;
import com.siimkinks.sqlite.speedtests.data.SecondLevel;
import com.siimkinks.sqlite.speedtests.data.ThirdLevel;

import java.util.HashSet;
import java.util.Set;

import static java.util.Arrays.asList;

public final class FourthLevelPutResolver extends PutResolver<FourthLevel> {
	@NonNull
	@Override
	public PutResult performPut(@NonNull StorIOSQLite storIOSQLite, @NonNull FourthLevel object) {
		final PutResults<ThirdLevel> dependencyResults = storIOSQLite
				.put()
				.objects(asList(object.one, object.two))
				.prepare()
				.executeAsBlocking();
		final ContentValues contentValues = new ContentValues();
		contentValues.put(FourthLevel.C_ONE, object.one.id);
		contentValues.put(FourthLevel.C_TWO, object.two.id);
		final SQLiteDatabase db = LibInitializer.dbHelper.getWritableDatabase();
		final int updateCount = db.update(FourthLevel.TABLE, contentValues, FourthLevel.C_ID + "=?", new String[]{String.valueOf(object.id)});
		if (updateCount <= 0) {
			final long id = db.insert(FourthLevel.TABLE, null, contentValues);
			if (id != -1) {
				object.id = id;
			}
		}
		final Set<String> affectedTables = new HashSet<>(2);

		affectedTables.add(MixedData.TABLE);
		affectedTables.add(FirstLevel.TABLE);
		affectedTables.add(SecondLevel.TABLE);
		affectedTables.add(ThirdLevel.TABLE);
		affectedTables.add(FourthLevel.TABLE);
		return PutResult.newUpdateResult(dependencyResults.numberOfUpdates() + dependencyResults.numberOfInserts() + 1, affectedTables);
	}
}
