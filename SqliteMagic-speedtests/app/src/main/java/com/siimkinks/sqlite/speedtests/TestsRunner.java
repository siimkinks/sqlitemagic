package com.siimkinks.sqlite.speedtests;


import android.util.Log;

import com.siimkinks.sqlite.speedtests.data.DataHandler;
import com.siimkinks.sqlite.speedtests.data.DataProvider;
import com.siimkinks.sqlite.speedtests.data.FirstLevelDataProvider;
import com.siimkinks.sqlite.speedtests.data.FourthLevelDataProvider;
import com.siimkinks.sqlite.speedtests.data.LongMixedDataProvider;
import com.siimkinks.sqlite.speedtests.data.MixedDataProvider;
import com.siimkinks.sqlite.speedtests.data.NumericDataProvider;
import com.siimkinks.sqlite.speedtests.data.StringsDataProvider;
import com.siimkinks.sqlite.speedtests.data.TestData;
import com.siimkinks.sqlite.speedtests.tests.BulkInsertDataTest;
import com.siimkinks.sqlite.speedtests.tests.BulkPersistWithInsertDataTest;
import com.siimkinks.sqlite.speedtests.tests.BulkPersistWithUpdateDataTest;
import com.siimkinks.sqlite.speedtests.tests.BulkUpdateDataTest;
import com.siimkinks.sqlite.speedtests.tests.CountTest;
import com.siimkinks.sqlite.speedtests.tests.EmptyTestCase;
import com.siimkinks.sqlite.speedtests.tests.QueryAllTest;
import com.siimkinks.sqlite.speedtests.tests.QueryFirstTest;

import java.util.ArrayList;
import java.util.List;

public class TestsRunner {
	private static final String TAG = TestsRunner.class.getSimpleName();
	private static final int TEST_SUIT_SIZE = 7;

	private final TestData testData;
	private final List<DataProvider> dataTypes = new ArrayList<>();

	public TestsRunner(TestData testData) {
		this.testData = testData;
		dataTypes.add(new StringsDataProvider(testData));
		dataTypes.add(new NumericDataProvider(testData));
		dataTypes.add(new MixedDataProvider(testData));
		dataTypes.add(new LongMixedDataProvider(testData));
		dataTypes.add(new FirstLevelDataProvider(testData));
		dataTypes.add(new FourthLevelDataProvider(testData));
	}

	@SuppressWarnings("unchecked")
	public TestResults runTests() {
		final TestResults results = new TestResults(dataTypes.size() * TEST_SUIT_SIZE);

		final TestCase emptyTestCase = new EmptyTestCase(testData.generatePingData());
		emptyTestCase.setUp();
		emptyTestCase.run();
		emptyTestCase.tearDown();

		for (DataProvider provider : dataTypes) {
			final List data = provider.generateNew();
			final DataHandler handler = provider.dataHandler();
			runTestCase(results, new BulkInsertDataTest(data, handler));
			runTestCase(results, new CountTest(handler, data.size()));
			runTestCase(results, new QueryFirstTest(handler, data.get(0)));
			runTestCase(results, new QueryAllTest(handler, data));
			runTestCase(results, new BulkUpdateDataTest(data, handler, provider));
			runTestCase(results, new BulkPersistWithUpdateDataTest(data, handler, provider));
			runTestCase(results, new BulkPersistWithInsertDataTest(data, handler));
		}
		Log.d(TAG, "Tests for lib " + results.getLib() + " " + (results.isSuccess() ? "SUCCEEDED" : "FAILED"));
		return results;
	}

	private void runTestCase(TestResults results, TestCase testCase) {
		try {
			long start;
			long end;
			final TestCaseResult result = new TestCaseResult(testCase);
			testCase.setUp();
			start = System.nanoTime();
			testCase.run();
			end = System.nanoTime();
			result.iterations = testCase.getIterationCount();
			testCase.tearDown();
			System.gc();
			result.time = end - start;
			results.addTestCaseResult(result);
		} catch (Throwable e) {
			Log.e(TAG, "Failed to run test " + testCase.getName(), e);
		}
	}
}
