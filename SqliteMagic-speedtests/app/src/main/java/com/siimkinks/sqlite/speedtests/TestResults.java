package com.siimkinks.sqlite.speedtests;

import android.os.Build;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class TestResults {
	private final int expectedTestResultsSize;
	private final String lib;
	private final String modelName;
	private final List<TestCaseResult> testResults = new ArrayList<>();

	public TestResults(int expectedTestResultsSize) {
		this.expectedTestResultsSize = expectedTestResultsSize;
		this.lib = BuildConfig.FLAVOR;
		modelName = String.format("%s %s", Build.MANUFACTURER, Build.MODEL);
	}

	public void addTestCaseResult(@NonNull TestCaseResult result) {
		testResults.add(result);
	}

	public boolean isSuccess() {
		return expectedTestResultsSize == testResults.size();
	}
}
