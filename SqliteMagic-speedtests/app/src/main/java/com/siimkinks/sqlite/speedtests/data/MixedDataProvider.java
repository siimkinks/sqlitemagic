package com.siimkinks.sqlite.speedtests.data;

import android.support.annotation.NonNull;

import com.siimkinks.sqlite.speedtests.handlers.MixedDataHandler;

import java.util.List;

public final class MixedDataProvider implements DataProvider<MixedData> {
	private final TestData testData;
	private final MixedDataHandler dataHandler = new MixedDataHandler();

	public MixedDataProvider(TestData testData) {
		this.testData = testData;
	}

	@NonNull
	@Override
	public DataHandler<MixedData> dataHandler() {
		return dataHandler;
	}

	@NonNull
	@Override
	public List<MixedData> generateNew() {
		return testData.generateMixedData();
	}

	@Override
	public void update(@NonNull List<MixedData> data) {
		testData.updateMixedData(data);
	}
}
