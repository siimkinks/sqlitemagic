package com.siimkinks.sqlite.speedtests.activity;

import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBarActivity;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.siimkinks.sqlite.speedtests.BuildConfig;
import com.siimkinks.sqlite.speedtests.R;
import com.siimkinks.sqlite.speedtests.data.MixedData;
import com.siimkinks.sqlite.speedtests.data.TestData;
import com.siimkinks.sqlite.speedtests.handlers.MixedDataHandler;

import java.util.List;

import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ClickToRunTestsActivity extends ActionBarActivity {
	private static final int DATA_SIZE = 5000;
	private final TestData testData = new TestData();
	private final MixedDataHandler dataHandler = new MixedDataHandler();
	private TextView infoView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_click_to_run);
		this.infoView = (TextView) findViewById(R.id.info_view);
		findViewById(R.id.button_gc)
				.setOnClickListener(v -> System.gc());
		findViewById(R.id.button_delete_table)
				.setOnClickListener(v -> dataHandler.deleteTable());
		addTestButtons();

		updateInfo(BuildConfig.FLAVOR);
	}

	private void addTestButtons() {
		final ViewGroup testContainer = (ViewGroup) findViewById(R.id.test_container);
		addInsertButton(testContainer);
		addUpdateButton(testContainer);
		addPersistButton(testContainer);
		addCountButton(testContainer);
		addQueryFirstButton(testContainer);
		addQueryAllButton(testContainer);
	}

	private void addInsertButton(ViewGroup testContainer) {
		addButton(testContainer, "Insert", subscriber -> {
			subscriber.onNext("Preping");
			List<MixedData> data = testData.generateMixedData(DATA_SIZE * 3);
			dataHandler.deleteTable();
			System.gc();
			sleep(subscriber);

			subscriber.onNext("Running test");
			dataHandler.bulkInsert(data);
			sleep(subscriber);

			data = null;
			finishTest(subscriber);
		});
	}

	private void addUpdateButton(ViewGroup testContainer) {
		addButton(testContainer, "Update", subscriber -> {
			subscriber.onNext("Preping");
			List<MixedData> data = testData.generateMixedData(DATA_SIZE * 3);
			dataHandler.deleteTable();
			dataHandler.bulkInsert(data);
			testData.updateMixedData(data);
			System.gc();
			sleep(subscriber);
			sleep(subscriber);

			subscriber.onNext("Running test");
			dataHandler.bulkUpdate(data);
			sleep(subscriber);

			data = null;
			finishTest(subscriber);
		});
	}

	private void addPersistButton(ViewGroup testContainer) {
		addButton(testContainer, "Persist", subscriber -> {
			subscriber.onNext("Preping");
			List<MixedData> data = testData.generateMixedData(DATA_SIZE * 2);
			dataHandler.deleteTable();
			System.gc();
			sleep(subscriber);

			subscriber.onNext("Running test");
			dataHandler.bulkPersist(data);
			sleep(subscriber);
			testData.updateMixedData(data);
			System.gc();
			sleep(subscriber);
			dataHandler.bulkPersist(data);
			sleep(subscriber);

			data = null;
			finishTest(subscriber);
		});
	}

	private void addCountButton(ViewGroup testContainer) {
		addButton(testContainer, "Count", subscriber -> {
			subscriber.onNext("Preping");
			List<MixedData> data = testData.generateMixedData(DATA_SIZE * 5);
			dataHandler.deleteTable();
			dataHandler.bulkInsert(data);
			data = null;
			System.gc();
			sleep(subscriber);

			subscriber.onNext("Running test");
			for (int i = 0; i < 500; i++) {
				dataHandler.countAll();
			}
			sleep(subscriber);

			finishTest(subscriber);
		});
	}

	private void addQueryFirstButton(ViewGroup testContainer) {
		addButton(testContainer, "Query first", subscriber -> {
			subscriber.onNext("Preping");
			final int iterationCount = 1000;
			List<MixedData> data = testData.generateMixedData(DATA_SIZE);
			dataHandler.deleteTable();
			dataHandler.bulkInsert(data);
			data.clear();
			System.gc();
			sleep(subscriber);

			subscriber.onNext("Running test");
			for (int i = 0; i < iterationCount; i++) {
				data.add(dataHandler.queryFirst());
			}
			sleep(subscriber);

			data = null;
			finishTest(subscriber);
		});
	}

	private void addQueryAllButton(ViewGroup testContainer) {
		addButton(testContainer, "Query all", subscriber -> {
			subscriber.onNext("Preping");
			List<MixedData> data = testData.generateMixedData(DATA_SIZE * 3);
			dataHandler.deleteTable();
			dataHandler.bulkInsert(data);
			data = null;
			System.gc();
			sleep(subscriber);

			subscriber.onNext("Running test");
			dataHandler.queryAll();
			sleep(subscriber);

			finishTest(subscriber);
		});
	}

	private void addButton(ViewGroup testContainer, String btnText, Observable.OnSubscribe<String> action) {
		final Button b = new Button(this);
		b.setText(btnText);
		b.setOnClickListener(v -> Observable.create(action)
				.subscribeOn(Schedulers.computation())
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(updateInfo));
		testContainer.addView(b);
	}

	private void finishTest(Subscriber<? super String> subscriber) {
		System.gc();
		subscriber.onNext("Finished");
		subscriber.onCompleted();
	}

	private void sleep(Subscriber<? super String> subscriber) {
		subscriber.onNext("Sleeping");
		SystemClock.sleep(3000);
	}

	private void updateInfo(@NonNull String infoText) {
		infoView.setText(infoText);
	}

	private final UpdateInfo updateInfo = new UpdateInfo();

	private class UpdateInfo implements Observer<String> {

		@Override
		public void onCompleted() {

		}

		@Override
		public void onError(Throwable e) {
			e.printStackTrace();
		}

		@Override
		public void onNext(String s) {
			updateInfo(s);
		}
	}
}
