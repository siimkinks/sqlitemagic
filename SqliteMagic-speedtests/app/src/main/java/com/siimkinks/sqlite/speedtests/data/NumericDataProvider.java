package com.siimkinks.sqlite.speedtests.data;

import android.support.annotation.NonNull;

import com.siimkinks.sqlite.speedtests.handlers.NumericDataHandler;

import java.util.List;

public final class NumericDataProvider implements DataProvider<NumericData> {
	private final TestData testData;
	private final NumericDataHandler dataHandler = new NumericDataHandler();

	public NumericDataProvider(TestData testData) {
		this.testData = testData;
	}

	@NonNull
	@Override
	public DataHandler<NumericData> dataHandler() {
		return dataHandler;
	}

	@NonNull
	@Override
	public List<NumericData> generateNew() {
		return testData.generateNumericData();
	}

	@Override
	public void update(@NonNull List<NumericData> data) {
		testData.updateNumericData(data);
	}
}
