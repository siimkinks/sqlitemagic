package com.siimkinks.sqlite.speedtests.tests;

import com.siimkinks.sqlite.speedtests.TestCase;
import com.siimkinks.sqlite.speedtests.data.DataHandler;

import java.util.List;

import lombok.Getter;

import static com.google.common.truth.Truth.assertThat;

public final class BulkPersistWithInsertDataTest<DataType> extends TestCase {
	List<DataType> data;
	@Getter
	private final DataHandler<DataType> handler;

	public BulkPersistWithInsertDataTest(List<DataType> data, DataHandler<DataType> handler) {
		this.handler = handler;
		this.data = data;
	}

	@Override
	public void setUp() {
		super.setUp();
		handler.deleteTable();
		assertThat(handler.countAll()).isEqualTo(0);
		assertThat(handler.countDependencies()).isEqualTo(0);
	}

	@Override
	public void run() {
		handler.bulkPersist(data);
	}

	@Override
	public int getIterationCount() {
		return data.size();
	}

	@Override
	public void tearDown() {
		super.tearDown();
		assertThat(handler.countAll()).isEqualTo(data.size());
		assertThat(handler.countDependencies()).isAtLeast(handler.expectedDependenciesSize(data.size()));
		data = null;
	}
}
