package com.siimkinks.sqlite.speedtests.tests;

import com.siimkinks.sqlite.speedtests.TestCase;
import com.siimkinks.sqlite.speedtests.data.DataHandler;
import com.siimkinks.sqlite.speedtests.data.DataProvider;

import java.util.List;

import lombok.Getter;

import static com.google.common.truth.Truth.assertThat;

public final class BulkUpdateDataTest<DataType> extends TestCase {
	List<DataType> data;
	@Getter
	private final DataHandler<DataType> handler;

	public BulkUpdateDataTest(List<DataType> data, DataHandler<DataType> handler, DataProvider<DataType> provider) {
		this.data = data;
		this.handler = handler;
		provider.update(data);
	}

	@Override
	public void setUp() {
		super.setUp();
		assertThat(handler.countAll()).isEqualTo(data.size());
		assertThat(handler.countDependencies()).isEqualTo(handler.expectedDependenciesSize(data.size()));
	}

	@Override
	public void run() {
		handler.bulkUpdate(data);
	}

	@Override
	public int getIterationCount() {
		return data.size();
	}

	@Override
	public void tearDown() {
		super.tearDown();
		assertThat(handler.countAll()).isEqualTo(data.size());
		assertThat(handler.countDependencies()).isAtLeast(handler.expectedDependenciesSize(data.size()));
		data = null;
	}
}
