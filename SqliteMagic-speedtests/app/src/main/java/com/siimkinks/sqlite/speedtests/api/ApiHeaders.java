package com.siimkinks.sqlite.speedtests.api;

import retrofit.RequestInterceptor;

public class ApiHeaders implements RequestInterceptor {
	@Override
	public void intercept(RequestFacade request) {
		request.addHeader("Content-Type", "application/json");
	}
}
