package com.siimkinks.sqlite.speedtests;

import android.app.Application;

public class App extends Application {

	Initializer initializer;
	@Override
	public void onCreate() {
		super.onCreate();
		initializer = new LibInitializer();
		initializer.init(this);
	}
}
