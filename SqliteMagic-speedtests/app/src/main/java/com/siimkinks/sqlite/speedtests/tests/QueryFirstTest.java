package com.siimkinks.sqlite.speedtests.tests;

import com.siimkinks.sqlite.speedtests.TestCase;
import com.siimkinks.sqlite.speedtests.data.DataHandler;

import lombok.Getter;

import static com.google.common.truth.Truth.assertThat;

public final class QueryFirstTest<DataType> extends TestCase {
	private static final int ITERATIONS = 1000;

	@Getter
	private final DataHandler<DataType> handler;
	private final DataType expected;
	private DataType result;

	public QueryFirstTest(DataHandler<DataType> handler, DataType expected) {
		this.handler = handler;
		this.expected = expected;
	}

	@Override
	public void run() {
		final DataHandler<DataType> handler = this.handler;
		for (int i = 0, size = ITERATIONS - 1; i < size; i++) {
			handler.queryFirst();
		}
		this.result = handler.queryFirst();
	}

	@Override
	public int getIterationCount() {
		return ITERATIONS;
	}

	@Override
	public void tearDown() {
		super.tearDown();
		assertThat(result).isEqualTo(expected);
	}
}
