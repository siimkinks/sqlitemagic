package com.siimkinks.sqlite.speedtests;

import android.os.Environment;
import android.support.annotation.NonNull;
import android.util.Log;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.siimkinks.sqlite.speedtests.api.ApiHeaders;
import com.siimkinks.sqlite.speedtests.api.ResultService;
import com.siimkinks.sqlite.speedtests.data.TestData;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;

import retrofit.Endpoints;
import retrofit.RestAdapter;
import retrofit.client.Client;
import retrofit.client.OkClient;
import retrofit.converter.JacksonConverter;
import rx.Observable;
import rx.Subscriber;
import rx.schedulers.Schedulers;

public class TestsController {

	private static final String TAG = TestsController.class.getSimpleName();
	private final App app;
	private final RestAdapter restAdapter = buildBasicRestAdapter(createHttpClient());
	private final ResultService resultService;

	public TestsController(final App app) {
		this.app = app;
		this.resultService = restAdapter.create(ResultService.class);
	}

	public void runTests(final Runnable onEndActions) {
		TestData.create()
				.flatMap(testData -> runTest(testData))
				.doOnNext(this::saveResults)
//				.flatMap(resultService::sendResults)
				.subscribeOn(Schedulers.computation())
				.observeOn(Schedulers.io())
				.subscribe(new Subscriber<TestResults>() {
					@Override
					public void onCompleted() {
						Log.d(TAG, "Tests completed successfully");
						onEnd();
					}

					@Override
					public void onError(Throwable e) {
						Log.e(TAG, "Error while running tests", e);
						onEnd();
					}

					@Override
					public void onNext(TestResults response) {
//						Log.d(TAG, "Test results sent w/statusCode=" + response.getStatus());
					}

					private void onEnd() {
						app.initializer.destroy(app);
						onEndActions.run();
					}
				});
	}

	private Observable<TestResults> runTest(final TestData testData) {
		return Observable.create(new Observable.OnSubscribe<TestResults>() {
			@Override
			public void call(Subscriber<? super TestResults> subscriber) {
				try {
					final TestsRunner runner = new TestsRunner(testData);
					final TestResults testResults = runner.runTests();
					if (!subscriber.isUnsubscribed()) {
						subscriber.onNext(testResults);
						subscriber.onCompleted();
					}
				} catch (Throwable e) {
					if (!subscriber.isUnsubscribed()) {
						subscriber.onError(e);
					}
				}
			}
		});
	}

	private void saveResults(@NonNull TestResults testResults) {
		FileOutputStream fos = null;
		try {
			final File outputDir = new File(Environment.getExternalStorageDirectory(), testResults.getLib());
			if (!outputDir.exists()) {
				outputDir.mkdirs();
			}
			final File outputFile = new File(outputDir, System.currentTimeMillis() + ".json");
			fos = new FileOutputStream(outputFile);
			OBJECT_MAPPER.writeValue(fos, testResults);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
					// ignored
				}
			}
		}
	}

	public final static ObjectMapper OBJECT_MAPPER;

	static {
		OBJECT_MAPPER = new ObjectMapper();
		OBJECT_MAPPER.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
		OBJECT_MAPPER.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
		OBJECT_MAPPER.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		OBJECT_MAPPER.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
		OBJECT_MAPPER.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"));
	}

	public static RestAdapter buildBasicRestAdapter(Client client) {
		return new RestAdapter.Builder()
				.setClient(client)
				.setEndpoint(Endpoints.newFixedEndpoint(BuildConfig.SERVER_URL))
				.setConverter(new JacksonConverter(OBJECT_MAPPER))
				.setRequestInterceptor(new ApiHeaders())
				.build();
	}

	static Client createHttpClient() {
		return new OkClient();
	}
}
