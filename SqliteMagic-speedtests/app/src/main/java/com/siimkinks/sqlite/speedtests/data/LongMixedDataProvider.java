package com.siimkinks.sqlite.speedtests.data;

import android.support.annotation.NonNull;

import com.siimkinks.sqlite.speedtests.handlers.LongMixedDataHandler;

import java.util.List;

public final class LongMixedDataProvider implements DataProvider<LongMixedData> {
	private final TestData testData;
	private final LongMixedDataHandler dataHandler = new LongMixedDataHandler();

	public LongMixedDataProvider(TestData testData) {
		this.testData = testData;
	}

	@NonNull
	@Override
	public DataHandler<LongMixedData> dataHandler() {
		return dataHandler;
	}

	@NonNull
	@Override
	public List<LongMixedData> generateNew() {
		return testData.generateLongMixedData();
	}

	@Override
	public void update(@NonNull List<LongMixedData> data) {
		testData.updateLongMixedData(data);
	}
}
