package com.siimkinks.sqlite.speedtests.data;


import android.support.annotation.NonNull;

import com.siimkinks.sqlite.speedtests.RandomUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;

import rx.Observable;

public class TestData {
	private static final int SIMPLE_DATA_COUNT = 25000;
	private static final int COMPLEX_DATA_COUNT = 400;
	private static final long SEED = 1447966251849L;

	private Random random = new Random(SEED);
	private RandomUtil randomUtil = new RandomUtil(random);

	public TestData() {
	}

	public PingData generatePingData() {
		final PingData o = new PingData();
		o.test = randomUtil.nextString();
		return o;
	}

	public List<FourthLevel> generateFourthLevelData() {
		final int size = COMPLEX_DATA_COUNT;
		final RandomUtil randomUtil = this.randomUtil;
		final Random random = this.random;
		final List<FourthLevel> output = new ArrayList<>(size);
		for (int i = 0; i < size; i++) {
			final FourthLevel o = createFourthLevelData(randomUtil, random);
			output.add(o);
		}
		return output;
	}

	@NonNull
	private FourthLevel createFourthLevelData(RandomUtil randomUtil, Random random) {
		final FourthLevel o = new FourthLevel();
		o.one = createThirdLevelData(randomUtil, random);
		o.two = createThirdLevelData(randomUtil, random);
		return o;
	}

	public void updateFourthLevelData(List<FourthLevel> data) {
		final RandomUtil randomUtil = this.randomUtil;
		final Random random = this.random;
		for (FourthLevel o : data) {
			updateThirdLevelData(randomUtil, random, o.one);
			updateThirdLevelData(randomUtil, random, o.two);
		}
	}

	public List<ThirdLevel> generateThirdLevelData() {
		final int size = COMPLEX_DATA_COUNT;
		final RandomUtil randomUtil = this.randomUtil;
		final Random random = this.random;
		final List<ThirdLevel> output = new ArrayList<>(size);
		for (int i = 0; i < size; i++) {
			final ThirdLevel o = createThirdLevelData(randomUtil, random);
			output.add(o);
		}
		return output;
	}

	@NonNull
	private ThirdLevel createThirdLevelData(RandomUtil randomUtil, Random random) {
		final ThirdLevel o = new ThirdLevel();
		o.one = createSecondLevelData(randomUtil, random);
		o.two = createSecondLevelData(randomUtil, random);
		return o;
	}

	public void updateThirdLevelData(RandomUtil randomUtil, Random random, ThirdLevel data) {
		updateSecondLevelData(randomUtil, random, data.one);
		updateSecondLevelData(randomUtil, random, data.two);
	}

	public List<SecondLevel> generateSecondLevelData() {
		final int size = COMPLEX_DATA_COUNT;
		final RandomUtil randomUtil = this.randomUtil;
		final Random random = this.random;
		final List<SecondLevel> output = new ArrayList<>(size);
		for (int i = 0; i < size; i++) {
			final SecondLevel o = createSecondLevelData(randomUtil, random);
			output.add(o);
		}
		return output;
	}

	@NonNull
	private SecondLevel createSecondLevelData(RandomUtil randomUtil, Random random) {
		final SecondLevel o = new SecondLevel();
		o.one = createFirstLevelData(randomUtil, random);
		o.two = createFirstLevelData(randomUtil, random);
		return o;
	}

	public void updateSecondLevelData(RandomUtil randomUtil, Random random, SecondLevel data) {
		updateFirstLevelData(random, randomUtil, data.one);
		updateFirstLevelData(random, randomUtil, data.two);
	}

	public List<FirstLevel> generateFirstLevelData() {
		final int size = COMPLEX_DATA_COUNT;
		final RandomUtil randomUtil = this.randomUtil;
		final Random random = this.random;
		final List<FirstLevel> output = new ArrayList<>(size);
		for (int i = 0; i < size; i++) {
			final FirstLevel o = createFirstLevelData(randomUtil, random);
			output.add(o);
		}
		return output;
	}

	@NonNull
	private FirstLevel createFirstLevelData(RandomUtil randomUtil, Random random) {
		final FirstLevel o = new FirstLevel();
		MixedData d = new MixedData();
		fillMixedData(random, randomUtil, d);
		o.one = d;
		d = new MixedData();
		fillMixedData(random, randomUtil, d);
		o.two = d;
		return o;
	}

	public void updateFirstLevelData(List<FirstLevel> data) {
		final RandomUtil randomUtil = this.randomUtil;
		final Random random = this.random;
		for (FirstLevel o : data) {
			updateFirstLevelData(random, randomUtil, o);
		}
	}

	private void updateFirstLevelData(Random random, RandomUtil randomUtil, FirstLevel o) {
		fillMixedData(random, randomUtil, o.one);
		fillMixedData(random, randomUtil, o.two);
	}

	public List<StringsData> generateStringsData() {
		final int size = SIMPLE_DATA_COUNT;
		final RandomUtil randomUtil = this.randomUtil;
		final List<StringsData> output = new ArrayList<>(size);
		for (int i = 0; i < size; i++) {
			final StringsData o = new StringsData();
			fillStringsData(randomUtil, o);
			output.add(o);
		}
		return output;
	}

	public void updateStringsData(List<StringsData> data) {
		final RandomUtil randomUtil = this.randomUtil;
		for (StringsData o : data) {
			fillStringsData(randomUtil, o);
		}
	}

	private void fillStringsData(RandomUtil randomUtil, StringsData o) {
		o.one = randomUtil.nextString();
		o.two = randomUtil.nextString();
		o.three = randomUtil.nextString();
		o.four = randomUtil.nextString();
		o.five = randomUtil.nextString();
		o.six = randomUtil.nextString();
		o.seven = randomUtil.nextString();
		o.eight = randomUtil.nextString();
		o.nine = randomUtil.nextString();
		o.ten = randomUtil.nextString();
		o.eleven = randomUtil.nextString();
		o.twelve = randomUtil.nextString();
	}

	public List<NumericData> generateNumericData() {
		final int size = SIMPLE_DATA_COUNT;
		final Random random = this.random;
		final List<NumericData> output = new ArrayList<>(size);
		for (int i = 0; i < size; i++) {
			final NumericData o = new NumericData();
			fillNumericData(random, o);
			output.add(o);
		}
		return output;
	}

	public void updateNumericData(List<NumericData> data) {
		final Random random = this.random;
		for (NumericData o : data) {
			fillNumericData(random, o);
		}
	}

	private void fillNumericData(Random random, NumericData o) {
		o.primitiveInt = random.nextInt();
		o.boxedInt = random.nextInt();
		o.primitiveLong = random.nextLong();
		o.boxedLong = random.nextLong();
		o.primitiveBoolean = random.nextBoolean();
		o.boxedBoolean = random.nextBoolean();
		o.primitiveFloat = random.nextFloat();
		o.boxedFloat = random.nextFloat();
		o.primitiveDouble = random.nextDouble();
		o.boxedDouble = random.nextDouble();
		o.primitiveShort = (short) random.nextInt(Short.MAX_VALUE);
		o.boxedShort = (short) random.nextInt(Short.MAX_VALUE);
	}

	public List<MixedData> generateMixedData() {
		final int size = SIMPLE_DATA_COUNT;
		return generateMixedData(size);
	}

	@NonNull
	public List<MixedData> generateMixedData(int size) {
		final Random random = this.random;
		final RandomUtil randomUtil = this.randomUtil;
		final List<MixedData> output = new ArrayList<>(size);
		for (int i = 0; i < size; i++) {
			final MixedData o = new MixedData();
			fillMixedData(random, randomUtil, o);
			output.add(o);
		}
		return output;
	}

	public void updateMixedData(List<MixedData> data) {
		final Random random = this.random;
		final RandomUtil randomUtil = this.randomUtil;
		for (MixedData o : data) {
			fillMixedData(random, randomUtil, o);
		}
	}

	private void fillMixedData(Random random, RandomUtil randomUtil, MixedData o) {
		o.one = randomUtil.nextString();
		o.two = randomUtil.nextString();
		o.oneL = random.nextLong();
		o.twoL = random.nextLong();
		o.threeL = random.nextLong();
		o.oneD = random.nextDouble();
		o.twoD = random.nextDouble();
		o.threeD = random.nextDouble();
	}

	public List<LongMixedData> generateLongMixedData() {
		final int size = SIMPLE_DATA_COUNT;
		final Random random = this.random;
		final RandomUtil randomUtil = this.randomUtil;
		final List<LongMixedData> output = new ArrayList<>(size);
		for (int i = 0; i < size; i++) {
			final LongMixedData o = new LongMixedData();
			fillLongMixedData(random, randomUtil, o);
			output.add(o);
		}
		return output;
	}

	public void updateLongMixedData(List<LongMixedData> data) {
		final Random random = this.random;
		final RandomUtil randomUtil = this.randomUtil;
		for (LongMixedData o : data) {
			fillLongMixedData(random, randomUtil, o);
		}
	}

	private void fillLongMixedData(Random random, RandomUtil randomUtil, LongMixedData o) {
		o.one = randomUtil.nextString();
		o.two = randomUtil.nextString();
		o.five = randomUtil.nextString();
		o.six = randomUtil.nextString();
		o.nine = randomUtil.nextString();
		o.ten = randomUtil.nextString();
		o.oneL = random.nextLong();
		o.twoL = random.nextLong();
		o.threeL = random.nextLong();
		o.fiveL = random.nextLong();
		o.sixL = random.nextLong();
		o.sevenL = random.nextLong();
		o.oneD = random.nextDouble();
		o.twoD = random.nextDouble();
		o.threeD = random.nextDouble();
		o.fiveD = random.nextDouble();
		o.sixD = random.nextDouble();
		o.sevenD = random.nextDouble();
		o.oneI = random.nextInt();
		o.twoI = random.nextInt();
		o.threeI = random.nextInt();
		o.fiveI = random.nextInt();
		o.sixI = random.nextInt();
		o.sevenI = random.nextInt();
		o.oneF = random.nextFloat();
		o.twoF = random.nextFloat();
		o.threeF = random.nextFloat();
		o.fiveF = random.nextFloat();
		o.sixF = random.nextFloat();
		o.sevenF = random.nextFloat();
	}

	public static Observable<TestData> create() {
		return Observable.fromCallable(new Callable<TestData>() {
			@Override
			public TestData call() throws Exception {
				return new TestData();
			}
		});
	}
}
