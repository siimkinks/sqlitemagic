package com.siimkinks.sqlite.speedtests.data;

import android.support.annotation.NonNull;

import java.util.List;

public interface DataProvider<DataType> {
	@NonNull
	DataHandler<DataType> dataHandler();

	@NonNull
	List<DataType> generateNew();

	void update(@NonNull List<DataType> data);
}
