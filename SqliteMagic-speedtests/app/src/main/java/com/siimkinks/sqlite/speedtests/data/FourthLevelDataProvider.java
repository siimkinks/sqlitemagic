package com.siimkinks.sqlite.speedtests.data;

import android.support.annotation.NonNull;

import com.siimkinks.sqlite.speedtests.handlers.FourthLevelDataHandler;

import java.util.List;

public final class FourthLevelDataProvider implements DataProvider<FourthLevel> {
	private final TestData testData;
	private final FourthLevelDataHandler dataHandler = new FourthLevelDataHandler();

	public FourthLevelDataProvider(TestData testData) {
		this.testData = testData;
	}

	@NonNull
	@Override
	public DataHandler<FourthLevel> dataHandler() {
		return dataHandler;
	}

	@NonNull
	@Override
	public List<FourthLevel> generateNew() {
		return testData.generateFourthLevelData();
	}

	@Override
	public void update(@NonNull List<FourthLevel> data) {
		testData.updateFourthLevelData(data);
	}

}
