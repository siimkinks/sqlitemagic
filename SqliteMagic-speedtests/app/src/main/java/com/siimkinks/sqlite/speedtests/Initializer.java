package com.siimkinks.sqlite.speedtests;

public interface Initializer {
	void init(App app);
	void destroy(App app);
}
