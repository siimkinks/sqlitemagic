package com.siimkinks.sqlite.speedtests.api;


import com.siimkinks.sqlite.speedtests.TestResults;

import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.POST;
import rx.Observable;

public interface ResultService {

	@POST("/results")
	Observable<Response> sendResults(@Body TestResults testResults);
}
