package com.siimkinks.sqlite.speedtests;

import android.support.annotation.CallSuper;
import android.util.Log;

import com.siimkinks.sqlite.speedtests.data.DataHandler;

public abstract class TestCase {
	private static final String TAG = "TestCase";

	public String getName() {
		final DataHandler handler = getHandler();
		if (handler != null) {
			final Class<? extends DataHandler> handlerClass = handler.getClass();
			final String handlerName = handlerClass.getSimpleName();
			return getClass().getSimpleName() + "-" + handlerName.substring(0, handlerName.indexOf("Handler"));
		}
		return getClass().getSimpleName();
	}

	public abstract DataHandler getHandler();
	@CallSuper
	public void setUp() {
		Log.i(TAG, "Starting " + getName());
	}
	public abstract void run();
	public abstract int getIterationCount();
	@CallSuper
	public void tearDown() {
		Log.i(TAG, "Ending " + getName());
	}
}
