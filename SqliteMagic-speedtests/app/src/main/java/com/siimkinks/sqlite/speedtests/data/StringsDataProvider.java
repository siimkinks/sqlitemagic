package com.siimkinks.sqlite.speedtests.data;

import android.support.annotation.NonNull;

import com.siimkinks.sqlite.speedtests.handlers.StringsDataHandler;

import java.util.List;

public final class StringsDataProvider implements DataProvider<StringsData> {
	private final TestData testData;
	private final StringsDataHandler dataHandler = new StringsDataHandler();

	public StringsDataProvider(TestData testData) {
		this.testData = testData;
	}

	@NonNull
	@Override
	public DataHandler<StringsData> dataHandler() {
		return dataHandler;
	}

	@NonNull
	@Override
	public List<StringsData> generateNew() {
		return testData.generateStringsData();
	}

	@Override
	public void update(@NonNull List<StringsData> data) {
		testData.updateStringsData(data);
	}
}
