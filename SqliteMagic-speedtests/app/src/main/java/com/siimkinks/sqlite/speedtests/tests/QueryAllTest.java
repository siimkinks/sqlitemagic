package com.siimkinks.sqlite.speedtests.tests;

import com.siimkinks.sqlite.speedtests.TestCase;
import com.siimkinks.sqlite.speedtests.data.DataHandler;

import java.util.List;

import lombok.Getter;

import static com.google.common.truth.Truth.assertThat;

public final class QueryAllTest<DataType> extends TestCase {
	private static final int ITERATIONS = 10;

	@Getter
	private final DataHandler<DataType> handler;
	private final List<DataType> expected;
	private List<DataType> result;

	public QueryAllTest(DataHandler<DataType> handler, List<DataType> expected) {
		this.handler = handler;
		this.expected = expected;
	}

	@Override
	public void run() {
		final DataHandler<DataType> handler = this.handler;
		for (int i = 0, size = ITERATIONS - 1; i < size; i++) {
			handler.queryAll();
		}
		this.result = handler.queryAll();
	}

	@Override
	public int getIterationCount() {
		return ITERATIONS * expected.size();
	}

	@Override
	public void tearDown() {
		super.tearDown();
		assertThat(result.size()).isEqualTo(expected.size());
		assertThat(result).containsExactlyElementsIn(expected);
	}
}
