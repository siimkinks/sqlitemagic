package com.siimkinks.sqlite.speedtests.tests;


import android.support.annotation.NonNull;

import com.siimkinks.sqlite.speedtests.TestCase;
import com.siimkinks.sqlite.speedtests.data.DataHandler;
import com.siimkinks.sqlite.speedtests.data.PingData;
import com.siimkinks.sqlite.speedtests.handlers.PingDataHandler;

public final class EmptyTestCase extends TestCase {
	private PingData pingData;

	public EmptyTestCase(PingData pingData) {
		this.pingData = pingData;
	}

	@NonNull
	@Override
	public DataHandler getHandler() {
		return null;
	}

	@Override
	public void setUp() {
		super.setUp();
		PingDataHandler.insert(pingData);
	}

	@Override
	public void run() {
	}

	@Override
	public int getIterationCount() {
		return 1;
	}

	@Override
	public void tearDown() {
		super.tearDown();
		pingData = null;
	}
}
