package com.siimkinks.sqlite.speedtests.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Process;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;

import com.siimkinks.sqlite.speedtests.App;
import com.siimkinks.sqlite.speedtests.R;
import com.siimkinks.sqlite.speedtests.TestsController;


public class MainActivity extends ActionBarActivity {
	public static final String TAG = MainActivity.class.getSimpleName();

	public static final String RUN_COUNT = "runCount";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		final int runCount = getIntent().getIntExtra(RUN_COUNT, 0);
		Log.d(TAG, "Started with run count " + runCount);
		final SharedPreferences sharedPreferences = getSharedPreferences(getClass().getSimpleName(), Context.MODE_PRIVATE);
		final int lastRun = sharedPreferences.getInt(RUN_COUNT, -1);
		Log.d(TAG, "Last run count " + lastRun);
		if (runCount > lastRun) {
			TestsController controller = new TestsController((App) getApplication());
			controller.runTests(() -> {
				sharedPreferences
						.edit()
						.putInt(RUN_COUNT, runCount)
						.commit();
				killApp();
			});
		} else {
			Log.d(TAG, "Last run count not less than this run count; killing self");
			killApp();
		}
	}

	private void killApp() {
		finish();
		Process.killProcess(Process.myPid());
	}


}
