package com.siimkinks.sqlite.speedtests.data;

import android.support.annotation.NonNull;

import java.util.List;

public abstract class DataHandler<DataType> {
	public abstract void deleteTable();

	public abstract void bulkInsert(@NonNull List<DataType> data);

	public abstract void bulkUpdate(@NonNull List<DataType> data);

	public abstract void bulkPersist(@NonNull List<DataType> data);

	@NonNull
	public abstract List<DataType> queryAll();

	@NonNull
	public abstract DataType queryFirst();

	public abstract long countAll();

	public abstract long countDependencies();

	public abstract long expectedDependenciesSize(int size);
}
