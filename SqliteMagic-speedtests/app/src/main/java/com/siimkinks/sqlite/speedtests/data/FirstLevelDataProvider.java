package com.siimkinks.sqlite.speedtests.data;

import android.support.annotation.NonNull;

import com.siimkinks.sqlite.speedtests.handlers.FirstLevelDataHandler;

import java.util.List;

public final class FirstLevelDataProvider implements DataProvider<FirstLevel> {
	private final TestData testData;
	private final FirstLevelDataHandler dataHandler = new FirstLevelDataHandler();

	public FirstLevelDataProvider(TestData testData) {
		this.testData = testData;
	}

	@NonNull
	@Override
	public DataHandler<FirstLevel> dataHandler() {
		return dataHandler;
	}

	@NonNull
	@Override
	public List<FirstLevel> generateNew() {
		return testData.generateFirstLevelData();
	}

	@Override
	public void update(@NonNull List<FirstLevel> data) {
		testData.updateFirstLevelData(data);
	}
}
