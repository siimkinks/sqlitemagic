package com.siimkinks.sqlite.speedtests;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TestCaseResult {
	private String name;
	long time;
	int iterations;

	public TestCaseResult(TestCase testCase) {
		this.name = testCase.getName();
	}
}
