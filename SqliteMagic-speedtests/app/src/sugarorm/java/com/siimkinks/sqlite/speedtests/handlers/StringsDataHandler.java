package com.siimkinks.sqlite.speedtests.handlers;

import android.support.annotation.NonNull;

import com.orm.query.Select;
import com.siimkinks.sqlite.speedtests.data.DataHandler;
import com.siimkinks.sqlite.speedtests.data.StringsData;

import java.util.List;

public final class StringsDataHandler extends DataHandler<StringsData> {
	private static final Select<StringsData> QUERY_FIRST = Select.from(StringsData.class).limit("1");

	@Override
	public void deleteTable() {
		StringsData.deleteAll(StringsData.class);
	}

	@Override
	public void bulkInsert(@NonNull List<StringsData> data) {
		StringsData.saveInTx(data);
	}

	@Override
	public void bulkUpdate(@NonNull List<StringsData> data) {
		StringsData.saveInTx(data);
	}

	@Override
	public void bulkPersist(@NonNull List<StringsData> data) {
		StringsData.saveInTx(data);
	}

	@NonNull
	@Override
	public List<StringsData> queryAll() {
		return StringsData.find(StringsData.class, null);
	}

	@NonNull
	@Override
	public StringsData queryFirst() {
		return QUERY_FIRST.first();
	}

	@Override
	public long countAll() {
		return StringsData.count(StringsData.class);
	}

	@Override
	public long countDependencies() {
		return 0;
	}

	@Override
	public long expectedDependenciesSize(int size) {
		return 0;
	}
}
