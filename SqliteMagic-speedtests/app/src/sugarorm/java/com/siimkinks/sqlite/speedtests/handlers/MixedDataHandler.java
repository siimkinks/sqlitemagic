package com.siimkinks.sqlite.speedtests.handlers;

import android.support.annotation.NonNull;

import com.orm.query.Select;
import com.siimkinks.sqlite.speedtests.data.DataHandler;
import com.siimkinks.sqlite.speedtests.data.MixedData;

import java.util.List;

public final class MixedDataHandler extends DataHandler<MixedData> {
	private static final Select<MixedData> QUERY_FIRST = Select.from(MixedData.class).limit("1");

	@Override
	public void deleteTable() {
		MixedData.deleteAll(MixedData.class);
	}

	@Override
	public void bulkInsert(@NonNull List<MixedData> data) {
		MixedData.saveInTx(data);
	}

	@Override
	public void bulkUpdate(@NonNull List<MixedData> data) {
		MixedData.saveInTx(data);
	}

	@Override
	public void bulkPersist(@NonNull List<MixedData> data) {
		MixedData.saveInTx(data);
	}

	@NonNull
	@Override
	public List<MixedData> queryAll() {
		return MixedData.find(MixedData.class, null);
	}

	@NonNull
	@Override
	public MixedData queryFirst() {
		return QUERY_FIRST.first();
	}

	@Override
	public long countAll() {
		return MixedData.count(MixedData.class);
	}

	@Override
	public long countDependencies() {
		return 0;
	}

	@Override
	public long expectedDependenciesSize(int size) {
		return 0;
	}
}
