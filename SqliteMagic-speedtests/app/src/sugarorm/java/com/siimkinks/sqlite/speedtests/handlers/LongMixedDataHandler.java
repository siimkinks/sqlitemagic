package com.siimkinks.sqlite.speedtests.handlers;

import android.support.annotation.NonNull;

import com.orm.query.Select;
import com.siimkinks.sqlite.speedtests.data.DataHandler;
import com.siimkinks.sqlite.speedtests.data.LongMixedData;

import java.util.List;

public final class LongMixedDataHandler extends DataHandler<LongMixedData> {
	private static final Select<LongMixedData> QUERY_FIRST = Select.from(LongMixedData.class).limit("1");

	@Override
	public void deleteTable() {
		LongMixedData.deleteAll(LongMixedData.class);
	}

	@Override
	public void bulkInsert(@NonNull List<LongMixedData> data) {
		LongMixedData.saveInTx(data);
	}

	@Override
	public void bulkUpdate(@NonNull List<LongMixedData> data) {
		LongMixedData.saveInTx(data);
	}

	@Override
	public void bulkPersist(@NonNull List<LongMixedData> data) {
		LongMixedData.saveInTx(data);
	}

	@NonNull
	@Override
	public List<LongMixedData> queryAll() {
		return LongMixedData.find(LongMixedData.class, null);
	}

	@NonNull
	@Override
	public LongMixedData queryFirst() {
		return QUERY_FIRST.first();
	}

	@Override
	public long countAll() {
		return LongMixedData.count(LongMixedData.class);
	}

	@Override
	public long countDependencies() {
		return 0;
	}

	@Override
	public long expectedDependenciesSize(int size) {
		return 0;
	}
}
