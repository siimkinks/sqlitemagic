package com.siimkinks.sqlite.speedtests.handlers;


import android.support.annotation.NonNull;

import com.orm.query.Select;
import com.siimkinks.sqlite.speedtests.data.DataHandler;
import com.siimkinks.sqlite.speedtests.data.NumericData;

import java.util.List;

public final class NumericDataHandler extends DataHandler<NumericData> {
	private static final Select<NumericData> QUERY_FIRST = Select.from(NumericData.class).limit("1");

	@Override
	public void deleteTable() {
		NumericData.deleteAll(NumericData.class);
	}

	@Override
	public void bulkInsert(@NonNull List<NumericData> data) {
		NumericData.saveInTx(data);
	}

	@Override
	public void bulkUpdate(@NonNull List<NumericData> data) {
		NumericData.saveInTx(data);
	}

	@Override
	public void bulkPersist(@NonNull List<NumericData> data) {
		NumericData.saveInTx(data);
	}

	@NonNull
	@Override
	public List<NumericData> queryAll() {
		return NumericData.find(NumericData.class, null);
	}

	@NonNull
	@Override
	public NumericData queryFirst() {
		return QUERY_FIRST.first();
	}

	@Override
	public long countAll() {
		return NumericData.count(NumericData.class);
	}

	@Override
	public long countDependencies() {
		return 0;
	}

	@Override
	public long expectedDependenciesSize(int size) {
		return 0;
	}
}
