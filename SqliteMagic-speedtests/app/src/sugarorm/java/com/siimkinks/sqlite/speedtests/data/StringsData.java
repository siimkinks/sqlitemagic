package com.siimkinks.sqlite.speedtests.data;

import android.support.annotation.NonNull;

import com.orm.SugarRecord;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public final class StringsData extends SugarRecord {
	@NonNull
	String one;
	@NonNull
	String two;
	@NonNull
	String three;
	@NonNull
	String four;
	@NonNull
	String five;
	@NonNull
	String six;
	@NonNull
	String seven;
	@NonNull
	String eight;
	@NonNull
	String nine;
	@NonNull
	String ten;
	@NonNull
	String eleven;
	@NonNull
	String twelve;
}
