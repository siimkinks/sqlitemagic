package com.siimkinks.sqlite.speedtests.data;

import android.support.annotation.NonNull;

import com.orm.SugarRecord;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public final class MixedData extends SugarRecord {
	@NonNull
	String one;
	@NonNull
	String two;
	String three; // null
	String four; // null
	long oneL;
	long twoL;
	@NonNull
	Long threeL;
	Long fourL; // null
	double oneD;
	double twoD;
	@NonNull
	Double threeD;
	Double fourD; // null
}
