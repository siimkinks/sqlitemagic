package com.siimkinks.sqlite.speedtests.handlers;

import android.support.annotation.NonNull;

import com.orm.SugarTransactionHelper;
import com.orm.query.Select;
import com.siimkinks.sqlite.speedtests.data.DataHandler;
import com.siimkinks.sqlite.speedtests.data.FirstLevel;
import com.siimkinks.sqlite.speedtests.data.MixedData;

import java.util.List;

public final class FirstLevelDataHandler extends DataHandler<FirstLevel> {
	private static final Select<FirstLevel> QUERY_FIRST = Select.from(FirstLevel.class).limit("1");

	@Override
	public void deleteTable() {
		FirstLevel.deleteAll(FirstLevel.class);
		MixedData.deleteAll(MixedData.class);
	}

	@Override
	public void bulkInsert(@NonNull List<FirstLevel> data) {
		SugarTransactionHelper.doInTransaction(() -> {
			for (FirstLevel o : data) {
				o.save();
			}
		});
	}

	@Override
	public void bulkUpdate(@NonNull List<FirstLevel> data) {
		SugarTransactionHelper.doInTransaction(() -> {
			for (FirstLevel o : data) {
				o.save();
			}
		});
	}

	@Override
	public void bulkPersist(@NonNull List<FirstLevel> data) {
		SugarTransactionHelper.doInTransaction(() -> {
			for (FirstLevel o : data) {
				o.save();
			}
		});
	}

	@NonNull
	@Override
	public List<FirstLevel> queryAll() {
		return FirstLevel.find(FirstLevel.class, null);
	}

	@NonNull
	@Override
	public FirstLevel queryFirst() {
		return QUERY_FIRST.first();
	}

	@Override
	public long countAll() {
		return FirstLevel.count(FirstLevel.class);
	}

	@Override
	public long countDependencies() {
		return MixedData.count(MixedData.class);
	}

	@Override
	public long expectedDependenciesSize(int size) {
		return size * 2;
	}
}
