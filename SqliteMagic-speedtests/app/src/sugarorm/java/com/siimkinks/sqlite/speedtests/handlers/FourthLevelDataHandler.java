package com.siimkinks.sqlite.speedtests.handlers;

import android.support.annotation.NonNull;

import com.orm.SugarTransactionHelper;
import com.orm.query.Select;
import com.siimkinks.sqlite.speedtests.data.DataHandler;
import com.siimkinks.sqlite.speedtests.data.FirstLevel;
import com.siimkinks.sqlite.speedtests.data.FourthLevel;
import com.siimkinks.sqlite.speedtests.data.MixedData;
import com.siimkinks.sqlite.speedtests.data.SecondLevel;
import com.siimkinks.sqlite.speedtests.data.ThirdLevel;

import java.util.List;

public final class FourthLevelDataHandler extends DataHandler<FourthLevel> {
	private static final Select<FourthLevel> QUERY_FIRST = Select.from(FourthLevel.class).limit("1");

	@Override
	public void deleteTable() {
		MixedData.deleteAll(MixedData.class);
		FirstLevel.deleteAll(FirstLevel.class);
		SecondLevel.deleteAll(SecondLevel.class);
		ThirdLevel.deleteAll(ThirdLevel.class);
		FourthLevel.deleteAll(FourthLevel.class);
	}

	@Override
	public void bulkInsert(@NonNull List<FourthLevel> data) {
		SugarTransactionHelper.doInTransaction(() -> {
			for (FourthLevel o : data) {
				o.save();
			}
		});
	}

	@Override
	public void bulkUpdate(@NonNull List<FourthLevel> data) {
		SugarTransactionHelper.doInTransaction(() -> {
			for (FourthLevel o : data) {
				o.save();
			}
		});
	}

	@Override
	public void bulkPersist(@NonNull List<FourthLevel> data) {
		SugarTransactionHelper.doInTransaction(() -> {
			for (FourthLevel o : data) {
				o.save();
			}
		});
	}

	@NonNull
	@Override
	public List<FourthLevel> queryAll() {
		return FourthLevel.find(FourthLevel.class, null);
	}

	@NonNull
	@Override
	public FourthLevel queryFirst() {
		return QUERY_FIRST.first();
	}

	@Override
	public long countAll() {
		return FourthLevel.count(FourthLevel.class);
	}

	@Override
	public long countDependencies() {
		return MixedData.count(MixedData.class) +
				FirstLevel.count(FirstLevel.class) +
				SecondLevel.count(SecondLevel.class) +
				ThirdLevel.count(ThirdLevel.class);
	}

	@Override
	public long expectedDependenciesSize(int size) {
		return size * 30;
	}
}
