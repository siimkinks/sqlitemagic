package com.siimkinks.sqlite.speedtests.data;

import android.support.annotation.NonNull;

import com.orm.SugarRecord;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public final class NumericData extends SugarRecord {
	int primitiveInt;
	@NonNull
	Integer boxedInt;
	long primitiveLong;
	@NonNull
	Long boxedLong;
	boolean primitiveBoolean;
	@NonNull
	Boolean boxedBoolean;
	float primitiveFloat;
	@NonNull
	Float boxedFloat;
	double primitiveDouble;
	@NonNull
	Double boxedDouble;
	short primitiveShort;
	@NonNull
	Short boxedShort;
}
