package com.siimkinks.sqlite.speedtests.data;

import android.support.annotation.NonNull;

import com.orm.SugarRecord;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public final class FirstLevel extends SugarRecord {
	@NonNull
	MixedData one;
	@NonNull
	MixedData two;

	@Override
	public long save() {
		one.save();
		two.save();
		return super.save();
	}
}
