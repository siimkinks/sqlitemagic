package com.siimkinks.sqlite.speedtests;

import com.orm.SugarContext;

public final class LibInitializer implements Initializer {
	@Override
	public void init(App app) {
		SugarContext.init(app);
	}

	@Override
	public void destroy(App app) {
		SugarContext.terminate();
	}
}
