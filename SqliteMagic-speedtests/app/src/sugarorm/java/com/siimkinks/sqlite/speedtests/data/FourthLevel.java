package com.siimkinks.sqlite.speedtests.data;

import android.support.annotation.NonNull;

import com.orm.SugarRecord;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public final class FourthLevel extends SugarRecord {
	@NonNull
	ThirdLevel one;
	@NonNull
	ThirdLevel two;

	@Override
	public long save() {
		one.save();
		two.save();
		return super.save();
	}
}
