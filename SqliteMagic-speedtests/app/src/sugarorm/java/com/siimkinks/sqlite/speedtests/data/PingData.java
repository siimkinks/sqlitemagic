package com.siimkinks.sqlite.speedtests.data;

import com.orm.SugarRecord;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public final class PingData extends SugarRecord {
	String test;
}
