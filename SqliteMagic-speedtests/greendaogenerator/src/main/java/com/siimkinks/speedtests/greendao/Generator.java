package com.siimkinks.speedtests.greendao;

import java.io.File;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;

public class Generator {
	public static void main(String[] args) {
		System.out.println(new File("./").getAbsolutePath());
		Schema schema = new Schema(1, "com.siimkinks.sqlite.speedtests.data");

		addPingData(schema);
		addStringsData(schema);
		addNumericData(schema);
		addLongMixedData(schema);
		final Entity mixedData = addMixedData(schema);
		final Entity firstLevel = addFirstLevelData(schema, mixedData);
		final Entity secondLevel = addSecondLevelData(schema, firstLevel);
		final Entity thirdLevel = addThirdLevelData(schema, secondLevel);
		addFourthLevelData(schema, thirdLevel);

		try {
			new DaoGenerator().generateAll(schema, "app/src/greendao/java");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void addPingData(Schema schema) {
		final Entity pingData = schema.addEntity("PingData");
		pingData.setCodeBeforeClass("@lombok.EqualsAndHashCode");
		pingData.addIdProperty().autoincrement();
		pingData.addStringProperty("test");
	}

	private static void addStringsData(Schema schema) {
		final Entity stringsData = schema.addEntity("StringsData");
		stringsData.setCodeBeforeClass("@lombok.EqualsAndHashCode");
		stringsData.addIdProperty().autoincrement();
		stringsData.addStringProperty("one").notNull();
		stringsData.addStringProperty("two").notNull();
		stringsData.addStringProperty("three").notNull();
		stringsData.addStringProperty("four").notNull();
		stringsData.addStringProperty("five").notNull();
		stringsData.addStringProperty("six").notNull();
		stringsData.addStringProperty("seven").notNull();
		stringsData.addStringProperty("eight").notNull();
		stringsData.addStringProperty("nine").notNull();
		stringsData.addStringProperty("ten").notNull();
		stringsData.addStringProperty("eleven").notNull();
		stringsData.addStringProperty("twelve").notNull();
	}

	private static void addNumericData(Schema schema) {
		final Entity numericData = schema.addEntity("NumericData");
		numericData.setCodeBeforeClass("@lombok.EqualsAndHashCode");
		numericData.addIdProperty().autoincrement();
		numericData.addIntProperty("primitiveInt").notNull();
		numericData.addIntProperty("boxedInt");
		numericData.addLongProperty("primitiveLong").notNull();
		numericData.addLongProperty("boxedLong");
		numericData.addBooleanProperty("primitiveBoolean").notNull();
		numericData.addBooleanProperty("boxedBoolean");
		numericData.addFloatProperty("primitiveFloat").notNull();
		numericData.addFloatProperty("boxedFloat");
		numericData.addDoubleProperty("primitiveDouble").notNull();
		numericData.addDoubleProperty("boxedDouble");
		numericData.addShortProperty("primitiveShort").notNull();
		numericData.addShortProperty("boxedShort");
	}

	private static void addLongMixedData(Schema schema) {
		final Entity longMixedData = schema.addEntity("LongMixedData");
		longMixedData.setCodeBeforeClass("@lombok.EqualsAndHashCode");
		longMixedData.addIdProperty().autoincrement();
		longMixedData.addStringProperty("one").notNull();
		longMixedData.addStringProperty("two").notNull();
		longMixedData.addStringProperty("three");
		longMixedData.addStringProperty("four");
		longMixedData.addStringProperty("five").notNull();
		longMixedData.addStringProperty("six").notNull();
		longMixedData.addStringProperty("seven");
		longMixedData.addStringProperty("eight");
		longMixedData.addStringProperty("nine").notNull();
		longMixedData.addStringProperty("ten").notNull();
		longMixedData.addStringProperty("eleven");
		longMixedData.addStringProperty("twelve");
		longMixedData.addLongProperty("oneL").notNull();
		longMixedData.addLongProperty("twoL").notNull();
		longMixedData.addLongProperty("threeL");
		longMixedData.addLongProperty("fourL");
		longMixedData.addLongProperty("fiveL").notNull();
		longMixedData.addLongProperty("sixL").notNull();
		longMixedData.addLongProperty("sevenL");
		longMixedData.addLongProperty("eightL");
		longMixedData.addDoubleProperty("oneD").notNull();
		longMixedData.addDoubleProperty("twoD").notNull();
		longMixedData.addDoubleProperty("threeD");
		longMixedData.addDoubleProperty("fourD");
		longMixedData.addDoubleProperty("fiveD").notNull();
		longMixedData.addDoubleProperty("sixD").notNull();
		longMixedData.addDoubleProperty("sevenD");
		longMixedData.addDoubleProperty("eightD");
		longMixedData.addIntProperty("oneI").notNull();
		longMixedData.addIntProperty("twoI").notNull();
		longMixedData.addIntProperty("threeI");
		longMixedData.addIntProperty("fourI");
		longMixedData.addIntProperty("fiveI").notNull();
		longMixedData.addIntProperty("sixI").notNull();
		longMixedData.addIntProperty("sevenI");
		longMixedData.addIntProperty("eightI");
		longMixedData.addFloatProperty("oneF").notNull();
		longMixedData.addFloatProperty("twoF").notNull();
		longMixedData.addFloatProperty("threeF");
		longMixedData.addFloatProperty("fourF");
		longMixedData.addFloatProperty("fiveF").notNull();
		longMixedData.addFloatProperty("sixF").notNull();
		longMixedData.addFloatProperty("sevenF");
		longMixedData.addFloatProperty("eightF");
	}

	private static Entity addMixedData(Schema schema) {
		final Entity mixedData = schema.addEntity("MixedData");
		mixedData.setCodeBeforeClass("@lombok.EqualsAndHashCode");
		mixedData.addIdProperty().autoincrement();
		mixedData.addStringProperty("one").notNull();
		mixedData.addStringProperty("two").notNull();
		mixedData.addStringProperty("three");
		mixedData.addStringProperty("four");
		mixedData.addLongProperty("oneL").notNull();
		mixedData.addLongProperty("twoL").notNull();
		mixedData.addLongProperty("threeL");
		mixedData.addLongProperty("fourL");
		mixedData.addDoubleProperty("oneD").notNull();
		mixedData.addDoubleProperty("twoD").notNull();
		mixedData.addDoubleProperty("threeD");
		mixedData.addDoubleProperty("fourD");
		return mixedData;
	}

	private static Entity addFirstLevelData(Schema schema, Entity mixedData) {
		final Entity firstLevel = schema.addEntity("FirstLevel");
		firstLevel.setCodeBeforeClass("@lombok.EqualsAndHashCode(doNotUseGetters = true, of = {\"id\", \"one\", \"two\"})");
		firstLevel.addIdProperty().autoincrement();
		final Property oneProperty = firstLevel.addLongProperty("one_id").getProperty();
		firstLevel.addToOne(mixedData, oneProperty, "one");
		final Property twoProperty = firstLevel.addLongProperty("two_id").getProperty();
		firstLevel.addToOne(mixedData, twoProperty, "two");
		return firstLevel;
	}

	private static Entity addSecondLevelData(Schema schema, Entity firstLevel) {
		final Entity secondLevel = schema.addEntity("SecondLevel");
		secondLevel.setCodeBeforeClass("@lombok.EqualsAndHashCode(doNotUseGetters = true, of = {\"id\", \"one\", \"two\"})");
		secondLevel.addIdProperty().autoincrement();
		final Property oneProperty = secondLevel.addLongProperty("one_id").getProperty();
		secondLevel.addToOne(firstLevel, oneProperty, "one");
		final Property twoProperty = secondLevel.addLongProperty("two_id").getProperty();
		secondLevel.addToOne(firstLevel, twoProperty, "two");
		return secondLevel;
	}

	private static Entity addThirdLevelData(Schema schema, Entity secondLevel) {
		final Entity thirdLevel = schema.addEntity("ThirdLevel");
		thirdLevel.setCodeBeforeClass("@lombok.EqualsAndHashCode(doNotUseGetters = true, of = {\"id\", \"one\", \"two\"})");
		thirdLevel.addIdProperty().autoincrement();
		final Property oneProperty = thirdLevel.addLongProperty("one_id").getProperty();
		thirdLevel.addToOne(secondLevel, oneProperty, "one");
		final Property twoProperty = thirdLevel.addLongProperty("two_id").getProperty();
		thirdLevel.addToOne(secondLevel, twoProperty, "two");
		return thirdLevel;
	}

	private static void addFourthLevelData(Schema schema, Entity thirdLevel) {
		final Entity fourthLevel = schema.addEntity("FourthLevel");
		fourthLevel.setCodeBeforeClass("@lombok.EqualsAndHashCode(doNotUseGetters = true, of = {\"id\", \"one\", \"two\"})");
		fourthLevel.addIdProperty().autoincrement();
		final Property oneProperty = fourthLevel.addLongProperty("one_id").getProperty();
		fourthLevel.addToOne(thirdLevel, oneProperty, "one");
		final Property twoProperty = fourthLevel.addLongProperty("two_id").getProperty();
		fourthLevel.addToOne(thirdLevel, twoProperty, "two");
	}
}
