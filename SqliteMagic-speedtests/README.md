Speed tests
==========
Contains speed tests for every library.

For every competing ORM library and build type there is a task in the form of
```run<library name><build type name>SpeedTests```.
It will assemble build variant and run it on all devices found with ```adb devices``` command.

Running tests:
--------
```
./gradlew run<library name><build type name>SpeedTests
```