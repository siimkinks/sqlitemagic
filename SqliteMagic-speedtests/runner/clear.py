#!/usr/bin/env python


import os
import shutil
import sys

from conf import *


def clear_app_data(app):
	print exec_cmd("adb shell pm clear %s" % get_app_package(app))[0]


def clear_app_results_in_device(app):
	print exec_cmd("adb shell rm -rf /sdcard/%s" % app)[0]


def clear_all_local_results():
	if os.path.exists(results_root_dir):
		shutil.rmtree(results_root_dir)


def clear_results():
	clear_all_local_results()
	for app in apps:
		clear_app_data(app)
		clear_app_results_in_device(app)


def main(argv):
	clear_results()


if __name__ == '__main__':
	sys.exit(main(sys.argv))