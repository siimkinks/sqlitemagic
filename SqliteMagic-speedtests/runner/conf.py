import subprocess
import errno
import os

apps = ["dbflow", "greendao", "sqlitemagic", "sugarorm", "storio"]

apk_dir = "../app/build/outputs/apk"
package_root = "com.siimkinks.speedtests."
results_root_dir = "results"

test_iterations = 4 # 8h


def get_app_package(app):
	return package_root + app


def exec_cmd(cmd, print_cmd=True):
	if not isinstance(cmd, basestring):
		cmd = ' '.join(cmd)
	if print_cmd:
		print "executing command", cmd
	p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
	(output, err) = p.communicate()
	p_status = p.wait()
	return output, err, p_status


def mkdir(dir):
	try:
		os.makedirs(dir)
	except OSError as exc:
		if exc.errno == errno.EEXIST and os.path.isdir(dir):
			pass
		else:
			raise