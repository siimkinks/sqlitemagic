#!/usr/bin/env python


import json
import os
import sys
from pprint import pprint

from conf import results_root_dir


def collect(json_data, lib_results):
	model_name = json_data['modelName']
	if model_name not in lib_results:
		lib_results[model_name] = dict()
	results_dict = lib_results[model_name]
	for test_result in json_data['testResults']:
		name = test_result['name']
		time = test_result['time']
		iterations = test_result['iterations']
		if name not in results_dict:
			results_dict[name] = (time, iterations)
		else:
			prev = results_dict[name]
			results_dict[name] = (prev[0] + time, prev[1] + iterations)


def fill_lib_results(all_results, lib_results, lib_name):
	for model_name in lib_results.keys():
		if model_name not in all_results:
			all_results[model_name] = dict()
		model_results = all_results[model_name]
		tests = lib_results[model_name]
		for test_name in tests:
			if test_name not in model_results:
				model_results[test_name] = dict()
			test_results = model_results[test_name]
			raw_test_result = tests[test_name]
			op_time_in_sec = (raw_test_result[0] / raw_test_result[1]) / 1e9
			ops_in_second = 1.0 / op_time_in_sec
			test_results[lib_name] = ops_in_second


def collect_all_data(output):
	for lib in os.listdir(results_root_dir):
		lib_dir = os.path.join(results_root_dir, lib)
		lib_results = dict()
		print "collecting " + lib_dir
		for result in os.listdir(lib_dir):
			result_file = os.path.join(lib_dir, result)
			with open(result_file) as f:
				data = json.load(f)
			collect(data, lib_results)
		fill_lib_results(output, lib_results, lib)


def print_all_results():
	all_results = dict()
	collect_all_data(all_results)
	pprint(all_results)


def main(argv):
	print_all_results()


if __name__ == '__main__':
	sys.exit(main(sys.argv))
