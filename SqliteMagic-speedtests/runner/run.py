#!/usr/bin/env python


import sys
import time

from clear import clear_app_data
from collect import print_all_results
from conf import *


def get_apk_abs_dir(app):
	return "%s/app-%s-debug.apk" % (apk_dir, app)


def ensure_dependencies():
	if not os.path.exists(apk_dir):
		raise Exception('no apks')
	for orm in apps:
		if not os.path.exists(get_apk_abs_dir(orm)):
			raise Exception('missing dependency ')


def install_app(app):
	exec_cmd("adb install -r %s" % (get_apk_abs_dir(app)))


def start_app(app, run_count):
	package = get_app_package(app)
	exec_cmd("adb shell am start -a android.intent.action.MAIN -c android.intent.category.LAUNCHER -n %s/com.siimkinks.sqlite.speedtests.activity.MainActivity --ei runCount %s" % (package, run_count))


def is_app_running(app):
	app_package_name = get_app_package(app)
	cmd = "adb shell ps " + app_package_name[-15:]
	return exec_cmd(cmd, False)[0].strip().endswith(app_package_name)


def wait_until_app_finishes(app):
	print "waiting %s to finish" % app
	start = time.time()
	while is_app_running(app):
		time.sleep(5)
	end = time.time()
	m, s = divmod((end - start), 60)
	h, m = divmod(m, 60)
	print app + (" finished in %d:%02d:%02ds" % (h, m, s))


def pull_results(app):
	dest_dir = results_root_dir + "/" + app
	mkdir(dest_dir)
	exec_cmd("adb pull /sdcard/%s %s" % (app, dest_dir))


def main(argv):
	ensure_dependencies()
	for app in apps:
		install_app(app)
		clear_app_data(app)
		for i in xrange(test_iterations):
			start_app(app, i)
			wait_until_app_finishes(app)
		pull_results(app)
	print_all_results()


if __name__ == '__main__':
	sys.exit(main(sys.argv))
