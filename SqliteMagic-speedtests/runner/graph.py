#!/usr/bin/env python
# -*- coding: utf-8 -*-


from __future__ import unicode_literals

import numpy as np
import sys

import matplotlib.pyplot as plt

from collect import collect_all_data
from conf import mkdir

output_dir = "../../paper/src/figs"
display_order = ["sqlitemagic", "greendao", "dbflow", "sugarorm", "storio"]
colors = ['c', 'm', 'g', 'b', 'r']
translations = {
	"StringsData": "Märgijadadega",
	"NumericData": "Arvudega",
	"LongMixedData": "Suur\nsegaandmetega",
	"MixedData": "Segaandmetega",
	"FirstLevelData": "Üheastmelise\nsõltuvusega",
	"FourthLevelData": "Neljaastmelise\nsõltuvusega",
	"BulkInsertDataTest": "Sisestus",
	"BulkUpdateDataTest": "Uuendus",
	"BulkPersistDataTest": "Tundmatu sisestus",
	"BulkPersistWithUpdateDataTest": "Tundmatu sisestus täis tabelisse",
	"BulkPersistWithInsertDataTest": "Tundmatu sisestus tühja tabelisse",
	"CountTest": "Loendus",
	"QueryAllTest": "Kõikide andmete pärimine",
	"QueryFirstTest": "Tabeli esimese kirje pärimine"
}


def translate(key):
	if key in translations:
		return translations[key]
	return key


def gen_chart(data_types, lib_results, test_name):
	index = np.arange(len(data_types))
	bar_width = 0.17
	fig, ax = plt.subplots()
	max_val = 0
	i = 4
	for lib in display_order:
		results = lib_results[lib]
		ax.barh(index + bar_width * i, results, bar_width, color=colors[i], label=lib)
		max_val = max(max_val, max(results))
		i -= 1
	plt.grid(True, axis='x')
	plt.autoscale(tight=True)
	ax.set_ylabel('Andmetüüp')
	ax.set_xlabel('Operatsioone sekundis')
	ax.axis('auto')
	plt.title(translate(test_name))
	plt.yticks(index + bar_width * 2.5, map(translate, data_types))
	handles, labels = ax.get_legend_handles_labels()
	lgd = ax.legend(handles, labels, loc='upper center', bbox_to_anchor=(0.5, -0.1), ncol=3)
	plt.tight_layout()
	mkdir(output_dir)
	plt.savefig("%s/%s.png" % (output_dir, test_name), bbox_extra_artists=(lgd,), bbox_inches='tight')


def gen_test_chart(test_name, data_type_results):
	data_types = data_type_results.keys()
	data_types.sort(reverse=True)
	lib_results = {}
	for data_type_name in data_types:
		for lib, val in data_type_results[data_type_name].iteritems():
			if lib not in lib_results:
				lib_results[lib] = []
			lib_results[lib].append(val)
	gen_chart(data_types, lib_results, test_name)


def gen_combined_test_chart(test_name, first_data_type_results, second_data_type_results):
	data_types = first_data_type_results.keys()
	data_types.sort(reverse=True)
	lib_results = {}
	err = {}
	for data_type_name in data_types:
		for lib in first_data_type_results[data_type_name]:
			if lib not in lib_results:
				lib_results[lib] = []
			if lib not in err:
				err[lib] = []
			first_val = first_data_type_results[data_type_name][lib]
			second_val = second_data_type_results[data_type_name][lib]
			err[lib].append(abs(first_val - second_val))
			lib_results[lib].append((first_val + second_val) / 2)
	gen_chart(data_types, lib_results, test_name)


def collect_structured_results():
	all_results = {}
	collect_all_data(all_results)
	first_device_results = all_results.itervalues().next()
	results = {}
	for key, value in first_device_results.iteritems():
		split_key = key.split("-")
		use_case_name = split_key[0]
		test_data_name = split_key[1]
		if use_case_name not in results:
			results[use_case_name] = {}
		use_case_results = results[use_case_name]
		use_case_results[test_data_name] = value
	return results


def gen_charts():
	results = collect_structured_results()
	test_case_names = results.keys()
	persist_w_insert = "BulkPersistWithInsertDataTest"
	persist_w_update = "BulkPersistWithUpdateDataTest"
	test_case_names.remove(persist_w_insert)
	test_case_names.remove(persist_w_update)
	for test_name in test_case_names:
		data_type_results = results[test_name]
		gen_test_chart(test_name, data_type_results)
	gen_combined_test_chart("BulkPersistDataTest", results[persist_w_insert], results[persist_w_update])


def main(argv):
	gen_charts()


if __name__ == '__main__':
	sys.exit(main(sys.argv))
