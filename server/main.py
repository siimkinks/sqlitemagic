import json
import webapp2

from google.appengine.ext import ndb

def test_lib_key(lib):
    return ndb.Key('TestResult', lib)

class Device(ndb.Model):
    model_name = ndb.StringProperty(indexed=False)

class TestCaseResult(ndb.Model):
    lib = ndb.StringProperty(indexed=False)
    name = ndb.StringProperty(indexed=True)
    time = ndb.IntegerProperty(indexed=False)
    device = ndb.StructuredProperty(Device)
    created = ndb.DateTimeProperty(auto_now_add=True)
    
    @classmethod
    def query_lib(cls, ancestor_key):
        return cls.query(ancestor=ancestor_key)

class Statistics(webapp2.RequestHandler):
    def get(self):
        stats = {}
        for obj in ndb.gql("SELECT DISTINCT name FROM TestCaseResult").fetch():
            test_name = obj.name
            test_stats = {}
            for test_case in ndb.gql("SELECT * FROM TestCaseResult WHERE name = :1", test_name).fetch():
                if test_case.lib not in test_stats:
                    test_stats[test_case.lib] = [0, 0]
                lib_test_case_stats = test_stats[test_case.lib]
                lib_test_case_stats[0] += test_case.time
                lib_test_case_stats[1] += 1
                test_stats[test_case.lib] = lib_test_case_stats
            stats[test_name] = {}
            for test_case, lib_test_case_stats in test_stats.iteritems():
                stats[test_name][test_case] = lib_test_case_stats[0] / lib_test_case_stats[1]
        
        self.response.headers['Content-Type'] = 'application/json'
        self.response.write(json.dumps(stats))

class RawResults(webapp2.RequestHandler):
    def get(self):
        query_limit = 20
        
        requested_lib_name = self.request.get('lib_name', "sqlitemagic")
        requested_lib_key = test_lib_key(requested_lib_name)
        
        self.response.headers['Content-Type'] = 'text/plain'
        
        test_case_results = TestCaseResult.query_lib(requested_lib_key).fetch(query_limit)
        self.response.write("Test case results:\n\n")
        for result in test_case_results:
            self.response.write(result)
            self.response.write("\n")

class PerformanceResults(webapp2.RequestHandler):
    def post(self):
        jsonstring = self.request.body
        jsonobject = json.loads(jsonstring)
        
        test_lib_str = jsonobject["lib"]
        test_lib = test_lib_key(test_lib_str)
        running_device = Device(model_name=jsonobject["modelName"])
        
        for test in jsonobject["testCases"]:
            test_case = TestCaseResult(parent=test_lib, name=test["name"], time=test["time"], device=running_device, lib=test_lib_str)
            test_case.put()
        
        self.response.headers['Content-Type'] = 'text/plain'
        self.response.set_status(200)
        self.response.write(jsonobject)

app = webapp2.WSGIApplication([
    ('/stats', Statistics),
    ('/raw', RawResults),
    ('/results', PerformanceResults),
], debug=True)
